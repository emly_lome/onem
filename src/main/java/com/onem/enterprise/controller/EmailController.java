/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.controller;

//import com.onem.enterprise.factory.FormFactory;
//import com.onem.enterprise.form.EmailForm;
import com.onem.enterprise.form.ContactForm;
import com.onem.enterprise.service.ConfigurationService;
import com.onem.enterprise.service.EmailUtil;
//import com.onem.enterprise.service.AdministratorService;
import com.onem.enterprise.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 *
 * @author ekasongo
 */
@Controller
@SessionAttributes(value = {"session", "userType"})
public class EmailController {

    @Autowired
    private JobService jobService;
    
    @Autowired
    private ConfigurationService configs;

    @Autowired
    private EmailUtil emailUtil;
    
    @RequestMapping(value = "send-message", method = RequestMethod.POST)
    public String processLogin(@ModelAttribute("message") ContactForm message,
            ModelMap model) {
        try {
            emailUtil.setTo(configs.getAdminEmail());
            emailUtil.setSubject(message.getSubject());
            emailUtil.setContactEmailText(message.getMessage());
            emailUtil.send();
            
            return "redirect:/contact.htm?msg=message sent;success";
        } catch (MessagingException ex) {
            Logger.getLogger(EmailController.class.getName()).log(Level.SEVERE, null, ex);
            return "redirect:/contact.htm?msg=message failed;error";    
        }
    }

/*   @Autowired
    private AdministratorService administratorService;

    @RequestMapping(value = "/apply", method = RequestMethod.GET)
    public String apply(@RequestParam int id, ModelMap model, HttpSession session) {
        Object user = session.getAttribute(SESSION_TAG);
        if (user == null) {
            return "redirect:/login.htm";
        }
        model.addAttribute(PAGE_TITLE_TAG, "Apply to Job");
        JobOffer job = jobService.getJobOffer(id);
        Enterprise enterprise = job.getEnterprise();
        String cc = (user instanceof JobSeeker) ? ((JobSeeker) user).getContactEmail() : ((Enterprise) user).getContactEmail();

        model.addAttribute("job", job);
        model.addAttribute("enterprise", enterprise);
        model.addAttribute("email", FormFactory.createEmailForm().setTo(enterprise.getContactEmail())
                .setCc(cc).setSubject(job.getJob().getTitle()));
        model.addAttribute("id", id);
        return EMAIL_LOCATION + "compose";
    }

    @RequestMapping(value = "apply-offer", method = RequestMethod.POST)
    public String applyOffer(@ModelAttribute("SpringWeb") final EmailForm emailForm, ModelMap model,
            final @RequestParam CommonsMultipartFile attachFile, HttpSession session, @RequestParam int id) {
        Object user = session.getAttribute(SESSION_TAG);
        if (user == null) {
            return "redirect:/login.htm";
        }
        mailSender.send(new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(
                        mimeMessage, true, "UTF-8");
                messageHelper.setTo(emailForm.getTo());
                messageHelper.setCc(emailForm.getCc());
                messageHelper.setSubject(emailForm.getSubject());
                messageHelper.setText(emailForm.getText());

                // determines if there is an upload file, attach it to the e-mail
                String attachName = attachFile.getOriginalFilename();
                if (!attachFile.equals("")) {

                    messageHelper.addAttachment(attachName, new InputStreamSource() {
                        @Override
                        public InputStream getInputStream() throws IOException {
                            return attachFile.getInputStream();
                        }
                    });
                }
            }
        });
        JobOfferApplication application = new JobOfferApplication();
        application.setJobOffer(jobService.getJobOffer(id));
        application.setApplicationTime(new Date());
        administratorService.recordApplication(application);
        if (user instanceof JobSeeker) {
            return "redirect:/job-seeker-area.htm?msg=Your email has been sent.";
        } else {
            return "redirect:/enterprises-area.htm?msg=Your email has been sent.";
        }
    }*/
    
}
