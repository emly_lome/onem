/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.controller;

import com.onem.enterprise.form.DocumentationForm;
import com.onem.enterprise.model.Administrator;
import com.onem.enterprise.model.Documentation;
import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.Job;
import com.onem.enterprise.service.ConfigurationService;
import com.onem.enterprise.service.EnterpriseService;
import com.onem.enterprise.service.JobSeekerService;
import com.onem.enterprise.service.JobService;
import com.onem.enterprise.service.PressReleaseService;
import com.onem.enterprise.util.FileUtil;
import com.onem.enterprise.util.PageUtil;
import com.onem.enterprise.util.StringUtil;
import static com.onem.enterprise.util.StringUtil.ADMINISTRATOR_LOCATION;
import static com.onem.enterprise.util.StringUtil.ADMINISTRATOR_USER;
import static com.onem.enterprise.util.StringUtil.DEFAULT_PAGE;
import static com.onem.enterprise.util.StringUtil.ENTERPRISE_STATUS_ACTIVATED;
import static com.onem.enterprise.util.StringUtil.ENTERPRISE_STATUS_DESACTIVATED;
import static com.onem.enterprise.util.StringUtil.JOB_STATUS_PENDING;
import static com.onem.enterprise.util.StringUtil.PAGE_TITLE_TAG;
import static com.onem.enterprise.util.StringUtil.SESSION_TAG;
import static com.onem.enterprise.util.StringUtil.STATUS;
import java.io.File;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author Lyn
 */
@Controller
@SessionAttributes(value = {"session", "user"})
public class AdminController {
    
    @Autowired
    private PressReleaseService releaseService;
    @Autowired
    private ConfigurationService configService;
    @Autowired
    private EnterpriseService enterpriseService;
    @Autowired
    private JobSeekerService jobSeekerService;
    @Autowired
    private JobService jobService;
    
    @RequestMapping(value = "administrator", method = RequestMethod.GET)
    public String enterAdminArea(ModelMap model, HttpSession session){
        Object user = session.getAttribute(SESSION_TAG);
        if(user == null || !(user instanceof Administrator)){
            return "redirect:/login.htm?home=" + ADMINISTRATOR_USER;
        }
        List<Enterprise> enterprises = enterpriseService.listAllEnterprises();
        model.addAttribute(PAGE_TITLE_TAG, "administrator");
        model.addAttribute("releases", releaseService.listAllDocumentation());
        model.addAttribute("document", new DocumentationForm());
        model.addAttribute("noOfEnterprise", enterprises.size());
        model.addAttribute("noOfApplication", jobSeekerService.listJobApplication().size());
        model.addAttribute("noOfJobSeker", jobSeekerService.listJobSeekers().size());
        model.addAttribute("noOfActivateEnterprise", getActivatedEnterprises(enterprises, ENTERPRISE_STATUS_ACTIVATED));
        model.addAttribute("noOfNonActivateEnterprise", getActivatedEnterprises(enterprises, ENTERPRISE_STATUS_DESACTIVATED));
        
        return ADMINISTRATOR_LOCATION + "administrator";
    }
    
    @RequestMapping(value = "manage-documentations", method = RequestMethod.GET)
    public String enterManageDocumentation(ModelMap model, HttpSession session){
        Object user = session.getAttribute(SESSION_TAG);
        if(user == null || !(user instanceof Administrator)){
            return "redirect:/login.htm?home=" + ADMINISTRATOR_USER;
        }
        model.addAttribute(PAGE_TITLE_TAG, "administrator");
        model.addAttribute("releases", releaseService.listAllDocumentation());
        model.addAttribute("document", new DocumentationForm());
        return ADMINISTRATOR_LOCATION + "manage-documentations";
    }
    
    @RequestMapping(value = "upload-documentation", method = RequestMethod.POST)
    public String UploadDocumentation(ModelMap model, @ModelAttribute DocumentationForm document,
                        final @RequestParam CommonsMultipartFile documentFile){
        try{
            Documentation doc = document.buildObject(null);
            if(doc == null){
                throw new RuntimeException("Failed to save!");
            }
            doc.setFileName(documentFile.getOriginalFilename());
            if(documentFile.isEmpty()){
                throw new RuntimeException("Pas de fichier attacher. Attacher votre document!!");
            }
            if(!documentFile.getOriginalFilename().endsWith(".pdf")){
                throw new RuntimeException("Attacher un ficher pdf");
            }
            
            File file = FileUtil.createDocumentationFile(documentFile.getOriginalFilename(), 
                            document.getType(), configService.getDocumentationLocation());
            documentFile.transferTo(file);
            releaseService.addDocument(doc);
        }catch(Exception e){
            e.printStackTrace();
        }
        return "redirect:/administrator.htm";
    }
    
    @RequestMapping(value = "manage-enterprises", method = RequestMethod.GET)
    public String manageEnterprises(ModelMap model, HttpSession session,
                                    @RequestParam(required = false) final Integer page, 
                                    @RequestParam(required = false) final String action,
                                    @RequestParam(required = false) final Integer option,
                                    @RequestParam(required = false) final String status,
                                    @RequestParam(required = false) final String current,
                                    @RequestParam(required = false) final Integer id){
        Object user = session.getAttribute(SESSION_TAG);
        if(user == null || !(user instanceof Administrator)){
            return "redirect:/login.htm?home=" + ADMINISTRATOR_USER;
        }
        if(STATUS.equals(action)){
            Enterprise enterprise = enterpriseService.getEnterprise(id);
            if(enterprise != null){
                enterprise.setStatus(status);
                enterpriseService.updateEnterprise(enterprise);
            }
        }
        List<Enterprise> enterprises = enterpriseService.listAllEnterprises();
        model.addAttribute(PAGE_TITLE_TAG, "manage enterprise");
        model.addAttribute("releases", releaseService.listAllDocumentation());
        model.addAttribute("enterprises", enterprises);
        model.addAttribute("noOfEnterprise", enterprises.size());
        model.addAttribute("noOfActivateEnterprise", getActivatedEnterprises(enterprises, ENTERPRISE_STATUS_ACTIVATED));
        model.addAttribute("noOfNonActivateEnterprise", getActivatedEnterprises(enterprises, ENTERPRISE_STATUS_DESACTIVATED));
        return ADMINISTRATOR_LOCATION + "manage-enterprises";
    }
    
    @RequestMapping(value = "manage-job-offers", method = RequestMethod.GET)
    public String manageJobOffers(HttpServletRequest request, ModelMap model, HttpSession session,
                                    @RequestParam(required = false) Integer page, 
                                    @RequestParam(required = false) final String action,
                                    @RequestParam(required = false) final Integer option,
                                    @RequestParam(required = false) final String status,
                                    @RequestParam(required = false) final String current,
                                    @RequestParam(required = false) final Integer id){
        Object user = session.getAttribute(SESSION_TAG);
        if(user == null || !(user instanceof Administrator)){
            return "redirect:/login.htm?home=" + ADMINISTRATOR_USER;
        }
        if(STATUS.equals(action)){
            Enterprise enterprise = enterpriseService.getEnterprise(id);
            if(enterprise != null){
                enterprise.setStatus(status);
                enterpriseService.updateEnterprise(enterprise);
            }
        }
        if(page == null){
            page = DEFAULT_PAGE;
        }
        List<Enterprise> enterprises = enterpriseService.listAllEnterprises();
        model.put("jobs", PageUtil.getPage(page, jobService.listAvailableOffers(JOB_STATUS_PENDING)));
        model.addAttribute("pages", PageUtil.size());
        model.addAttribute("selected", page);
        model.addAttribute(PAGE_TITLE_TAG, "manage job offer");
        model.addAttribute("releases", releaseService.listAllDocumentation());
        model.addAttribute("enterprises", enterprises);
        model.addAttribute("noOfEnterprise", enterprises.size());
        model.addAttribute("noOfActivateEnterprise", getActivatedEnterprises(enterprises, ENTERPRISE_STATUS_ACTIVATED));
        model.addAttribute("noOfNonActivateEnterprise", getActivatedEnterprises(enterprises, ENTERPRISE_STATUS_DESACTIVATED));
        return ADMINISTRATOR_LOCATION + "manage-job-offers";
    }   
    
    @RequestMapping(value = "manage-offer", method = RequestMethod.GET)
    public String getOffer(ModelMap model, @RequestParam int id) {
        Job offer = jobService.getJob(id);
        model.addAttribute(PAGE_TITLE_TAG, "Offer");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("offer",offer);
        return ADMINISTRATOR_LOCATION + "manage-job-offer";
    }
    
    private int getActivatedEnterprises(List<Enterprise> enterprises, String status){
        int no = 0;
        if(enterprises.isEmpty()){
            return no;
        }else{            
            for(Enterprise enterprise : enterprises){
                if(status.equals(enterprise.getStatus())){
                    no++;
                }
            }
            return no;
        }
    }
}
