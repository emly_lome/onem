/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.controller;

import com.onem.enterprise.exception.ResourceNotFoundException;
import com.onem.enterprise.form.ContactForm;
import com.onem.enterprise.service.JobSeekerService;
import com.onem.enterprise.service.PressReleaseService;
import com.onem.enterprise.util.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.onem.enterprise.util.StringUtil.PAGE_TITLE_TAG;
import static com.onem.enterprise.util.StringUtil.ONEM_LOCATION;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.Locale;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author ekasongo
 */
@Controller
public class DefaultController {
    
    @Autowired
    private PressReleaseService releaseService;

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact(ModelMap model, @RequestParam String msg) {
        model.addAttribute(PAGE_TITLE_TAG, "Contact");
        model.addAttribute("articles", releaseService.allNews());        
        model.addAttribute("message", new ContactForm());
        
        if(!msg.isEmpty()){
            for(String text : msg.split(";")){
                model.addAttribute("msg", text);
                break;
            }
        }
        return "contact";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Locale lang, ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "Home");
        model.addAttribute("articles", releaseService.allNews());
        return "index"; 
    }
    
    @RequestMapping(value = "/about-us", method = RequestMethod.GET)
    public String aboutUs(ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "about-us");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("message", new ContactForm());
        return ONEM_LOCATION + "about-us";
    }
    
    @RequestMapping(value = "/documentation", method = RequestMethod.GET)
    public String getDocumentation(ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "Documentation");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("message", new ContactForm());
        model.addAttribute("releases", releaseService.listAllDocumentation());
        return ONEM_LOCATION + "documentation";
    }

    @RequestMapping(value = "/regulation", method = RequestMethod.GET)
    public String regulation(ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "Regulation");
        model.addAttribute("articles", releaseService.allNews());
        return "regulation";
    }

    @RequestMapping(value = "/blog", method = RequestMethod.GET)
    public String blog(ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "Blog");
        model.addAttribute("articles", releaseService.allNews());
        return "blog";
    }
    
    @RequestMapping(value = "/policy-privacy", method = RequestMethod.GET)
    public String policyPrivacy(ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "policy privacy");
        model.addAttribute("articles", releaseService.allNews());
        return "policy-privacy";
    }
    
    @RequestMapping(value = "/terms-conditions", method = RequestMethod.GET)
    public String termsAndConditions(ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "Terms and conditions");
        model.addAttribute("articles", releaseService.allNews());
        return "terms-conditions";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String defaultPage(ModelMap model) {
        return "redirect:/index";
    }
    
    @ExceptionHandler(ResourceNotFoundException.class)
    public String getErrorPage(ModelMap model) {
        return "error/error";
    }
    
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String createAccount(ModelMap model,
                                @RequestParam String home){
        if(StringUtil.ENTERPRISE_USER.equals(home)){
            return "redirect:/register-enterprise.htm";
        }else{
            return "redirect:/register-job-seeker.htm";
        }
    }
}
