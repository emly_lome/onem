/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.controller;

import com.onem.enterprise.form.JobApplicationForm;
import com.onem.enterprise.form.JobForm;
import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.Job;
import com.onem.enterprise.model.JobOfferApplication;
import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.service.JobService;
import com.onem.enterprise.service.PressReleaseService;
import com.onem.enterprise.service.SectorService;
import com.onem.enterprise.util.StringUtil;
import static com.onem.enterprise.util.StringUtil.JOB_STATUS_PENDING;
import static com.onem.enterprise.util.StringUtil.JOB_STATUS_ACTIVE;
import static com.onem.enterprise.util.StringUtil.OFFER_LOCATION;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static com.onem.enterprise.util.StringUtil.PAGE_TITLE_TAG;
import static com.onem.enterprise.util.StringUtil.SESSION_TAG;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
/**
 *
 * @author ekasongo
 */
@Controller
@SessionAttributes(value = {"session", "userType", "jobApplicationForm", "jobForm"})
public class OfferController {
    
    @Autowired
    private JobService jobService;    
    @Autowired
    private SectorService sectorService;
    @Autowired
    private PressReleaseService releaseService;
    @Autowired
    private JavaMailSender mailSender;
    
    @ModelAttribute
    public JobForm createJobFormBean(){
        return new JobForm();
    }
    
    @ModelAttribute
    public JobApplicationForm createJobApplicationForm(){
        return new JobApplicationForm();
    }
    
    @RequestMapping(value = "offer", method = RequestMethod.GET)
    public String getOffer(ModelMap model, @RequestParam int id) {
        Job offer = jobService.getJob(id);
        model.addAttribute(PAGE_TITLE_TAG, "Offer");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("offer",offer);
        return OFFER_LOCATION + "offer";
    }
    
    @RequestMapping(value = "create-offer", method = RequestMethod.GET)
    public String createOffer(ModelMap model, @ModelAttribute JobForm jobForm) {
        model.addAttribute(PAGE_TITLE_TAG, "Create Job");
        model.addAttribute("articles", releaseService.allNews());
        model.put("jobForm", jobForm);
        model.addAttribute("sectors", sectorService.getAllAvailableSectors());
        return OFFER_LOCATION + "create-offer";
    }
    
    @RequestMapping(value = "save-job", method = RequestMethod.POST)
    public String saveJob(@Valid @ModelAttribute JobForm jobForm,
            BindingResult result, ModelMap model, HttpSession session) {
        
        Enterprise enterprise = (Enterprise) session.getAttribute(SESSION_TAG);
        
        if(enterprise == null){
            return "redirect:/login.htm?type=enterprise";
        }
        
        if(result.hasErrors()){
            System.out.println(result.getAllErrors());
        }
        Job job = jobForm.buildObject(enterprise);
        job.setStatus(JOB_STATUS_PENDING);
        jobService.recordJob(job);
        return "redirect:/enterprise-area.htm?page=1&id=" + enterprise.getId();
    }
    
    @RequestMapping(value = "apply", method = RequestMethod.GET)
    public String applyToOffer(ModelMap model, HttpSession session, 
                                @ModelAttribute JobApplicationForm jobApplicationForm,
                                @RequestParam int id){
        
        Object objectSession = session.getAttribute(SESSION_TAG);
        if(objectSession instanceof JobSeeker){
            Job job = jobService.getJob(id);
            model.addAttribute(PAGE_TITLE_TAG, "Apply to Job");
            model.addAttribute("articles", releaseService.allNews());
            model.addAttribute("job", job);
            model.addAttribute("form", jobApplicationForm
                                            .setTo(job.getEnterprise().getContactEmail())
                                            .setCc(((JobSeeker)objectSession).getContactEmail()));
        } else {
            return "redirect:/login.htm?home=jobseeker";
        }
        return OFFER_LOCATION + "apply";
    }
    
    @RequestMapping(value = "submit-application", method = RequestMethod.POST)
    public String submitApplication(@ModelAttribute final JobApplicationForm jobApplicationForm, ModelMap model,
            final @RequestParam CommonsMultipartFile cv, HttpSession session, @RequestParam int id){
        Object user = session.getAttribute(SESSION_TAG);
        if (user == null) {
            return "redirect:/login.htm";
        }
        mailSender.send(new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(
                        mimeMessage, true, "UTF-8");
                messageHelper.setTo(jobApplicationForm.getTo());
                messageHelper.setCc(jobApplicationForm.getCc());
                messageHelper.setSubject(jobApplicationForm.getSubject());
                messageHelper.setText(jobApplicationForm.getText());

                // determines if there is an upload file, attach it to the e-mail
                String attachName = cv.getOriginalFilename();
                if (!cv.isEmpty()) {

                    messageHelper.addAttachment(attachName, new InputStreamSource() {
                        @Override
                        public InputStream getInputStream() throws IOException {
                            return cv.getInputStream();
                        }
                    });
                }
            }
        });
        JobOfferApplication application = new JobOfferApplication();
        application.setJob(jobService.getJob(id));
        application.setApplicationTime(new Date());
        application.setJobSeeker((JobSeeker) user);
        application.setStatus("0");
        jobService.recordApplication(application);
        if (user instanceof JobSeeker) {
            return "redirect:/job-seeker-area.htm?msg=Your email has been sent"
                    + "&id=" + ((JobSeeker)user).getId();
        } else{
            return "redirect:/index.htm";
        }
    }
    
    @RequestMapping(value = "activate", method = RequestMethod.GET)
    public String activateOffer(@RequestParam int id){
        Job job = jobService.getJob(id);
        job.setStatus(JOB_STATUS_ACTIVE);
        jobService.recordJob(job);
        return "redirect:/manage-job-offers.htm";
    }
}
