/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.controller;

import com.onem.enterprise.form.EnterpriseForm;
import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.EnterpriseLogin;
import com.onem.enterprise.model.Job;
import com.onem.enterprise.service.EnterpriseService;
import com.onem.enterprise.service.JobService;
import com.onem.enterprise.service.PressReleaseService;
import com.onem.enterprise.util.PageUtil;
import static com.onem.enterprise.util.StringUtil.DEFAULT_PAGE;
import static com.onem.enterprise.util.StringUtil.ENTERPRISE_LOCATION;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.onem.enterprise.util.StringUtil.ENTERPRISE_USER;
import static com.onem.enterprise.util.StringUtil.PAGE_TITLE_TAG;
import static com.onem.enterprise.util.StringUtil.SESSION_TAG;
import static com.onem.enterprise.util.StringUtil.USER_TYPE_TAG;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author ekasongo
 */
@Controller
@SessionAttributes(value = {"session", "user", "enterpriseForm"})
public class EnterpriseController {
    
    @Autowired
    private EnterpriseService enterpriseService;
    
    @Autowired
    private PressReleaseService releaseService;
    
    @Autowired
    private JobService jobService;
    
    @ModelAttribute
    public EnterpriseForm init(){
        return new EnterpriseForm();
    }
    
    @RequestMapping(value = "enterprises", method = RequestMethod.GET)
    public String displayEnterprises(ModelMap model){
        model.put(PAGE_TITLE_TAG, "registered company");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("enterprises", enterpriseService.listAllEnterprises());
        return ENTERPRISE_LOCATION + "enterprises";
    }
    
    @RequestMapping(value = "enterprise", method = RequestMethod.GET)
    public String displayEnterpriseDetail(ModelMap model, @RequestParam int id){
        model.put(PAGE_TITLE_TAG, "enterprise");
        Enterprise enterprise = enterpriseService.getEnterprise(id);
        enterprise.setDescription(enterprise.getDescription().replace("?", "'"));
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("enterprise", enterprise);
        model.addAttribute("jobs", enterprise.getJobs());
        return ENTERPRISE_LOCATION + "enterprise";
    }
    
    @RequestMapping(value = "register-enterprise.htm", method = RequestMethod.GET)
    public String displayRegistrationForm(HttpServletRequest request, ModelMap model, 
                                            @ModelAttribute EnterpriseForm enterpriseForm){
        model.addAttribute(PAGE_TITLE_TAG, "Registration Enterprise");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("enterprise", enterpriseForm);
        List<String> errors = (List<String>) request.getSession().getAttribute("errors");
        model.addAttribute("errors", errors);
        return ENTERPRISE_LOCATION + "registration";
    }
    
    @RequestMapping(value = "enterprise-area", method = RequestMethod.GET)
    public String enterEnterpriseArea(ModelMap model, String msg, HttpSession session,
                                      @RequestParam int id, @RequestParam(required = false) Integer page) {
        Object activeSession = session.getAttribute(SESSION_TAG);
        if(activeSession == null){        
            model.addAttribute(USER_TYPE_TAG, ENTERPRISE_USER);            
        }
        if(page == null){
            page = DEFAULT_PAGE;
        }
        model.addAttribute(PAGE_TITLE_TAG, "enterprise");
        model.addAttribute("articles", releaseService.allNews());
        Enterprise enterprise = enterpriseService.getEnterprise(id);
        model.addAttribute(enterprise);
        model.addAttribute("offers", PageUtil.getPage(page, new ArrayList<Job>(enterprise.getJobs())));
        model.addAttribute("pages", PageUtil.size());
        model.addAttribute("selected", page);
        model.addAttribute("reports", jobService.listSectorsReport(enterprise));
        
        return ENTERPRISE_LOCATION + "enterprise-area";
    }
    
    @RequestMapping(value = "save-enterprise", method = RequestMethod.POST)
    public String saveEnterprise(ModelMap model, @Valid @ModelAttribute EnterpriseForm enterprise,
                                 BindingResult result, HttpSession session){
        List<String> errors = new ArrayList<String>();
        if(result.hasErrors()){            
            for(ObjectError error : result.getAllErrors()){
                errors.add(error.getDefaultMessage());
            }
            session.setAttribute("errors", errors);
            return "redirect:/register-enterprise.htm";
        }
        Enterprise obj = enterprise.buildObject(null);
        try {
            if(obj == null) {
                throw new RuntimeException("Failed to save Enterprise");
            }
            EnterpriseLogin login = new EnterpriseLogin();
            login.setUsername(enterprise.getUsername());
            login.setPassword(enterprise.getPassword());
            obj.setStatus("ACTIVE");
            login.setEntreprise(obj);
        
            enterpriseService.createEnterprise(login);
            session.setAttribute(SESSION_TAG, obj);
            session.setAttribute(USER_TYPE_TAG, ENTERPRISE_USER);
        } catch (RuntimeException e) {
            errors.add(e.getMessage());
            session.setAttribute("errors", errors);
            return "redirect:/register-enterprise.htm";
        }
        return "redirect:/enterprise-area.htm?page=1&id=" + obj.getId();
    }
}
