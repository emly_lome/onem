/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.controller;

import com.onem.enterprise.form.JobSeekerForm;
import com.onem.enterprise.form.PreferenceForm;
import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.model.JobSeekerFile;
import com.onem.enterprise.model.JobSeekerLogin;
import com.onem.enterprise.service.ConfigurationService;
import com.onem.enterprise.service.JobSeekerService;
import com.onem.enterprise.service.JobService;
import com.onem.enterprise.service.PressReleaseService;
import com.onem.enterprise.service.SectorService;
import com.onem.enterprise.util.DateUtil;
import com.onem.enterprise.util.FileUtil;
import com.onem.enterprise.util.ReferenceNoGenerator;
import static com.onem.enterprise.util.StringUtil.ENTERPRISE_LOCATION;
import static com.onem.enterprise.util.StringUtil.FILE_TYPE_CV;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static com.onem.enterprise.util.StringUtil.JOB_SEEKER_LOCATION;
import static com.onem.enterprise.util.StringUtil.JOB_SEEKER_USER;
import static com.onem.enterprise.util.StringUtil.PAGE_TITLE_TAG;
import static com.onem.enterprise.util.StringUtil.SESSION_TAG;
import static com.onem.enterprise.util.StringUtil.USER_TYPE_TAG;
import static com.onem.enterprise.util.StringUtil.JOB_STATUS_ACTIVE;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author ekasongo
 */
@Controller
@SessionAttributes(value = {"session", "user"})
public class JobSeekerController {

    @Autowired
    private JobSeekerService jobSeekerService;
    @Autowired
    private JobService jobService;
    @Autowired
    private ConfigurationService configService;
    @Autowired
    private SectorService sectorService;
    @Autowired
    private PressReleaseService releaseService;
    
    @RequestMapping(value = "candidates", method = RequestMethod.GET)
    public String getCandidates(ModelMap model) {
        model.addAttribute(PAGE_TITLE_TAG, "Candidates");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("candidates", jobSeekerService.listJobSeekers());
        model.addAttribute("no", jobSeekerService.listJobSeekers().size());
        model.addAttribute("reports", jobService.listSectorsReport());
        return ENTERPRISE_LOCATION + "candidates";
    }

    @RequestMapping(value = "register-job-seeker.htm", method = RequestMethod.GET)
    public String displayJobSeekerRegistrationForm(ModelMap model) {
        model.put(PAGE_TITLE_TAG, "Registration Job Seeker");
        model.addAttribute("articles", releaseService.allNews());
        JobSeekerForm form = new JobSeekerForm();
        int id = jobSeekerService.listJobSeekers().size() + 1;
        form.setRefno(ReferenceNoGenerator.getInstance().generateJobSeekerRef(id));
        model.put("form", form);
        return JOB_SEEKER_LOCATION + "registration";
    }

    @RequestMapping(value = "save-job-seeker", method = RequestMethod.POST)
    public String saveJobSeeker(@ModelAttribute JobSeekerForm form, ModelMap model,
            final @RequestParam CommonsMultipartFile cv, HttpSession session) throws IOException {
        int id = jobSeekerService.listJobSeekers().size() + 1;
        form.setRefno(ReferenceNoGenerator.getInstance().generateJobSeekerRef(id));
        JobSeeker seeker = form.buildObject(null);
        if (seeker == null) {

        }
        try {
            if(cv.isEmpty()){
                throw new RuntimeException("Pas de fichier attacher. Attacher votre cv!!");
            }
            if(!cv.getOriginalFilename().endsWith(".pdf")){
                throw new RuntimeException("Attacher un ficher pdf");
            }
            File file = FileUtil.createCandidateFile(seeker, cv.getOriginalFilename(), configService.getCandidatesFilesLocation());
            cv.transferTo(file);
            JobSeekerFile jobSeekerFile = new JobSeekerFile();
            jobSeekerFile.setName(file.getName());
            jobSeekerFile.setType(FILE_TYPE_CV);
            jobSeekerFile.setJobSeeker(seeker);
            jobSeekerFile.setDescription("test description");
            JobSeekerLogin login = new JobSeekerLogin(form.getEmail(), form.getPassword());
            login.setJobSeeker(seeker);
            jobSeekerService.createJobSeeker(login);
            jobSeekerService.addJobSeekerFile(jobSeekerFile);
            session.setAttribute(SESSION_TAG, seeker);
        } catch (RuntimeException e) {
            model.put(PAGE_TITLE_TAG, "Registration Job Seeker");
            model.addAttribute("articles", releaseService.allNews());
            model.addAttribute("error", e.getMessage());
            model.put("form", form);
            return JOB_SEEKER_LOCATION + "registration";
        }
        return "redirect:/job-seeker-area.htm?id=" + seeker.getId() + "&msg=You have been registered.";
    }

    @RequestMapping(value = "job-seeker-area", method = RequestMethod.GET)
    public String enterJobSeekerArea(ModelMap model, HttpSession session,
            @RequestParam(value = "msg", required = false) String msg,
            @RequestParam int id) {
        Object activeSession = session.getAttribute(SESSION_TAG);
        if (activeSession == null) {
            model.addAttribute(USER_TYPE_TAG, JOB_SEEKER_USER);
            return "redirect:/login.htm?home=jobseeker";
        }
        model.addAttribute(PAGE_TITLE_TAG, "Job Seeker Area");
        model.addAttribute("articles", releaseService.allNews());
        JobSeeker seeker = jobSeekerService.getJobSeeker(id);
        model.addAttribute("seeker", seeker);
        model.addAttribute("age", DateUtil.calculateAge(seeker.getDob()));
        model.addAttribute("preferences", seeker.getJobPreferences());
        model.addAttribute("jobs", jobService.listAvailableOffers(JOB_STATUS_ACTIVE));
        model.addAttribute("applications", seeker.getJobOfferApplications());
        model.addAttribute("reports", jobService.listSectorsReport());
        model.addAttribute("preferenceForm", new PreferenceForm());
        return JOB_SEEKER_LOCATION + "job-seeker-area";
    }

    @RequestMapping(value = "job-seeker", method = RequestMethod.GET)
    public String getCandidate(ModelMap model, @RequestParam int id) {
        model.addAttribute(PAGE_TITLE_TAG, "candidate");
        JobSeeker seeker = jobSeekerService.getJobSeeker(id);
        model.addAttribute(FILE_TYPE_CV, getCvFile(seeker));
        model.addAttribute("age", DateUtil.calculateAge(seeker.getDob()));
        model.addAttribute("reports", jobService.listSectorsReport());
        model.addAttribute("seeker", seeker);
        return JOB_SEEKER_LOCATION + "candidate";
    }

    private JobSeekerFile getCvFile(JobSeeker seeker) {
        JobSeekerFile file = null;
        Iterator<JobSeekerFile> it = seeker.getJobSeekerFiles().iterator();
        while (it.hasNext()) {
            file = it.next();
            if (FILE_TYPE_CV.equals(file.getType())) {
                break;
            } else {
                file = null;
            }
        }
        return file;
    }
}
