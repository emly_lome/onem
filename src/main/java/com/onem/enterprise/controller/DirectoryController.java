/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.controller;

import com.onem.enterprise.form.SearchJobForm;
import com.onem.enterprise.service.JobService;
import com.onem.enterprise.service.PressReleaseService;
import com.onem.enterprise.service.SectorService;
import com.onem.enterprise.util.PageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.onem.enterprise.util.StringUtil.*;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
/**
 *
 * @author ekasongo
 */
@Controller
public class DirectoryController {
    private final int SIZE = 4;
    private final Map pages = new HashMap();
    
    @Autowired
    private JobService jobService;
    
    @Autowired
    private SectorService sectorService;
    
    @Autowired
    private PressReleaseService releaseService;
    
    @RequestMapping(value = "directory", method = RequestMethod.GET)
    public String getDirectory(ModelMap model, 
                               @RequestParam(required = false) Integer page) {
        if(page == null){
            page = DEFAULT_PAGE;
        }
        model.addAttribute(PAGE_TITLE_TAG, "Directory");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("noOfOffersAvailable", jobService.listAvailableOffers(JOB_STATUS_ACTIVE).size());
        model.addAttribute("jobs", PageUtil.getPage(page, jobService.listAvailableOffers(JOB_STATUS_ACTIVE)));
        model.addAttribute("pages", PageUtil.size());
        model.addAttribute("selected", page);
        model.addAttribute("tags", jobService.listRegisterAlert());
        model.addAttribute("sectors", sectorService.getAllAvailableSectors());
        model.addAttribute("reports", jobService.listSectorsReport());
        model.addAttribute("search", new SearchJobForm());
        return "directory";
    }
    
    @RequestMapping(value = "sector-directory", method = RequestMethod.GET)
    public String getDirectoryBySector(ModelMap model, 
                                       @RequestParam(required = false) Integer page, 
                                       @RequestParam String sector) {
        if(page == null){
            page = DEFAULT_PAGE;
        }
        model.addAttribute(PAGE_TITLE_TAG, "Directory");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("jobs", PageUtil.getPage(page, jobService.listAvailableOffersBySector(sector,JOB_STATUS_ACTIVE)));
        model.addAttribute("pages", PageUtil.size());
        model.addAttribute("selected", page);
        model.addAttribute("tags", jobService.listRegisterAlert());
        model.addAttribute("sectors", sectorService.getAllAvailableSectors());
        model.addAttribute("reports", jobService.listSectorsReport());
        model.addAttribute("search", new SearchJobForm());
        return "directory";
    }
    
    @RequestMapping(value = "filter-offers", method = RequestMethod.POST)
    public String filterOffers(ModelMap model, 
                               @RequestParam(required = false) Integer page, 
                               @ModelAttribute SearchJobForm search){
        if(page == null){
            page = DEFAULT_PAGE;
        }
        model.addAttribute(PAGE_TITLE_TAG, "Directory");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("jobs", PageUtil.getPage(page, jobService.listFilteredOffers(search)));
        model.addAttribute("pages", PageUtil.size());
        model.addAttribute("selected", page);
        model.addAttribute("tags", jobService.listRegisterAlert());
        model.addAttribute("sectors", sectorService.getAllAvailableSectors());
        model.addAttribute("reports", jobService.listSectorsReport());
        model.addAttribute("search", search);
        return "directory";
    }
}
