/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.controller;

import com.onem.enterprise.form.LoginForm;
import com.onem.enterprise.model.Administrator;
import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.model.LoginResponse;
import com.onem.enterprise.service.EnterpriseService;
import com.onem.enterprise.service.JobSeekerService;
import com.onem.enterprise.service.PressReleaseService;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static com.onem.enterprise.util.StringUtil.*;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author ekasongo
 */
@Controller
@SessionAttributes(value = {"session", "user", "loginForm"})
public class LoginController {
    
    @Autowired
    private EnterpriseService enterpriseService;
    @Autowired
    private JobSeekerService jobSeekerService;
    @Autowired
    private PressReleaseService releaseService;
    
    @ModelAttribute
    public LoginForm createLoginForm(){
        LoginForm loginForm = new LoginForm();
        return loginForm;
    }
    
    @RequestMapping(value = "login.htm", method = RequestMethod.GET)
    public String login(HttpSession session, ModelMap model, 
                        @RequestParam(required = false) String home,
                        @RequestParam(required = false) String error,
                        @ModelAttribute LoginForm loginForm) {
        Object object = session.getAttribute(SESSION_TAG);
        if (object != null) {
            if (object instanceof Enterprise) {
                return "redirect:/enterprise-area.htm?page=1&id=" + ((Enterprise) object).getId();
            } else if (object instanceof JobSeeker) {
                return "redirect:/job-seeker-area.htm?id=" + ((JobSeeker)object).getId();
            }
        }
        
        model.addAttribute("home", home);
        model.addAttribute(PAGE_TITLE_TAG, "Login");
        model.addAttribute("articles", releaseService.allNews());
        model.addAttribute("loginForm", loginForm);
        model.addAttribute("error", error);
        return "login";
    }
    
    @RequestMapping(value = "private-area.htm", method = RequestMethod.POST)
    public String processLogin(@ModelAttribute LoginForm loginForm,@RequestParam String home, 
                                HttpServletRequest request, ModelMap model, HttpSession session) {
        LoginResponse response = enterpriseService.login(loginForm.getUsername(), loginForm.getPassword(), request.getLocale());
        if (response.isExist()) {
            if(response.getError() == null || response.getError().isEmpty()){
                Enterprise enterprise = enterpriseService.getEnterprise(loginForm.getUsername());
                session.setAttribute(SESSION_TAG, enterprise);
                session.setAttribute(USER_TYPE_TAG, ENTERPRISE_USER);
                return "redirect:/enterprise-area.htm?page=1&id=" + enterprise.getId();
            }else{
                return new StringBuilder().append("redirect:/login.htm?home=")
                                          .append(home).append("&error=").append(response.getError()).toString();
            }
        } 
        response = jobSeekerService.login(loginForm.getUsername(), loginForm.getPassword(), request.getLocale());
        if (response.isExist()) {
            if(response.getError() == null || response.getError().isEmpty()){
                JobSeeker jobSeeker = jobSeekerService.getJobSeeker(loginForm.getUsername());
                session.setAttribute(SESSION_TAG, jobSeeker);
                session.setAttribute(USER_TYPE_TAG, JOB_SEEKER_USER);
                return "redirect:/job-seeker-area.htm?id=" + jobSeeker.getId();
            }else{
                return new StringBuilder().append("redirect:/login.htm?home=")
                                          .append(home).append("&error=").append(response.getError()).toString();
            }
        } 
        if(loginForm.getUsername().equalsIgnoreCase("administrator") 
                && loginForm.getPassword().equals("password")){
            session.removeAttribute(SESSION_TAG);
            session.setAttribute(SESSION_TAG, new Administrator(loginForm.getUsername(), 
                                    loginForm.getPassword()));
            session.setAttribute(USER_TYPE_TAG, "administrator");
            return "redirect:/administrator.htm";
        }
        
        return new StringBuilder().append("redirect:/login.htm?home=")
                                  .append(home).append("&error=").append(response.getError()).toString();
    }
    
    @RequestMapping(value = "logout.htm", method = RequestMethod.GET)
    public String logout(HttpSession session, ModelMap model, SessionStatus status) {
        session.removeAttribute(SESSION_TAG);
        session.removeAttribute(USER_TYPE_TAG);
        status.setComplete();
        return "redirect:/index.htm";
    }
}
