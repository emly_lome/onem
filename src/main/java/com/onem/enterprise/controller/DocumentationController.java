/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.controller;

import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.service.ConfigurationService;
import com.onem.enterprise.service.JobSeekerService;
import com.onem.enterprise.util.FileUtil;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author ekasongo
 */
@Controller
public class DocumentationController {
    
    @Autowired
    private JobSeekerService jobSeekerService;
    
    @Autowired
    private ConfigurationService configurationService;
    
    @RequestMapping(value = "press-release", method = RequestMethod.GET)
    public String openPDF(@RequestParam String target, @RequestParam String type, 
                            ModelMap model, HttpSession session, HttpServletResponse response){
        try {
            String path = configurationService.getDocumentationLocation()
                    + File.separator + type + File.separator + target;
            byte[] contents = FileUtil.extractContent(path);
            response.setHeader("Content-Disposition", "inline; filename=\"" + target + "\"");
            response.setDateHeader("Expires", -1);
            response.setContentType("application/pdf");
            response.setContentLength(contents.length);
            response.getOutputStream().write(contents);
        } catch (IOException ex) {
            Logger.getLogger(DocumentationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @RequestMapping(value = "open-cv", method = RequestMethod.GET)
    public String openCV(@RequestParam int id, @RequestParam String file,
                            HttpServletResponse response){        
        try {
            JobSeeker seeker = jobSeekerService.getJobSeeker(id);
            String dir = seeker.getFirstName()+ "." + seeker.getLastName();
            String path = configurationService.getCandidatesFilesLocation() + dir + File.separator+ file;
            byte[] contents = FileUtil.extractContent(path);
            response.setHeader("Content-Disposition", "inline; filename=\"" + file + "\"");
            response.setDateHeader("Expires", -1);
            response.setContentType("application/pdf");
            response.setContentLength(contents.length);
            response.getOutputStream().write(contents);
        } catch (IOException ex) {
            Logger.getLogger(DocumentationController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
