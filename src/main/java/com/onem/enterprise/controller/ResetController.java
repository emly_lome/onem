/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.controller;

import com.onem.enterprise.form.RequestPasswordForm;
import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.model.JobSeekerLogin;
import com.onem.enterprise.service.ConfigurationService;
import com.onem.enterprise.service.EmailUtil;
import com.onem.enterprise.service.JobSeekerService;
import com.onem.enterprise.service.PressReleaseService;
import static com.onem.enterprise.util.StringUtil.PAGE_TITLE_TAG;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Lyn
 */
@Controller
public class ResetController {
    
    @Autowired
    private JobSeekerService jobSeekerService;
    @Autowired
    private PressReleaseService releaseService;
    @Autowired
    private ConfigurationService configurationService;
    @Autowired
    private EmailUtil emailUtil;
    
    @RequestMapping(value = "reset-password.htm", method = RequestMethod.POST)
    public String resetPassword(@ModelAttribute("emailForm") RequestPasswordForm emailForm, 
                    ModelMap model, HttpSession session) {
        
        String email = emailForm.getEmail();
        JobSeeker jobSeeker = jobSeekerService.getJobSeeker(email);
        String key = "";
        for (JobSeekerLogin login : jobSeeker.getJobSeekerLogins()) {
            if(login != null){
                key = login.getPassword();
                break;
            }
        }
        try {
            emailUtil.setTo(email);
            emailUtil.setSubject("Password reset");
            StringBuilder builder = new StringBuilder();
            builder.append("Please click on the link below to reset your password:")
                    .append("\n").append("Click <a href=\"http://")
                    .append(configurationService.getSystemDefaultUrl())
                    .append("/reset.htm?email=")
                    .append(email).append("&masterkey=").append(key)
                    .append("\" class=\"f-link--color\">here</a>").append("\n")
                    .append("Kind Regards,").append("\n")
                    .append("Administrator");
            emailUtil.setContactEmailText(builder.toString());
            emailUtil.send();
            
            return "redirect:/login.htm?home=jobseeker&msg=message sent successfuly";
        } catch (MessagingException ex) {
            Logger.getLogger(EmailController.class.getName()).log(Level.SEVERE, null, ex);
            return "redirect:/contact.htm?msg=message failed;error";    
        }
    }
    
    @RequestMapping(value = "forgot-password.htm", method = RequestMethod.GET)
    public String forgotUsernamePassword(ModelMap model){
        model.addAttribute(PAGE_TITLE_TAG, "Forgot");
        model.addAttribute("emailForm", new RequestPasswordForm());
        model.addAttribute("articles", releaseService.allNews());
        return "forgot";
    }
    
    @RequestMapping(value = "reset.htm", method = RequestMethod.GET)
    public String resetPassword(@RequestParam String email, @RequestParam String masterkey, 
                                    ModelMap model){
        model.addAttribute(PAGE_TITLE_TAG, "Reset");
        model.addAttribute("emailForm", new RequestPasswordForm());
        model.addAttribute("masterkey", masterkey);
        model.addAttribute("email", email);
        model.addAttribute("articles", releaseService.allNews());
        return "reset";
    }
    
    @RequestMapping(value = "reset.htm", method = RequestMethod.POST)
    public String resetPassword(@ModelAttribute("emailForm") RequestPasswordForm emailForm, 
                    ModelMap model, HttpSession session, @RequestParam String email, 
                    @RequestParam String masterkey) {
        
        JobSeeker jobSeeker = jobSeekerService.getJobSeeker(email);
        String key = "";
        for (JobSeekerLogin login : jobSeeker.getJobSeekerLogins()) {
            if(login != null){
                key = login.getPassword();
                if(key.equals(masterkey)){
                    login.setPassword(emailForm.getPassword());
                    jobSeekerService.updateJobSeekerLogin(login);
                }
                break;
            }
        }
        return "redirect:/login.htm?home=jobseeker&msg=message sent successfuly";
    }
}
