/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.form;

/**
 *
 * @author ekasongo
 */
public class SearchJobForm {
    
    private String type;
    private String location;
    private String sector;
    private String closing;
    private int id;

    public SearchJobForm() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getClosing() {
        return closing;
    }

    public void setClosing(String closing) {
        this.closing = closing;
    }

    public int getId() {
        return Integer.valueOf(sector.substring(0, 2).trim());
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
