/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.form;

import com.onem.enterprise.model.Enterprise;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ekasongo
 */
public class EnterpriseForm implements ObjectBuilder<Enterprise, Enterprise>{
    
    private final Enterprise enterprise;
    @Email
    @NotNull
    @NotEmpty(message = "Please enter a email address as username")
    private String username;
    
    @NotNull
    @Size(min = 6, max = 15)
    @NotEmpty(message = "Please enter a password")
    private String password;
    @NotNull
    @NotEmpty(message = "Please enter the company name")
    private String name;
    
    private String description;
    @NotNull
    @NotEmpty(message = "Please enter the address")
    private String address;
    
    @NotNull
    @NotEmpty(message = "Please enter the town")
    private String town;
    
    @NotNull
    @NotEmpty(message = "Please enter a contact name")
    private String contact;
    @NotNull
    @NotEmpty(message = "Please enter the contact job title")
    private String position;
    @Email
    @NotNull
    @NotEmpty(message = "Please enter the company contact email")
    private String email;
    @NotNull
    @NotEmpty(message = "Please enter the contact phone number")
    private String phone;
    
    public EnterpriseForm(){
        enterprise = new Enterprise();
    }

    @Override
    public Enterprise buildObject(Enterprise e) {
        return enterprise;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        enterprise.setName(name);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        enterprise.setDescription(description);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        enterprise.setAddress(address);
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
        enterprise.setTown(town);
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
        enterprise.setContactName(contact);
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
        enterprise.setContactPosition(position);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        enterprise.setContactEmail(email);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        enterprise.setContactNo(phone);
    }
    
}
