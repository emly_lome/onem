/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.form;

import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.Job;
import java.util.Date;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ekasongo
 */
public class JobForm implements ObjectBuilder<Job, Enterprise>{
    
    private final Job job;
    @NotNull
    @Size (max = 45, message = "Entered word up 45 letters")
    @NotEmpty(message = "Please enter a job title")
    private String title;
    
    @NotNull
    @NotEmpty(message = "Please select a job sector")
    private String sector;
    
    @NotNull
    @NotEmpty(message = "Please enter a job type")
    private String type;
    @NotNull
    @NotEmpty(message = "Please enter a location")
    private String location;
    @NotNull(message = "Please select a closing date")
    private Date closing;
    @NotNull
    @NotEmpty(message = "Please enter a description of the job")
    @Size (max = 5000, message = "Text limit exceded. Enter text up to 5000")
    private String description;
    @Size(max = 2000, message = "Text limit exceded. Enter text up to 2000")
    private String qualification;
    @Size(max = 2000, message = "Text limit exceded. Enter text up to 2000")
    private String experience;
    @NotNull(message = "Please enter a salary")
//    @NotEmpty(message = "Please enter a salary")
    private double salary;
    private String contact;
    
    public JobForm(){
        job = new Job();
    }

    @Override
    public Job buildObject(Enterprise e) {
        job.setEnterprise(e);
        return job;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        job.setTitle(title);
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
        job.setSector(sector);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        job.setType(type);
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
        job.setLocation(location);
    }

    public Date getClosing() {
        return closing;
    }

    public void setClosing(Date closing) {
        this.closing = closing;
        job.setClosingDate(closing);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        job.setDescription(description);
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
        job.setQualification(qualification);
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
        job.setExperience(experience);
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
        job.setSalary(salary);
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }    
}
