/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.form;

import com.onem.enterprise.model.JobSeeker;
import java.io.File;
import java.util.Date;

/**
 *
 * @author ekasongo
 */
public class JobSeekerForm implements ObjectBuilder<JobSeeker, Object>{
    
    private String email;
    private String password;
    private String title;
    private String firstname;
    private String lastname;
    private Date dob;
    private String gender;
    private String status;
    private String address;
    private String town;
    private String phone;
    private String refno;
    private String experience;
    private String qualification;
    private String description;
    private String maritalStatus;
    private final JobSeeker jobSeeker;

    public JobSeekerForm() {
        this.jobSeeker = new JobSeeker();
    }   

    @Override
    public JobSeeker buildObject(Object e) {
        return jobSeeker;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        jobSeeker.setContactEmail(email);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        jobSeeker.setTitle(title);
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
        jobSeeker.setFirstName(firstname);
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
        jobSeeker.setLastName(lastname);
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
        jobSeeker.setDob(dob);
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
        jobSeeker.setGender(gender);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        jobSeeker.setStatus(status);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        jobSeeker.setAddress(address);
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
        jobSeeker.setTown(town);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        jobSeeker.setContactNo(phone);
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
        jobSeeker.setRefno(refno);
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
        jobSeeker.setHighestQualification(qualification);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        jobSeeker.setDescription(description);
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        jobSeeker.setMaritalStatus(maritalStatus);
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
        jobSeeker.setYearsExperience(experience);
    }
    
}
