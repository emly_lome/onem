/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.form;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author ekasongo
 */
public class LoginForm {
    
    @NotNull
    @NotEmpty(message = "Please enter an username")
    private String username;
    @NotNull
    @NotEmpty(message = "Please enter a password")
    private String password;
    
    public LoginForm(){
        
    }
    
    public LoginForm(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public LoginForm setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public LoginForm setPassword(String password) {
        this.password = password;
        return this;
    }
    
    
}
