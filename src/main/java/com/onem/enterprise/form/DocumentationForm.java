/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.form;

import com.onem.enterprise.model.Documentation;

/**
 *
 * @author Lyn
 */
public class DocumentationForm implements ObjectBuilder<Documentation, Object>{
    
    private String name;
    private String description;
    private String type;
    private String filename;
    private final Documentation documentation;
    
    public DocumentationForm(){
        documentation = new Documentation();
    }

    @Override
    public Documentation buildObject(Object e) {
        return documentation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        documentation.setName(name);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        documentation.setDescription(description);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        
        if("Règlement".equals(type)){
            this.type = "Regulations";
        }else if("Discours".equals(type)){
            this.type = "Speech";
        }else if("Decrets".equals(type)){
            this.type = "Decret";
        }else {
            this.type = type;
        }
        documentation.setType(this.type);
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
        documentation.setFileName(filename);
    }    
}
