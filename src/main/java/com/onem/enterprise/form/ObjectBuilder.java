/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.form;

/**
 *
 * @author ekasongo
 */
public interface ObjectBuilder<T,E> {
    public T buildObject(E e);
}
