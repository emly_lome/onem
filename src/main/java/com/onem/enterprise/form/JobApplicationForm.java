/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.form;

/**
 *
 * @author Lyn
 */
public class JobApplicationForm {
    
    private String to;
    private String cc;
    private String text;
    private String subject;

    public JobApplicationForm() {
    }

    public JobApplicationForm(String to, String cc, String subject) {
        this.to = to;
        this.cc = cc;
        this.subject = subject;
    }

    public String getTo() {
        return to;
    }

    public JobApplicationForm setTo(String to) {
        this.to = to;
        return this;
    }

    public String getCc() {
        return cc;
    }

    public JobApplicationForm setCc(String cc) {
        this.cc = cc;
        return this;
    }

    public String getText() {
        return text;
    }

    public JobApplicationForm setText(String text) {
        this.text = text;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public JobApplicationForm setSubject(String subject) {
        this.subject = subject;
        return this;
    }
    
}
