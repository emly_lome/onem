/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service;

import com.onem.enterprise.model.JobOfferApplication;
import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.model.JobSeekerFile;
import com.onem.enterprise.model.JobSeekerLogin;
import java.util.List;

/**
 *
 * @author ekasongo
 */
public interface JobSeekerService extends LoginService{
    
    public List<JobSeeker> listJobSeekers();
    public boolean loginUser(String username, String password);
    public JobSeeker getJobSeeker(int id);
    public JobSeeker getJobSeeker(String username);    
    public void createJobSeeker(JobSeekerLogin login);
    public void addJobSeekerFile(JobSeekerFile file);
    public void editJobSeeker(JobSeeker seeker);
    public List<JobOfferApplication> listJobApplication();
    public void updateJobSeekerLogin(JobSeekerLogin login);
}
