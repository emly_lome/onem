/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service;

import com.onem.enterprise.form.SearchJobForm;
import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.Job;
import com.onem.enterprise.model.JobAlert;
import com.onem.enterprise.model.JobOfferApplication;
import com.onem.enterprise.model.SectorReport;
import java.util.List;

/**
 *
 * @author ekasongo
 */
public interface JobService {
    
    public static final int TYPE_SEARCH = 1;
    public static final int LOCATION_SEARCH = 2;
    public static final int CONTRACT_SEARCH = 3;
    
    public int getNumberOfApplication();
    
    public void recordApplication(JobOfferApplication application);
    
    public Job getJob(int id);
    
    public void recordJob(Job job);
    
    public void recordJobAlert(JobAlert job);
    
    public void removeJobAlert(int id);
    
    public List<Job> listAvailableOffers();
    
    public List<Job> listAvailableOffers(String status);
    
    public List<Job> listAvailableOffers(String search, List<Job> offers, int searchType);
    
    public List<Job> listAvailableOffersBySector(String sector, String status);
    
    public List<Job> listFilteredOffers(SearchJobForm search);
    
    public List<JobAlert> listRegisterAlert();
    
    public List<SectorReport> listSectorsReport();
    
    public List<SectorReport> listSectorsReport(Enterprise enterprise);
}
