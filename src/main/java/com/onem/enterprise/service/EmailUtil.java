/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service;

import com.onem.enterprise.model.Job;
import java.io.File;
import javax.mail.MessagingException;

/**
 *
 * @author ekasongo
 */
public interface EmailUtil {
    
     public void setTo(String dest, boolean multi) throws MessagingException;
    
    public void setTo(String dest) throws MessagingException;
    
    public void setCc(String dest) throws MessagingException;
    
    public void setJobEmailText(String destinatorName, Job offer) throws MessagingException;
    
    public void setContactEmailText(String text) throws MessagingException;
    
    public void setSubject(String subjet) throws MessagingException;
    
    public void setAttachment(File attachFile) throws MessagingException;
    
    public void send();
}
