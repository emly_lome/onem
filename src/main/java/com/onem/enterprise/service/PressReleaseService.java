/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service;

import com.onem.enterprise.model.Documentation;
import com.onem.enterprise.model.Article;
import java.util.List;

/**
 *
 * @author ekasongo
 */
public interface PressReleaseService {
    
    public List<Documentation> listDocumentationByType(String type);
    
    public List<Documentation> listAllDocumentation();
    
    public void addDocument(Documentation document);
    
    public void deleteDocument(int id);
    
    public List<Article> allNews();
}
