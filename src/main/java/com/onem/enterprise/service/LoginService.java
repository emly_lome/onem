/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.service;

import com.onem.enterprise.model.LoginResponse;
import java.util.Locale;

/**
 *
 * @author Lyn
 */
public interface LoginService {
    
    public LoginResponse login(String username, String password, Locale locale);
    
}
