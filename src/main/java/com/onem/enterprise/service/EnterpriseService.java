/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service;

import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.EnterpriseLogin;
import com.onem.enterprise.model.Job;
import java.util.List;

/**
 *
 * @author ekasongo
 */
public interface EnterpriseService extends LoginService{
    
    public boolean loginEnterprise(String username, String password);    
    public void recordEnterprise(Enterprise enterprise, EnterpriseLogin login);    
    public Enterprise getEnterprise(int id);
    public Enterprise getEnterprise(String username);
    public List<Enterprise> listAllEnterprises();
    public void createEnterprise(EnterpriseLogin login);
    public List<Job> listOfOffers(Enterprise enterprise);
    public List<Job> listOfOffers();
    public void updateEnterprise(Enterprise enterprise);
}
