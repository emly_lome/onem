/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service;

import com.onem.enterprise.model.Configuration;

/**
 *
 * @author ekasongo
 */
public interface ConfigurationService {
    
    public final String ADMIN_EMAIL = "admin.email";
    public final String CANDIDATES_FILES = "candidates.files";
    public final String DOCUMENT_LOCATION = "document.location";
    public final String DEFAULT_URL = "default.url";
    
    public String getAdminEmail();
    
    public String getDocumentationLocation();
    
    public String getCandidatesFilesLocation();
    
    public String getSystemDefaultUrl();
    
    public Configuration getConfiguration(String key);
    
}
