/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service.impl;

import com.onem.enterprise.model.JobOfferApplication;
import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.model.JobSeekerFile;
import com.onem.enterprise.model.JobSeekerLogin;
import com.onem.enterprise.model.LoginResponse;
import com.onem.enterprise.repository.JobOfferApplicationRepository;
import com.onem.enterprise.repository.JobSeekerFileRepositor;
import com.onem.enterprise.repository.JobSeekerLoginRepository;
import com.onem.enterprise.repository.JobSeekerRepository;
import com.onem.enterprise.service.JobSeekerService;
import com.onem.enterprise.util.MessageUtil;
import com.onem.enterprise.util.PasswordGeneratorUtil;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ekasongo
 */
@Service("jobSeekerService")
public class JobSeekerServiceImpl implements JobSeekerService{
    
    @Autowired
    private JobSeekerRepository jobSeekerRepository;
    @Autowired
    private JobSeekerLoginRepository jobSeekerLoginRepository;
    @Autowired
    private PasswordGeneratorUtil passwordGeneratorUtil;    
    @Autowired
    private JobSeekerFileRepositor jobSeekerFileRepository;
    @Autowired
    private JobOfferApplicationRepository applicationRepository;
    
    @Override
    public List<JobSeeker> listJobSeekers() {
        return jobSeekerRepository.findAll();
    }

    @Override
    public boolean loginUser(String username, String password) {
        if(username != null){
            JobSeekerLogin login = jobSeekerLoginRepository.findUserByUsername(username);
            if(login != null){
                String current = login.getPassword();
                return passwordGeneratorUtil.matchPassword(current, password);
            }
        }
        return false;
    }

    @Override
    public JobSeeker getJobSeeker(int id) {
        return jobSeekerRepository.findOne(id);
    }

    @Override
    public JobSeeker getJobSeeker(String username) {
        return jobSeekerLoginRepository.findUserByUsername(username).getJobSeeker();
    }

    @Override
    @Transactional
    public void createJobSeeker(JobSeekerLogin login) {
        if(login != null){
            JobSeeker seeker = login.getJobSeeker();
            if(seeker != null){
                jobSeekerRepository.saveAndFlush(seeker);
            }
            login.setJobSeeker(seeker);
            login.setPassword(encryptPassword(login.getPassword()));
            jobSeekerLoginRepository.saveAndFlush(login);
        }
    }
    
    private String encryptPassword(String password){
        return passwordGeneratorUtil.encryptPassword(password);
    }

    @Override
    public void editJobSeeker(JobSeeker seeker) {
        jobSeekerRepository.save(seeker);
    }

    @Override
    public void addJobSeekerFile(JobSeekerFile file) {
        jobSeekerFileRepository.saveAndFlush(file);
    }

    @Override
    public List<JobOfferApplication> listJobApplication() {
        return applicationRepository.findAll();
    }

    @Override
    @Transactional
    public void updateJobSeekerLogin(JobSeekerLogin login) {
        if(login != null && !login.getPassword().isEmpty()){
            login.setPassword(encryptPassword(login.getPassword()));
            jobSeekerLoginRepository.save(login);
        }
    }

    @Override
    public LoginResponse login(String username, String password, Locale locale) {
        LoginResponse response = new LoginResponse();   
        response.setType(LoginResponse.LOGIN_TYPE.JOB_SEEKER);
        JobSeekerLogin login = jobSeekerLoginRepository.findUserByUsername(username);
        if(login != null){
            String current = login.getPassword();
            response.setExist(true);
            if(!passwordGeneratorUtil.matchPassword(current, password)){
                response.setError(MessageUtil.getInstance().getMessage(MessageUtil.USERNAME_AND_PASSWORD_NOT_MATCHING_MESSAGE, locale));
            }
        }else{
            response.setExist(false);
            response.setError(MessageUtil.getInstance().getMessage(MessageUtil.USER_NOT_FOUND_MESSAGE, locale));
        }
        return response;
    }
}
