/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service.impl;

import com.onem.enterprise.exception.ExistException;
import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.EnterpriseLogin;
import com.onem.enterprise.model.Job;
import com.onem.enterprise.model.LoginResponse;
import com.onem.enterprise.repository.EnterpriseLoginRepository;
import com.onem.enterprise.repository.EnterpriseRepository;
import com.onem.enterprise.repository.JobRepository;
import com.onem.enterprise.service.EnterpriseService;
import com.onem.enterprise.util.MessageUtil;
import com.onem.enterprise.util.PasswordGeneratorUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ekasongo
 */
@Service("enterpriseService")
public class EnterpriseServiceImpl implements EnterpriseService{
    
    @Autowired
    private EnterpriseLoginRepository enterpriseLoginRepository;
    
    @Autowired
    private EnterpriseRepository enterpriseRepository;
    
    @Autowired
    private PasswordGeneratorUtil passwordGeneratorUtil;
    
    @Autowired
    private JobRepository jobRepository;

    @Override
    public boolean loginEnterprise(String username, String password) {
        
        EnterpriseLogin enterpriseLogin = enterpriseLoginRepository.getEnterpriseLogin(username);
        if(enterpriseLogin != null){
            String current = enterpriseLogin.getPassword();
            return passwordGeneratorUtil.matchPassword(current, password);
        }
        return false;
    }

    @Override
    @Transactional
    public void recordEnterprise(Enterprise enterprise, EnterpriseLogin login) {
        if(enterprise != null && login != null){
            int result = enterpriseRepository.checkEnterpriseExist(enterprise.getName());
            if(result > 0){
                throw new ExistException("Enterprise already exist with name: " + enterprise.getName());
            }
            enterprise = enterpriseRepository.saveAndFlush(enterprise);
            login.setEntreprise(enterprise);
            enterpriseLoginRepository.saveAndFlush(login);
        }
    }

    @Override
    public Enterprise getEnterprise(int id) {
        return enterpriseRepository.findOne(id);
    }

    @Override
    public List<Enterprise> listAllEnterprises() {
        return enterpriseRepository.findAll();
    }

    @Override
    public Enterprise getEnterprise(String username) {
        EnterpriseLogin enterpriseLogin = enterpriseLoginRepository.getEnterpriseLogin(username);
        if(enterpriseLogin != null){
            return enterpriseLogin.getEntreprise();
        }
        return null;
    }

    @Override
    @Transactional
    public void createEnterprise(EnterpriseLogin login) {
        if(login != null){
            Enterprise enterprise = login.getEntreprise();
            if(enterprise != null){
                enterpriseRepository.saveAndFlush(enterprise);
            }
            login.setEntreprise(enterprise);
            login.setPassword(encryptPassword(login.getPassword()));
            enterpriseLoginRepository.save(login);
        }
    }    
    
    private String encryptPassword(String password){
        return passwordGeneratorUtil.encryptPassword(password);
    }

    @Override
    public List<Job> listOfOffers(Enterprise enterprise) {
        List<Job> offers = new ArrayList<Job>();
        if(enterprise != null){

        }
        return offers;
    }

    @Override
    public List<Job> listOfOffers() {
        return jobRepository.findAll();
    }

    @Override
    @Transactional
    public void updateEnterprise(Enterprise enterprise) {
        if(enterprise != null){
            enterpriseRepository.save(enterprise);
        }
    }

    @Override
    public LoginResponse login(String username, String password, Locale locale) {
        LoginResponse response = new LoginResponse();
        response.setType(LoginResponse.LOGIN_TYPE.ENTREPRISE);
        EnterpriseLogin enterpriseLogin = enterpriseLoginRepository.getEnterpriseLogin(username);
        if(enterpriseLogin != null){
            String current = enterpriseLogin.getPassword();
            response.setExist(true);
            if(!passwordGeneratorUtil.matchPassword(current, password)){
                response.setError(MessageUtil.getInstance().getMessage(MessageUtil.USERNAME_AND_PASSWORD_NOT_MATCHING_MESSAGE, locale));
            }
        }else{
            response.setExist(false);
            response.setError(MessageUtil.getInstance().getMessage(MessageUtil.USER_NOT_FOUND_MESSAGE, locale));
        }
        return response;
    }
}
