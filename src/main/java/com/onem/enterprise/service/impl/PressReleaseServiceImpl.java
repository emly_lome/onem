/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service.impl;

import com.onem.enterprise.model.Documentation;
import com.onem.enterprise.model.Article;
import com.onem.enterprise.repository.ArticleRepository;
import com.onem.enterprise.repository.DocumentationRepository;
import com.onem.enterprise.service.PressReleaseService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ekasongo
 */
@Service
public class PressReleaseServiceImpl implements PressReleaseService{
    
    @Autowired
    private DocumentationRepository documentationRepository;
    
    @Autowired
    private ArticleRepository newsRepository;

    @Override
    public List<Documentation> listDocumentationByType(String type) {
        List<Documentation> results = new ArrayList<Documentation>();
        List<Documentation> docs = documentationRepository.findAll();
        if(type != null && !type.isEmpty()){
            Iterator<Documentation> it = docs.iterator();
            while(it.hasNext()){
                Documentation doc = it.next();
                if (doc.getType().contains(type)) {
                    results.add(doc);
                }                        
            }
        }
        return results;
    }

    @Override
    public List<Documentation> listAllDocumentation() {
        return documentationRepository.findAll();
    }

    @Override
    @Transactional
    public void addDocument(Documentation document) {
        documentationRepository.save(document);
    }

    @Override
    @Transactional
    public void deleteDocument(int id) {
        documentationRepository.delete(id);
    }

    @Override
    public List<Article> allNews() {
        return newsRepository.findAll();
    }    
}
