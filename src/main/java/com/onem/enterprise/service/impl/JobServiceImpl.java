/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.service.impl;

import com.onem.enterprise.form.SearchJobForm;
import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.Job;
import com.onem.enterprise.model.JobAlert;
import com.onem.enterprise.model.JobOfferApplication;
import com.onem.enterprise.model.Sector;
import com.onem.enterprise.model.SectorReport;
import com.onem.enterprise.repository.JobAlterRepository;
import com.onem.enterprise.repository.JobOfferApplicationRepository;
import com.onem.enterprise.repository.JobRepository;
import com.onem.enterprise.repository.SectorRepository;
import com.onem.enterprise.service.JobService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ekasongo
 */
@Service("jobService")
public class JobServiceImpl implements JobService {

    @Autowired
    private JobRepository jobRepository;
    @Autowired
    private JobAlterRepository jobAlterRepository;
    @Autowired
    private JobOfferApplicationRepository jobOfferApplicationRepository;
    @Autowired
    private SectorRepository sectorRepository;

    @Override
    public int getNumberOfApplication() {
        return jobOfferApplicationRepository.findAll().size();
    }

    @Override
    public void recordApplication(JobOfferApplication application) {
        jobOfferApplicationRepository.saveAndFlush(application);
    }
    
    
    @Override
    public List<SectorReport> listSectorsReport(Enterprise enterprise) {
        List<String> sectors = jobRepository.findAvailableSectors();
        Map<String, Integer> report = new HashMap<String, Integer>();
        for (String sector : sectors) {
            report.put(sector, 0);
        }
        for (String sector : sectors) {
            for (Job job : jobRepository.findAvailableJobByEnterprise(enterprise)) {
                if (sector.equals(job.getSector())) {
                    Integer no = report.get(sector) + 1;
                    report.put(sector, no);
                }
            }
        }

        List<SectorReport> reports = new ArrayList<SectorReport>();
        Iterator<Map.Entry<String, Integer>> it = report.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            int no = (Integer) entry.getValue();
            reports.add(new SectorReport(((String) entry.getKey()), no));
        }
        return reports;
    }
    @Override
    public List<SectorReport> listSectorsReport() {
        List<String> sectors = jobRepository.findAvailableSectors();
        Map<String, Integer> report = new HashMap<String, Integer>();
        for (String sector : sectors) {
            report.put(sector, 0);
        }
        for (String sector : sectors) {
            for (Job job : jobRepository.findAll()) {
                if (sector.equals(job.getSector())) {
                    Integer no = report.get(sector) + 1;
                    report.put(sector, no);
                }
            }
        }

        List<SectorReport> reports = new ArrayList<SectorReport>();
        Iterator<Map.Entry<String, Integer>> it = report.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = it.next();
            int no = (Integer) entry.getValue();
            reports.add(new SectorReport(((String) entry.getKey()), no));
        }
        return reports;
    }

    @Override
    @Transactional
    public void recordJob(Job job) {
        if (job != null) {
            jobRepository.saveAndFlush(job);
        }
    }

    @Override
    public List<Job> listAvailableOffers() {
        return jobRepository.findAll();
    }

    @Override
    public List<Job> listAvailableOffers(String status) {
        if(status != null && !status.isEmpty()){
            return jobRepository.findAvailableJobByStatus(status);
        }else{
            return new ArrayList<Job>();
        }
    }

    @Override
    public Job getJob(int id) {
        return jobRepository.findOne(id);
    }

    @Override
    public List<Job> listAvailableOffers(String search, List<Job> offers, int searchType) {
        Iterator<Job> it = offers.iterator();
        while (it.hasNext()) {
            Job offer = it.next();
            switch (searchType) {
                case TYPE_SEARCH:
                    if (!offer.getTitle().toLowerCase().contains(search.toLowerCase())) {
                        it.remove();
                    }
                    break;
                case LOCATION_SEARCH:
                    if (!offer.getLocation().toLowerCase().contains(search.toLowerCase())) {
                        it.remove();
                    }
                    break;
                case CONTRACT_SEARCH:
                    if (!offer.getType().toLowerCase().contains(search.toLowerCase())) {
                        it.remove();
                    }
                    break;
                default:
                    break;
            }
        }
        return offers;
    }

    @Override
    @Transactional
    public void recordJobAlert(JobAlert job) {
        jobAlterRepository.saveAndFlush(job);
    }

    @Override
    @Transactional
    public void removeJobAlert(int id) {
        jobAlterRepository.delete(id);
    }

    @Override
    public List<JobAlert> listRegisterAlert() {
        return jobAlterRepository.findAll();
    }

    @Override
    public List<Job> listAvailableOffersBySector(String sector, String status) {
        List<Job> jobs = new ArrayList<Job>();
        for (Job job : jobRepository.findAvailableJobByStatus(status)) {
            if (sector.equals(job.getSector())) {
                jobs.add(job);
            }
        }
        return jobs;
    }

    @Override
    public List<Job> listFilteredOffers(SearchJobForm search) {
        List<Job> jobs = new ArrayList<Job>();
        for (Job job : jobRepository.findAll()) {
            boolean match = false;
            if (search.getType().isEmpty()) {
                match = true;
            } else {
                if (search.getType().equalsIgnoreCase(job.getType())) {
                    match = true;
                }
            }
            if (match && !search.getLocation().isEmpty() && !search.getLocation().equalsIgnoreCase(job.getLocation())) {
                match = false;
            }
            Sector sector = sectorRepository.findOne(search.getId());
            if (match && !search.getSector().isEmpty() && !sector.getName().trim().equalsIgnoreCase(job.getSector().trim())) {
                match = false;
            }
            if(match){
                jobs.add(job);
            }
        }
        return jobs;
    }
}
