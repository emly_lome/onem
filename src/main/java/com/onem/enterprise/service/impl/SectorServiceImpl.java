/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service.impl;

import com.onem.enterprise.model.Sector;
import com.onem.enterprise.repository.SectorRepository;
import com.onem.enterprise.service.SectorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ekasongo
 */
@Service
public class SectorServiceImpl implements SectorService{
    
    @Autowired
    private SectorRepository sectorRepository;

    @Override
    public List<Sector> getAllAvailableSectors() {
        return sectorRepository.findAll();
    }
    
}
