/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service.impl;

import com.onem.enterprise.model.Job;
import com.onem.enterprise.service.EmailUtil;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import static com.onem.enterprise.util.StringUtil.*;
import java.io.File;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

/**
 *
 * @author ekasongo
 */
@Service
public class EmailUtilImpl implements EmailUtil{
    
    @Autowired
    private JavaMailSender mailSender;    
    private MimeMessage message;    
    private MimeMessageHelper helper;
    private StringBuilder textMsg;
    
    @Override
    public void setTo(String dest, boolean multi) throws MessagingException{
        message = mailSender.createMimeMessage();
        helper = new MimeMessageHelper(message, multi, "UTF-8");
        textMsg = new StringBuilder(EMAIL_OPEN_HTML_TAG);
        helper.setTo(dest);
     }
    
    @Override
    public void setTo(String dest) throws MessagingException{
        setTo(dest, false);
   }
    
    @Override
    public void setJobEmailText(String destinatorName, Job offer) throws MessagingException{
        textMsg.append(EMAIL_OPEN_PARAGRAPHE.replace(EMAIL_PARAGRAPHE, destinatorName));
        String text = offer.getDescription();
        String jobEmail = EMAIL_JOB_ALERT.replace(JOB_LOCATION, offer.getLocation())
                            .replace(ENTERPRISE_NAME, offer.getEnterprise().getName())
                            .replace(JOB_SALARY, offer.getSalary().toString())
                            .replace(OFFER_END_DATE, offer.getClosingDate().toString());
        if(isNotNullOrEmpty(text)){
            String[] pars = text.split("\n");
            StringBuilder description = new StringBuilder();
            for(String par : pars){
                if(!par.isEmpty()){
                    description.append(EMAIL_OPEN_PARAGRAPHE.replace(EMAIL_PARAGRAPHE, par));
                }
            }
            jobEmail = jobEmail.replace(JOB_DESCRIPTION, description.toString());
            textMsg.append(jobEmail);
        }
        textMsg.append(EMAIL_CLOSING_SIGNATURE).append(EMAIL_CLOSE_HTML_TAG);
        helper.setText(textMsg.toString(), true);
    }
    
    
    
    @Override
    public void send(){
        mailSender.send(message);
    }

    @Override
    public void setSubject(String subjet) throws MessagingException {
        helper.setSubject(subjet);
    }

    @Override
    public void setContactEmailText(String text) throws MessagingException {
        if(isNotNullOrEmpty(text)){
            String[] pars = text.split("\n");
            for(String par : pars){
                if(!par.isEmpty()){
                     textMsg.append(EMAIL_OPEN_PARAGRAPHE.replace(EMAIL_PARAGRAPHE, par));
                }
            }
        }
        textMsg.append(EMAIL_CLOSE_HTML_TAG);
        helper.setText(textMsg.toString(), true);
    }

    @Override
    public void setAttachment(File attachFile) throws MessagingException {
        if(attachFile != null){
            FileSystemResource file = new FileSystemResource(attachFile);
            helper.addAttachment(attachFile.getName(), file);
        }
    }

    @Override
    public void setCc(String dest) throws MessagingException {
        helper.setCc(dest);
    }
}
