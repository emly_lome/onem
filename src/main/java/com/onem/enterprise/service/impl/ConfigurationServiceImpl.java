/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service.impl;

import com.onem.enterprise.model.Configuration;
import com.onem.enterprise.repository.ConfigurationRepository;
import com.onem.enterprise.service.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ekasongo
 */
@Service
public class ConfigurationServiceImpl implements ConfigurationService{
    
    @Autowired
    private ConfigurationRepository configurationRepository;

    @Override
    public String getAdminEmail() {
        Configuration config = getConfiguration(ADMIN_EMAIL);
        return config != null ? config.getValue() : "";
    }

    @Override
    public Configuration getConfiguration(String key) {
        return configurationRepository.findConfigurationByName(key);
    }

    @Override
    public String getDocumentationLocation() {
        Configuration config = getConfiguration(DOCUMENT_LOCATION);
        return config != null ? config.getValue() : "";
    }

    @Override
    public String getCandidatesFilesLocation() {
        Configuration config = getConfiguration(CANDIDATES_FILES);
        return config != null ? config.getValue() : "";
    }

    @Override
    public String getSystemDefaultUrl() {
        Configuration config = getConfiguration(DEFAULT_URL);
        return config != null ? config.getValue() : "";
    }   
}
