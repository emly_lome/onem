/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.service;

import com.onem.enterprise.model.Sector;
import java.util.List;

/**
 *
 * @author ekasongo
 */
public interface SectorService {
    public List<Sector> getAllAvailableSectors();
}
