/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.util;

import com.onem.enterprise.model.JobSeeker;
import com.onem.enterprise.service.ConfigurationService;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ekasongo
 */
public class FileUtil {

    public static byte[] extractContent(String path) throws FileNotFoundException, IOException {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        try {
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum);
            }
        } catch (IOException ex) {
            Logger.getLogger(FileUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bos.toByteArray();
    }
    
    public static File createCandidateFile(JobSeeker seeker, String name, String location) throws IOException{
        File dir = new File(location + seeker.getFirstName() + "." + seeker.getLastName());
        if(!dir.exists()){
            dir.mkdir();
        }
        File file = new File(dir, name);
        if(!file.exists()){
            file.createNewFile();
        }
        return file;
    }
    
    public static File createDocumentationFile(String name, String type, String location) throws IOException{
        File dir = new File(location + type);
        if(!dir.exists()){
            dir.mkdir();
        }
        File file = new File(dir, name);
        if(!file.exists()){
            file.createNewFile();
        }
        return file;
    }
}
