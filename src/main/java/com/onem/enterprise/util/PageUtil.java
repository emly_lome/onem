/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.util;

import com.onem.enterprise.model.Job;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author ekasongo
 */
public class PageUtil {
    
    public static final int SIZE = 4;
    private static final Map pages = new HashMap();    
    
    public static List<Job> getPage(int page, List<Job> list){ 
        int insert = 1;
        List<Job> jobs = new ArrayList<Job>();
        for(int i = 0;i<list.size(); i++) {
            jobs.add(list.get(i));
            if(i != 0 && i % SIZE == 0){
                pages.put(new Integer(insert), jobs);
                insert++;
                jobs = new ArrayList<Job>();
            } else if ((list.size() - i) < SIZE){
                pages.put(new Integer(insert), jobs);
            }
        }
        return (List<Job>) pages.get(new Integer(page));
    }
    
    public static int size(){
        return pages.size();
    }
    
}
