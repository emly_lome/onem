/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ekasongo
 */
public class StringUtil {    
    
    public static final int DEFAULT_PAGE = 1;
    public static final String ENTERPRISE_STATUS_ACTIVATED = "activated";
    public static final String ENTERPRISE_STATUS_DESACTIVATED = "deactivated";
    
    public static final String JOB_STATUS_PENDING = "PENDING";
    public static final String JOB_STATUS_ACTIVE = "ACTIVE";
    public static final String JOB_STATUS_EXPIRED = "EXPIRED";
    
    public static final int ACTIVATE = 1;
    public static final int DEACTIVATE = 0;
    public static final String STATUS = "status";
    
    public static final String ENTERPRISE_USER = "enterprise";
    public static final String JOB_SEEKER_USER = "job-seeker";
    public static final String ADMINISTRATOR_USER = "administrator";
    
    public static final String FILE_TYPE_CV = "cv";
    
    public static final String ENTERPRISE_LOCATION = "enterprise/";
    public static final String JOB_SEEKER_LOCATION = "jobseeker/";
    public static final String EMAIL_LOCATION = "email/";
    public static final String ADMINISTRATOR_LOCATION = "admin/";
    public static final String PUBLICATION_LOCATION = "publication/";
    public static final String ONEM_LOCATION = "onem/";
    public static final String OFFER_LOCATION = "offer/";
    
    public static final String USER_TYPE_TAG = "user";
    public static final String SESSION_TAG = "session";
    public static final String PAGE_TITLE_TAG = "title";
    public static final String NEWS_TAG = "news";
    
    public static final String ENTERPRISE_NAME = "enterprise.name";
    public static final String JOB_LOCATION = "job.location";
    public static final String JOB_SALARY = "job.salary";
    public static final String JOB_DESCRIPTION = "job.description";
    public static final String OFFER_START_DATE = "offer.startDate";
    public static final String OFFER_END_DATE = "offer.closingDate";
    
    public static final String EMAIL_PARAGRAPHE = "PARAGRAPHE";
    public static final String EMAIL_OPEN_HTML_TAG = "<html><body>";
    public static final String EMAIL_CLOSE_HTML_TAG = "</body></html>";
    public static final String EMAIL_OPEN_PARAGRAPHE_TAG = "<p>";
    public static final String EMAIL_OPEN_PARAGRAPHE = "<p>PARAGRAPHE</p>";
    public static final String EMAIL_CLOSE_PARAGRAPHE_TAG = "</p>";
    public static final String EMAIL_CLOSING_SIGNATURE = "<p>Administration - ONEM <br/>" + new Date() + "</p>";
    public static final String EMAIL_JOB_ALERT = "<table><tr><td>enterprise.name</td><td>job.location</td>\n" +
                            "<td>&#36;job.salary</td><td>offer.startDate</td><td>offer.closingDate</td></tr>\n" +
                            "<tr><td colspan=\"5\">job.description</td></tr></table>";
    
    private static final String FORMAT = "dd/MM/yyyy";    
    
    public static Date convertToDate(String data) throws ParseException{
        SimpleDateFormat format = new SimpleDateFormat(FORMAT);
        return format.parse(data);
    }
    
    public static boolean isNotNullOrEmpty(String data){
        return data != null && !data.isEmpty();
    }
}
