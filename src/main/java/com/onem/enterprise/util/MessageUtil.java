/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author Lyn
 */
public class MessageUtil {
    
    public static final String USER_NOT_FOUND_MESSAGE = "message.user.not.found";
    public static final String USERNAME_AND_PASSWORD_NOT_MATCHING_MESSAGE = "message.username.and.password.not.matching";
    
    private static MessageUtil instance;
    
    private MessageUtil(){        
    }
    
    public static MessageUtil getInstance(){
        if(instance == null){
            instance = new MessageUtil();
        }
        return instance;
    }
    
    public String getMessage(String key, Locale locale){
        return ResourceBundle.getBundle("messages", locale).getString(key);
    }
    
}
