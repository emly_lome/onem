/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.util;

import java.util.Date;

/**
 *
 * @author ekasongo
 */
public class ReferenceNoGenerator {
    
    private static ReferenceNoGenerator instance;
    
    private final String JOB_SEEKER_PREFIX = "JS";
    private final String ENTERPRISE_PREFIX = "EN";
    private final String DELIMETER = "-";
    
    private ReferenceNoGenerator(){
        
    }
    
    public synchronized static ReferenceNoGenerator getInstance(){
        if(instance == null){
            instance = new ReferenceNoGenerator();
        }
        return instance;
    }
    
    public String generateJobSeekerRef(int id){
        String date = DateUtil.formatDate(new Date());
        return JOB_SEEKER_PREFIX + DELIMETER + date + DELIMETER + id;
    }
    
    public String generateEnterpriseRef(int id){
        String date = DateUtil.formatDate(new Date());
        return JOB_SEEKER_PREFIX + DELIMETER + date + DELIMETER + id;
    }
}
