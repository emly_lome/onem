/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Component;

/**
 *
 * @author ekasongo
 */
@Component
public class PasswordGeneratorUtil {
    
    private MessageDigest md;
    
    public PasswordGeneratorUtil(){
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(PasswordGeneratorUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String encryptPassword(String password){
        md.update(password.getBytes());
        byte[] bytes = md.digest();
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<bytes.length;i++){
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
    
    public boolean matchPassword(String current, String entered){
        entered = encryptPassword(entered);
        return current.equals(entered);
    }
}
