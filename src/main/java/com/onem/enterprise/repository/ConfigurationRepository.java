/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.repository;

import com.onem.enterprise.model.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ekasongo
 */
public interface ConfigurationRepository extends JpaRepository<Configuration, Integer>{
    
    @Query("SELECT c FROM Configuration c WHERE c.name = :name")
    public Configuration findConfigurationByName(@Param("name") String name);
}
