/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.repository;

import com.onem.enterprise.model.EnterpriseLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ekasongo
 */
public interface EnterpriseLoginRepository extends JpaRepository<EnterpriseLogin, Integer>{
    
    @Query("Select e from EnterpriseLogin e where username = :username")
    public EnterpriseLogin getEnterpriseLogin(@Param("username") String username);
}
