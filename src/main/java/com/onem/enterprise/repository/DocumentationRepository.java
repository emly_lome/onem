/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.repository;

import com.onem.enterprise.model.Documentation;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ekasongo
 */
public interface DocumentationRepository extends JpaRepository<Documentation, Integer>{
    
}
