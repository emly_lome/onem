/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.repository;

import com.onem.enterprise.model.JobSeeker;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ekasongo
 */
public interface JobSeekerRepository extends JpaRepository<JobSeeker, Integer>{
    
}
