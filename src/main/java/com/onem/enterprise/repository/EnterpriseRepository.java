/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.repository;

import com.onem.enterprise.model.Enterprise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ekasongo
 */
public interface EnterpriseRepository extends JpaRepository<Enterprise, Integer>{
    
    @Query("Select count(e) from Enterprise e where name = :name")
    public int checkEnterpriseExist(@Param("name") String name);
}
