/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.repository;

import com.onem.enterprise.model.JobSeekerLogin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ekasongo
 */
public interface JobSeekerLoginRepository extends JpaRepository<JobSeekerLogin, Integer>{
    
    @Query("SELECT j FROM JobSeekerLogin j WHERE j.username = :username")
    public JobSeekerLogin findUserByUsername(@Param("username") String username);
    
}
