/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.repository;

import com.onem.enterprise.model.Enterprise;
import com.onem.enterprise.model.Job;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ekasongo
 */
public interface JobRepository extends JpaRepository<Job, Integer>{
    
    @Query("Select distinct(j.sector) from Job j")
    public List<String> findAvailableSectors();
    
    @Query("Select job from Job job where status = :status")
    public List<Job> findAvailableJobByStatus(@Param("status") String status);
    
    @Query("Select job from Job job where job.enterprise = :enterprise")
    public List<Job> findAvailableJobByEnterprise(@Param("enterprise") Enterprise id);
}
