/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ekasongo
 */
@Entity
@Table(name = "enterprise_login")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EnterpriseLogin.findAll", query = "SELECT e FROM EnterpriseLogin e"),
    @NamedQuery(name = "EnterpriseLogin.findById", query = "SELECT e FROM EnterpriseLogin e WHERE e.id = :id"),
    @NamedQuery(name = "EnterpriseLogin.findByUsername", query = "SELECT e FROM EnterpriseLogin e WHERE e.username = :username"),
    @NamedQuery(name = "EnterpriseLogin.findByPassword", query = "SELECT e FROM EnterpriseLogin e WHERE e.password = :password")})
public class EnterpriseLogin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "password")
    private String password;
    @JoinColumn(name = "entreprise_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Enterprise entreprise;

    public EnterpriseLogin() {
    }

    public EnterpriseLogin(Integer id) {
        this.id = id;
    }

    public EnterpriseLogin(Integer id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Enterprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Enterprise entreprise) {
        this.entreprise = entreprise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EnterpriseLogin)) {
            return false;
        }
        EnterpriseLogin other = (EnterpriseLogin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onem.enterprise.model.EnterpriseLogin[ id=" + id + " ]";
    }
    
}
