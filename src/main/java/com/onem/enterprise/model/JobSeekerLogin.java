/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ekasongo
 */
@Entity
@Table(name = "job_seeker_login")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobSeekerLogin.findAll", query = "SELECT j FROM JobSeekerLogin j"),
    @NamedQuery(name = "JobSeekerLogin.findById", query = "SELECT j FROM JobSeekerLogin j WHERE j.id = :id"),
    @NamedQuery(name = "JobSeekerLogin.findByUsername", query = "SELECT j FROM JobSeekerLogin j WHERE j.username = :username"),
    @NamedQuery(name = "JobSeekerLogin.findByPassword", query = "SELECT j FROM JobSeekerLogin j WHERE j.password = :password")})
public class JobSeekerLogin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "password")
    private String password;
    @JoinColumn(name = "job_seeker_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobSeeker jobSeeker;

    public JobSeekerLogin() {
    }

    public JobSeekerLogin(Integer id) {
        this.id = id;
    }

    public JobSeekerLogin(Integer id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }    

    public JobSeekerLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JobSeeker getJobSeeker() {
        return jobSeeker;
    }

    public void setJobSeeker(JobSeeker jobSeeker) {
        this.jobSeeker = jobSeeker;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobSeekerLogin)) {
            return false;
        }
        JobSeekerLogin other = (JobSeekerLogin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onem.enterprise.model.JobSeekerLogin[ id=" + id + " ]";
    }
    
}
