/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onem.enterprise.model;

/**
 *
 * @author Lyn
 */
public class LoginResponse {
    
    private LOGIN_TYPE type;
    private String error;
    private boolean exist;

    public LOGIN_TYPE getType() {
        return type;
    }

    public void setType(LOGIN_TYPE type) {
        this.type = type;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }
    
    
    
    public static enum LOGIN_TYPE{
        ENTREPRISE,
        JOB_SEEKER,
        ADMINISTRATOR
    }
    
}
