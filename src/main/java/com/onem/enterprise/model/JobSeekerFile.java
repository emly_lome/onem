/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ekasongo
 */
@Entity
@Table(name = "job_seeker_file")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobSeekerFile.findAll", query = "SELECT j FROM JobSeekerFile j"),
    @NamedQuery(name = "JobSeekerFile.findById", query = "SELECT j FROM JobSeekerFile j WHERE j.id = :id"),
    @NamedQuery(name = "JobSeekerFile.findByName", query = "SELECT j FROM JobSeekerFile j WHERE j.name = :name"),
    @NamedQuery(name = "JobSeekerFile.findByType", query = "SELECT j FROM JobSeekerFile j WHERE j.type = :type"),
    @NamedQuery(name = "JobSeekerFile.findByDescription", query = "SELECT j FROM JobSeekerFile j WHERE j.description = :description")})
public class JobSeekerFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "job_seeker_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobSeeker jobSeeker;

    public JobSeekerFile() {
    }

    public JobSeekerFile(Integer id) {
        this.id = id;
    }

    public JobSeekerFile(Integer id, String name, String type, String description) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JobSeeker getJobSeeker() {
        return jobSeeker;
    }

    public void setJobSeeker(JobSeeker jobSeeker) {
        this.jobSeeker = jobSeeker;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobSeekerFile)) {
            return false;
        }
        JobSeekerFile other = (JobSeekerFile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onem.enterprise.model.JobSeekerFile[ id=" + id + " ]";
    }
    
}
