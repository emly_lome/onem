/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

/**
 *
 * @author ekasongo
 */
public class SectorReport {
    
    private String sector;
    private Enterprise enterprise;
    private int availableJobs;

    public SectorReport() {
    }

    public SectorReport(String sector, int availableJobs) {
        this(sector,availableJobs,null);
    }

    public SectorReport(String sector, int availableJobs, Enterprise enterprise) {
        this.sector = sector;
        this.availableJobs = availableJobs;
        this.enterprise = enterprise;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public int getAvailableJobs() {
        return availableJobs;
    }

    public void setAvailableJobs(int availableJobs) {
        this.availableJobs = availableJobs;
    }
    
}
