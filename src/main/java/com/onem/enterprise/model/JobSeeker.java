/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ekasongo
 */
@Entity
@Table(name = "job_seeker")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobSeeker.findAll", query = "SELECT j FROM JobSeeker j"),
    @NamedQuery(name = "JobSeeker.findById", query = "SELECT j FROM JobSeeker j WHERE j.id = :id"),
    @NamedQuery(name = "JobSeeker.findByTitle", query = "SELECT j FROM JobSeeker j WHERE j.title = :title"),
    @NamedQuery(name = "JobSeeker.findByFirstName", query = "SELECT j FROM JobSeeker j WHERE j.firstName = :firstName"),
    @NamedQuery(name = "JobSeeker.findByLastName", query = "SELECT j FROM JobSeeker j WHERE j.lastName = :lastName"),
    @NamedQuery(name = "JobSeeker.findByGender", query = "SELECT j FROM JobSeeker j WHERE j.gender = :gender"),
    @NamedQuery(name = "JobSeeker.findByDob", query = "SELECT j FROM JobSeeker j WHERE j.dob = :dob"),
    @NamedQuery(name = "JobSeeker.findByStatus", query = "SELECT j FROM JobSeeker j WHERE j.status = :status"),
    @NamedQuery(name = "JobSeeker.findByContactNo", query = "SELECT j FROM JobSeeker j WHERE j.contactNo = :contactNo"),
    @NamedQuery(name = "JobSeeker.findByContactEmail", query = "SELECT j FROM JobSeeker j WHERE j.contactEmail = :contactEmail")})
public class JobSeeker implements Serializable {
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "jobSeeker")
    private Set<JobOfferApplication> jobOfferApplications;
    @Size(max = 45)
    @Column(name = "marital_status")
    private String maritalStatus;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "town")
    private String town;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "highest_qualification")
    private String highestQualification;
    @Size(max = 100)
    @Column(name = "years_experience")
    private String yearsExperience;
    @Size(max = 1000)
    @Column(name = "description")
    private String description;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "Title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "last_name")
    private String lastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "gender")
    private String gender;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dob")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "ref_no")
    private String refno;
    @Size(max = 45)
    @Column(name = "contact_no")
    private String contactNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "contact_email")
    private String contactEmail;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "jobSeeker")
    private Set<JobAlert> jobAlerts;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "jobSeeker")
    private Set<JobSeekerFile> jobSeekerFiles;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "jobSeeker")
    private Set<JobSeekerLogin> jobSeekerLogins;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "jobSeeker")
    private Set<JobPreference> jobPreferences;

    public JobSeeker() {
    }

    public JobSeeker(Integer id) {
        this.id = id;
    }

    public JobSeeker(Integer id, String title, String firstName, String lastName, String gender, Date dob, String status, String contactEmail) {
        this.id = id;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dob = dob;
        this.status = status;
        this.contactEmail = contactEmail;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @XmlTransient
    public Set<JobAlert> getJobAlerts() {
        return jobAlerts;
    }

    public void setJobAlerts(Set<JobAlert> jobAlerts) {
        this.jobAlerts = jobAlerts;
    }

    @XmlTransient
    public Set<JobSeekerFile> getJobSeekerFiles() {
        return jobSeekerFiles;
    }

    public void setJobSeekerFiles(Set<JobSeekerFile> jobSeekerFiles) {
        this.jobSeekerFiles = jobSeekerFiles;
    }

    @XmlTransient
    public Set<JobSeekerLogin> getJobSeekerLogins() {
        return jobSeekerLogins;
    }

    public void setJobSeekerLogins(Set<JobSeekerLogin> jobSeekerLogins) {
        this.jobSeekerLogins = jobSeekerLogins;
    }

    @XmlTransient
    public Set<JobPreference> getJobPreferences() {
        return jobPreferences;
    }

    public void setJobPreferences(Set<JobPreference> jobPreferences) {
        this.jobPreferences = jobPreferences;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobSeeker)) {
            return false;
        }
        JobSeeker other = (JobSeeker) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return title + " " + firstName + " " + lastName;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getHighestQualification() {
        return highestQualification;
    }

    public void setHighestQualification(String highestQualification) {
        this.highestQualification = highestQualification;
    }

    public String getYearsExperience() {
        return yearsExperience;
    }

    public void setYearsExperience(String yearsExperience) {
        this.yearsExperience = yearsExperience;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    @XmlTransient
    public Set<JobOfferApplication> getJobOfferApplications() {
        return jobOfferApplications;
    }

    public void setJobOfferApplications(Set<JobOfferApplication> jobOfferApplications) {
        this.jobOfferApplications = jobOfferApplications;
    }
}
