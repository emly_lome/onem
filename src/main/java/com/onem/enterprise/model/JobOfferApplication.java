/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ekasongo
 */
@Entity
@Table(name = "job_offer_application")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JobOfferApplication.findAll", query = "SELECT j FROM JobOfferApplication j"),
    @NamedQuery(name = "JobOfferApplication.findById", query = "SELECT j FROM JobOfferApplication j WHERE j.id = :id"),
    @NamedQuery(name = "JobOfferApplication.findByApplicationTime", query = "SELECT j FROM JobOfferApplication j WHERE j.applicationTime = :applicationTime")})
public class JobOfferApplication implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "status")
    private String status;
    @JoinColumn(name = "job_seeker_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private JobSeeker jobSeeker;
    
    @JoinColumn(name = "job_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Job job;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "application_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date applicationTime;

    public JobOfferApplication() {
    }

    public JobOfferApplication(Integer id) {
        this.id = id;
    }

    public JobOfferApplication(Integer id, Date applicationTime) {
        this.id = id;
        this.applicationTime = applicationTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getApplicationTime() {
        return applicationTime;
    }

    public void setApplicationTime(Date applicationTime) {
        this.applicationTime = applicationTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JobOfferApplication)) {
            return false;
        }
        JobOfferApplication other = (JobOfferApplication) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onem.enterprise.model.JobOfferApplication[ id=" + id + " ]";
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JobSeeker getJobSeeker() {
        return jobSeeker;
    }

    public void setJobSeeker(JobSeeker jobSeeker) {
        this.jobSeeker = jobSeeker;
    }
    
}
