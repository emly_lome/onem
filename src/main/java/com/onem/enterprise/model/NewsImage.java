/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ekasongo
 */
@Entity
@Table(name = "news_image")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NewsImage.findAll", query = "SELECT n FROM NewsImage n"),
    @NamedQuery(name = "NewsImage.findById", query = "SELECT n FROM NewsImage n WHERE n.id = :id"),
    @NamedQuery(name = "NewsImage.findByName", query = "SELECT n FROM NewsImage n WHERE n.name = :name"),
    @NamedQuery(name = "NewsImage.findByFilename", query = "SELECT n FROM NewsImage n WHERE n.filename = :filename")})
public class NewsImage implements Serializable {
    @JoinColumn(name = "artical_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Article articalId;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Size(max = 45)
    @Column(name = "filename")
    private String filename;

    public NewsImage() {
    }

    public NewsImage(Integer id) {
        this.id = id;
    }

    public NewsImage(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NewsImage)) {
            return false;
        }
        NewsImage other = (NewsImage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onem.enterprise.model.temp.NewsImage[ id=" + id + " ]";
    }

    public Article getArticalId() {
        return articalId;
    }

    public void setArticalId(Article articalId) {
        this.articalId = articalId;
    }
    
}
