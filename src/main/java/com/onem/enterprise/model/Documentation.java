/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ekasongo
 */
@Entity
@Table(name = "documentation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Documentation.findAll", query = "SELECT d FROM Documentation d"),
    @NamedQuery(name = "Documentation.findById", query = "SELECT d FROM Documentation d WHERE d.id = :id"),
    @NamedQuery(name = "Documentation.findByName", query = "SELECT d FROM Documentation d WHERE d.name = :name"),
    @NamedQuery(name = "Documentation.findByDescription", query = "SELECT d FROM Documentation d WHERE d.description = :description"),
    @NamedQuery(name = "Documentation.findByType", query = "SELECT d FROM Documentation d WHERE d.type = :type"),
    @NamedQuery(name = "Documentation.findByFileName", query = "SELECT d FROM Documentation d WHERE d.fileName = :fileName")})
public class Documentation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "name")
    private String name;
    @Size(max = 500)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "file_name")
    private String fileName;

    public Documentation() {
    }

    public Documentation(Integer id) {
        this.id = id;
    }

    public Documentation(Integer id, String name, String type, String fileName) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.fileName = fileName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Documentation)) {
            return false;
        }
        Documentation other = (Documentation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onem.enterprise.model.Documentation[ id=" + id + " ]";
    }
    
}
