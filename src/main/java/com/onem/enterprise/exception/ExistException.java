/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onem.enterprise.exception;

/**
 *
 * @author ekasongo
 */
public class ExistException extends RuntimeException{
    
    public ExistException(String message){
        super(message);
    }
    
}
