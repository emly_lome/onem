
<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="forgot.title"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="forgot.title"/></span></li>
            </ul>
        </div>
    </div>
    <div class="container b-forgot-password-page">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="b-forgot-password-form">
                    <h3 class="f-primary-l is-global-title"><spring:message code="forgot.field"/></h3>
                    <p><spring:message code="forgot.field.description"/>${type}</p>
                    <div class="b-form-row b-form-inline b-form-horizontal b-form-password">
                        <form:form name="login" action="reset-password.htm" method="POST" commandName="emailForm">
                            <div class="b-form-row">
                                <div class="b-form-horizontal--mail f-form-horizontal--mail">
                                    <i class="fa fa-envelope"></i>
                                    <form:input path="email" type="text" id="create_account_email" class="form-control" placeholder="Email" />
                                </div>
                            </div>
                            <div class="b-form-row">
                                <div>
                                    <input name="<spring:message code="forgot.action"/>" type="submit" class="b-btn f-btn button-sm b-btn-default f-primary-b b-btn__w100"/>
                                </div>
                            </div>
                        </form:form>
                        <div class="b-form-row">
                            <div class="b-form-horizontal--mail">
                                <a href="#" class="f-link--color">Contact Online Support?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>