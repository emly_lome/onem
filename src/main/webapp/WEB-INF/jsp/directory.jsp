<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.rom"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="menu.rom"/> </span></li>
            </ul>
        </div>
    </div>
    <section class="b-infoblock">
        <div class="container">
            <div class="b-full-search-form">
                <form:form action="filter-offers.htm?page=1" method="POST" class="b-col-default-indent" commandName="search">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 b-full-search-form_col">
                            <div class="b-full-search-form_title f-full-search-form_title f-primary-b"><spring:message code="offer.type.title"/></div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-6 b-full-search-form_check">
                                    <label for="radio_off">
                                        <form:radiobutton path="type" class="b-form-radio" name="radio" id="radio_off" value="permanent"/>
                                        <span class="f-full-search-form_label f-primary-b"><spring:message code="offer.type.permanent"/></span>
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6 b-full-search-form_check">
                                    <label for="hotels">
                                        <form:radiobutton  path="type" class="b-form-radio" name="radio" id="hotels" value="contract"/>
                                        <span class="f-full-search-form_label f-primary-b"><spring:message code="offer.type.contract"/></span>
                                    </label>
                                </div>
                                <div class="visible-md"></div>
                                <div class="col-sm-6 col-xs-6 b-full-search-form_check">
                                    <label for="car">
                                        <form:radiobutton  path="type" class="b-form-radio" name="radio" id="car" value="placement"/>
                                        <span class="f-full-search-form_label f-primary-b"><spring:message code="offer.type.placement"/></span>
                                    </label>
                                </div>
                                <div class="col-sm-6 col-xs-6 b-full-search-form_check">
                                    <label for="self_cat">
                                        <form:radiobutton  path="type" class="b-form-radio" name="radio" id="self_cat"/>
                                        <span class="f-full-search-form_label f-primary-b"><spring:message code="offer.type.all"/></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 b-full-search-form_col">
                            <div class="b-full-search-form_title f-full-search-form_title f-primary-b"><spring:message code="offer.location.title"/></div>
                            <div class="b-full-search-form_label f-full-search-form_label f-primary-b"><spring:message code="offer.location.town"/></div>
                            <spring:message code="offer.location.eg" var="location"/>
                            <form:input path="location" type="text" class="form-control" placeholder="${location}"/>
                        </div>
                        <div class="clearfix visible-md visible-sm"></div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 b-full-search-form_col">
                            <div class="b-full-search-form_title f-full-search-form_title f-primary-b"><spring:message code="offer.sector.title"/></div>
                            <div class="b-full-search-form_label f-full-search-form_label f-primary-b"><spring:message code="offer.sector.field"/></div>
                            <div class="row">
                                <div class="b-form-row b-form-select b-select--alt">
                                    <form:select path="sector" class="j-select">
                                        <option selected="selected"></option>
                                        <c:forEach begin="1" end="42" var="sector">
                                            <option>${sector} - <spring:message code="sector.${sector}"/></option>
                                        </c:forEach>                                        
                                    </form:select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 b-full-search-form_col">
                            <div class="b-full-search-form_title f-full-search-form_title f-primary-b"><spring:message code="offer.period.title"/></div>
                            <div class="b-full-search-form_label f-full-search-form_label f-primary-b"><spring:message code="offer.period.end"/></div>
                            <div class="row">                       
                                <div class="col-md-6">
                                    <div class="b-form-row b-form-control__icon-wrap">
                                        <form:input path="closing" type="text" class="form-control datepicker datepicker" id="dp21"/>
                                        <i class="fa fa-calendar b-form-control__icon f-form-control__icon"></i>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input name="<spring:message code="offer.action"/>" type="submit" class="b-search-map__submit b-btn f-btn b-btn-light f-btn-light f-primary-b f-left"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </section>
    <div class="container ">
        <div class="l-inner-page-container">
            <div class="row">
                <div class="col-md-9">
                    <c:forEach var="job" items="${jobs}">
                        <div class="b-search-result-box">
                            <div class="b-search-item b-diagonal-line-bg-light">
                                <h3 class="f-primary-l  is-global-title f-h4-special"><a href="offer.htm?id=${job.id}" class="f-more">${job.title}</a></h3>
                                <div class="b-form-row f-h4-special b-form--mini clearfix">
                                    <div class="pull-left">
                                        <a href="apply.htm?id=${job.id}" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate b-blog-one-column__info_edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                    <div class="b-blog-one-column__info_container">
                                        <div class="b-blog-one-column__info f-blog-one-column__info">
                                            <spring:message code="offer.result.category"/> : <span class="f-more">${job.type}</span>
                                            <span class="b-blog-one-column__info_delimiter"></span>
                                            <i class="fa fa-comment"></i><span class="f-more">${fn:length(jobOfferApplications)} <spring:message code="offer.result.no.application"/></span>
                                            <span class="b-blog-one-column__info_delimiter"></span>
                                            <spring:message code="offer.result.sector"/> : <span class="f-more">${job.sector}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-form-row b-blog-one-column__text"><span class="f-more"><spring:message code="offer.result.description"/></span> ${fn:substring(job.description, 0, 300)}...</div>
                                <div class="b-form-row">
                                    <a href="offer.htm?id=${job.id}" class="b-btn f-btn b-btn-xs b-btn-default f-primary-b"><spring:message code="offer.result.read.more"/></a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                    <div class="row b-pagination f-pagination">
                        <div class="container">
                            <ul>
                                <li><a href="directory.htm?page=1">First</a></li>
                                <li><a class="prev" href="directory.htm?page=<c:if test="${selected - 1 > 0}">${selected - 1}</c:if>"><i class="fa fa-angle-left"></i></a></li>
                                        <c:forEach var="page" begin="1" end="${pages}">
                                    <li <c:if test="${page == selected}">class="is-active-pagination"</c:if>><a href="directory.htm?page=${page}">${page}</a></li>
                                    </c:forEach>
                                <li><a class="next" href="directory.htm?page=<c:if test="${selected + 1 == pages}">${selected + 1}</c:if>"><i class="fa fa-angle-right"></i></a></li>
                                <li><a href="directory.htm?page=${pages}">Last</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row b-col-default-indent">
                        <div class="col-md-12">
                            <div class="b-categories-filter">
                                <h4 class="f-primary-b b-h4-special f-h4-special--gray f-h4-special"><spring:message code="right.side.panel.sector.title"/></h4>
                                <ul>
                                    <c:forEach var="report" items="${reports}">
                                        <li>
                                            <a class="f-categories-filter_name" href="sector-directory.htm?page=1&sector=${report.sector}"><i class="fa fa-plus"></i> ${report.sector}</a>
                                            <span class="b-categories-filter_count f-categories-filter_count">${report.availableJobs}</span>
                                        </li>
                                    </c:forEach>       
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">popular postes</h4>
                            <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                                <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Phasellus id mattis dolorunc et erat hendrerit, tincidunt</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Vel eleifend id ullamcorper eu velit</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                14, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Lorem ipsum dolor sit amet purus vitae magna rhoncus</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b hidden">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md12">
                            <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">tags cloud</h4>
                            <div>
                                <c:forEach var="tag" items="${tags}">
                                    <a class="f-tag b-tag" href="#">${tag.description}</a>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
