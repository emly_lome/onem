

<div class="l-main-container">
    <div class="b-slidercontainer b-slider">
        <div class="j-fullscreenslider j-arr-hide">
            <ul>
                <li data-transition="" data-slotamount="17" >
                    <div class="tp-bannertimer"></div>
                    <img data-retina src="img/slider/engineer.jpg">
                    <div class="caption lft caption-left"  data-x="left" data-y="150" data-hoffset="60" data-speed="700" data-start="2000">
                        <div class="b-slider-lg-info-l__item-title f-slider-lg-info-l__item-title b-slider-lg-info-l__item-title-tertiary b-bg-slider-lg-info-l__item-title">
                            <h2 class="f-primary-l"><spring:message code="onem.sub.title"/></h2>
                            <h1 class="f-primary-b"><spring:message code="index.enterprise.title"/></h1>
                        </div>
                    </div>
                    <div class="caption lfl caption-left"  data-x="left" data-y="270" data-speed="1000" data-start="2600">
                        <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                            <a href="login.htm?home=enterprise" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b"><spring:message code="index.enterprise.action"/></a>
                            <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                        </div>
                    </div>
                </li>
                <li data-transition="slotfade-vertical" data-slotamount="7" >
                    <div class="tp-bannertimer"></div>
                    <img data-retina src="img/slider/architect.jpg">
                    <div class="caption lft caption-left"  data-x="left" data-y="150" data-hoffset="60" data-speed="700" data-start="2000">
                        <div class="b-slider-lg-info-l__item-title f-slider-lg-info-l__item-title b-slider-lg-info-l__item-title-tertiary b-bg-slider-lg-info-l__item-title">
                            <h2 class="f-primary-l"><spring:message code="onem.sub.title"/></h2>
                            <h1 class="f-primary-b"><spring:message code="index.who.we.are.title"/></h1>
                        </div>
                    </div>
                    <div class="caption lfl caption-left"  data-x="left" data-y="270" data-speed="1000" data-start="2600">
                        <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                            <a href="about-us.htm" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b"><spring:message code="index.who.we.are.action"/></a>
                            <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                        </div>
                    </div>
                </li>
                <li data-transition="slotfade-vertical" data-slotamount="7" >
                    <div class="tp-bannertimer"></div>
                    <img data-retina src="img/slider/search-job.jpg">
                    <div class="caption lft caption-left"  data-x="left" data-y="150" data-hoffset="60" data-speed="700" data-start="2000">
                        <div class="b-slider-lg-info-l__item-title f-slider-lg-info-l__item-title b-slider-lg-info-l__item-title-tertiary">
                            <h2 class="f-primary-l"><spring:message code="onem.sub.title"/></h2>
                            <h1 class="f-primary-b"><spring:message code="index.job.search.title"/></h1>
                        </div>
                    </div>
                    <div class="caption lfl caption-left"  data-x="left" data-y="270" data-speed="1000" data-start="2600">
                        <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                            <a href="directory.htm?page=1" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b"><spring:message code="index.job.search.action"/></a>
                            <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                        </div>
                    </div>
                </li>
                <li data-transition="slotfade-vertical" data-slotamount="7" >
                    <div class="tp-bannertimer"></div>
                    <img data-retina src="img/slider/mechanic.jpg">
                    <div class="caption lft caption-left"  data-x="left" data-y="150" data-hoffset="60" data-speed="700" data-start="2000">
                        <div class="b-slider-lg-info-l__item-title f-slider-lg-info-l__item-title b-slider-lg-info-l__item-title-tertiary b-bg-slider-lg-info-l__item-title">
                            <h2 class="f-primary-l"><spring:message code="onem.sub.title"/></h2>
                            <h1 class="f-primary-b"><spring:message code="index.job.seeker.title"/></h1>
                        </div>
                    </div>
                    <div class="caption lfl caption-left"  data-x="left" data-y="270" data-speed="1000" data-start="2600">
                        <div class="b-slider-lg-info-l__item-link f-slider-lg-info-l__item-link">
                            <a href="login.htm?home=jobseeker" class="b-slider-lg-info-l__item-anchor f-slider-lg-info-l__item-anchor f-primary-b"><spring:message code="index.job.seeker.action"/></a>
                            <span class="b-slider-lg-info-l__item-link-after"><i class="fa fa-chevron-right"></i></span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <section class="b-infoblock f-center b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-primary-l"><spring:message code="onem.sub.title"/> <strong class="f-primary-b"><spring:message code="onem.sub.title.location"/></strong></h2>
            <p class="b-desc-section f-desc-section f-secondary-l f-small"><spring:message code="index.sub.title.description"/></p>
            <div class="b-some-examples f-some-examples b-some-examples-secondary f-some-examples-secondary f-secondary row">
                <div class="col-sm-4 col-xs-12">
                    <div class="b-some-examples__item b-bg-transparent f-some-examples__item">
                        <div class="b-some-examples__item_img view view-sixth">
                            <a href="about-us.htm"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/homepage/who-we-are.jpg" alt=""/></a>
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="about-us.htm" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-some-examples__item_info">
                            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-primary-b"><a href="about-us.htm"><spring:message code="sub.menu.about.us.who"/>?</a></div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description">
                                <spring:message code="index.sub.menu.about.us.who.desc"/>
                            </div>
                            <a href="about-us.htm" class="f-primary-b f-more"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="b-some-examples__item b-bg-transparent f-some-examples__item">
                        <div class="b-some-examples__item_img view view-sixth">
                            <a href="about-us.htm"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/homepage/what-we-do.jpg" alt=""/></a>
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="about-us.htm" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-some-examples__item_info">
                            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-primary-b"><a href="about-us.htm"><spring:message code="sub.menu.about.us.what"/>?</a></div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description">
                                <spring:message code="index.sub.menu.about.us.what.desc"/>
                            </div>
                            <a href="about-us.htm" class="f-primary-b f-more"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="b-some-examples__item b-bg-transparent f-some-examples__item">
                        <div class="b-some-examples__item_img view view-sixth">
                            <a href="about-us.htm"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/homepage/what-we-care-for.jpg" alt=""/></a>
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="about-us.htm" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-some-examples__item_info">
                            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-primary-b"><a href="about-us.htm"><spring:message code="sub.menu.about.us.care"/>?</a></div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description">
                                <spring:message code="index.sub.menu.about.us.care.desc"/>
                            </div>
                            <a href="about-us.htm" class="f-primary-b f-more"><i class="fa fa-arrow-circle-right"></i> Read more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="b-infoblock b-desc-section-container">
        <div class="container">
            <h2 class="f-primary-l f-center"><spring:message code="sub.menu.our"/> <span class="f-primary-b"><spring:message code="sub.menu.our.service"/></span></h2>
            <p class="b-desc-section f-desc-section f-secondary-l f-small f-center"><spring:message code="index.sub.service.description"/></p>
            <div class="b-infoblock-with-icon-group row b-infoblock-with-icon--circle-icon">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="b-infoblock-with-icon">
                        <a href="about-us.htm" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                            <i class="fa fa-users"></i>
                        </a>
                        <div class="b-infoblock-with-icon__info">
                            <a href="about-us.htm" class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb"><spring:message code="index.sub.service.job.seeker"/></a>
                            <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                                <spring:message code="index.sub.service.job.seeker.description"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="b-infoblock-with-icon">
                        <a href="about-us.htm" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                            <i class="fa fa-desktop"></i>
                        </a>
                        <div class="b-infoblock-with-icon__info">
                            <a href="about-us.htm" class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb"><spring:message code="index.sub.service.enterprise"/></a>
                            <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                                <spring:message code="index.sub.service.enterprise.description"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="b-infoblock-with-icon">
                        <a href="documentation.htm" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate">
                            <i class="fa fa-comments"></i>
                        </a>
                        <div class="b-infoblock-with-icon__info">
                            <a href="documentation.htm" class="f-infoblock-with-icon__info_title b-infoblock-with-icon__info_title f-primary-sb"><spring:message code="index.sub.service.documentation"/></a>
                            <div class="f-infoblock-with-icon__info_text b-infoblock-with-icon__info_text">
                                <spring:message code="index.sub.service.documentation.description"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="b-infoblock f-center b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-primary-l"><spring:message code="sub.menu.our"/> <strong class="f-primary-b"><spring:message code="sub.menu.our.events.recent"/></strong></h2>
            <p class="b-infoblock-description f-desc-section f-primary-l f-small"><spring:message code="index.sub.event.description"/></p>
            <div class="b-app-with-img row">
                <div class="col-sm-4 col-xs-12">
                    <div class="b-app-with-img__item">
                        <div class="b-app-with-img__item_img view view-sixth">
                            <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/homepage/event-1.jpg" alt=""/></a>
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="img/homepage/big/event-1.jpg" class="b-btn f-btn b-btn-light f-btn-light info fancybox" title="Mobile games" rel="group2"><i class="fa fa-arrows-alt"></i></a>
                                        <a href="portfolio_our_portfolio_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-app-with-img__item_text f-center">
                            <div class="b-app-with-img__item_name f-app-with-img__item_name f-primary-b"><a href="portfolio_our_portfolio_detail.html">Mobile games</a></div>
                            <div class="b-app-with-img__item_info f-app-with-img__item_info f-primary">Mobile Design, 3D Graphic</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="b-app-with-img__item">
                        <div class="b-app-with-img__item_img view view-sixth">
                            <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/homepage/event-2.jpg" alt=""/></a>
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="img/homepage/big/event-2.jpg" class="b-btn f-btn b-btn-light f-btn-light info fancybox" title="Character design" rel="group2"><i class="fa fa-arrows-alt"></i></a>
                                        <a href="portfolio_our_portfolio_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-app-with-img__item_text f-center">
                            <div class="b-app-with-img__item_name f-app-with-img__item_name f-primary-b"><a href="portfolio_our_portfolio_detail.html">Character design</a></div>
                            <div class="b-app-with-img__item_info f-app-with-img__item_info f-primary">Mobile Design, 3D Graphic</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="b-app-with-img__item">
                        <div class="b-app-with-img__item_img view view-sixth">
                            <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/homepage/event-3.jpg" alt=""/></a>
                            <div class="b-item-hover-action f-center mask">
                                <div class="b-item-hover-action__inner">
                                    <div class="b-item-hover-action__inner-btn_group">
                                        <a href="img/homepage/big/event-3.jpg" class="b-btn f-btn b-btn-light f-btn-light info fancybox" title="branding design" rel="group2"><i class="fa fa-arrows-alt"></i></a>
                                        <a href="portfolio_our_portfolio_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="b-app-with-img__item_text f-center">
                            <div class="b-app-with-img__item_name f-app-with-img__item_name f-primary-b"><a href="portfolio_our_portfolio_detail.html">branding design</a></div>
                            <div class="b-app-with-img__item_info f-app-with-img__item_info f-primary">Brading Design / 2D Designs</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="b-infoblock b-diagonal-line-bg-light">
        <div class="container">
            <h2 class="f-primary-l f-center"><spring:message code="sub.menu.our"/> <span class="f-primary-b"><spring:message code="index.sub.menu.message"/></span></h2>
            <p class="b-infoblock-description f-desc-section f-center f-employee__desc f-small f-primary-l"><spring:message code="index.sub.menu.message.description"/></p>
            <div class="row" data-animate-group-wrap>
                <div class="col-sm-6 col-xs-12">
                    <div class="b-mention-item">
                        <div class="b-mention-item__user f-center">
                            <div class="b-mention-item__user_img">
                                <img data-retina class="fade-in-animate" data-animate-group="2" src="img/users/user-md.png" alt="">
                            </div>
                            <div class="b-mention-item__user_info f-mention-item__user_info">
                                <div class="f-mention-item__user_name f-primary-b"><spring:message code="onem.director"/></div>
                                <div class="f-mention-item__user_position"><spring:message code="onem.director.title"/></div>
                            </div>
                        </div>
                        <div class="b-remaining">
                            <div class="b-mention-item__comment">
                                <div class="b-mention-item__comment_text f-mention-item__comment_text"><spring:message code="onem.director.message"/><br/>
                                    <a href="http://onem.cd" target="_blank" class="f-primary-it f-more">http://onem.cd</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="b-mention-item">
                        <div class="b-mention-item__user f-center">
                            <div class="b-mention-item__user_img">
                                <img data-retina class="fade-in-animate" data-animate-group="2" src="img/users/user-bl.png" alt="">
                            </div>
                            <div class="b-mention-item__user_info f-mention-item__user_info">
                                <div class="f-mention-item__user_name f-primary-b"><spring:message code="onem.bl.director"/></div>
                                <div class="f-mention-item__user_position"><spring:message code="onem.bl.director.title"/></div>
                            </div>
                        </div>
                        <div class="b-remaining">
                            <div class="b-mention-item__comment">
                                <div class="b-mention-item__comment_text f-mention-item__comment_text"><spring:message code="onem.bl.director.message"/><br/>
                                    <a href="http://businesslighthouseltd.com" target="_blank" class="f-primary-it f-more">http://businesslighthouseltd.com</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="b-carousel-reset b-carousel-arr-out b-carousel-small-arr f-carousel-small-arr b-remaining">
            <div class="f-center b-logo-group j-logo-slider">
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-1.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-1h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-2.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-2h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-3.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-3h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-4.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-4h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-5.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-5h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-3.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-3h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-4.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-4h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-5.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-5h.png" alt=""/>
                    </a></div>
            </div>
        </div>
    </section>
</div>

