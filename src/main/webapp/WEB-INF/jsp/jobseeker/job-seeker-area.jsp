<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="job.seeker.area.title"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="job.seeker.area.title"/> </span></li>
            </ul>
        </div>
    </div>
</div>
<div class="container ">
    <div class="l-inner-page-container">
        <div class="row">
            <div class="col-md-9">
                <div class="b-detail-home-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="b-form-row f-primary-l f-detail-home-content_title c-secondary f-h4-special b-blog__title clearfix">
                                ${seeker.title} ${seeker.firstName} ${seeker.lastName}
                            </div>
                            <div class="b-form-row f-h4-special clearfix">
                                <div class="b-blog-one-column__info_container b-blog-one-column__info_container--estate">
                                    <span class="b-txt-wrap">
                                        <button class="button-xs b-btn-title-real-estate f-primary-b">${seeker.status}</button>
                                    </span>
                                    <span class="b-txt-wrap"><span class="f-info b-info"><span class="f-primary-b">${seeker.highestQualification}</span> Age : ${age} years</span>
                                        <span class="b-blog-one-column__info_delimiter"></span></span>
                                    <span class="b-txt-wrap"> <spring:message code="job.seeker.registration.form.personel.ref.no"/>: ${seeker.refno}
                                        <span class="b-blog-one-column__info_delimiter"></span></span>
                                    <span class="b-txt-wrap"><i class="b-icon b-icon--garage"></i> ${seeker.address}, ${seeker.town}

                                </div>
                            </div>
                            <p class="f-primary-l">${seeker.description}</p>                            
                        </div>
                    </div>
                </div>
                <div class="b-detail-home-content">
                    <div class="b-tabs f-tabs j-tabs b-tabs-reset b-tabs--secondary f-tabs--secondary">
                        <ul>
                            <li><a href="#tabs-31"><spring:message code="job.seeker.area.addional.details"/></a></li>
                            <li><a href="#tabs-32"><spring:message code="job.seeker.area.preferences"/></a></li>
                            <li><a href="#tabs-33"><spring:message code="job.seeker.area.applications"/></a></li>
                        </ul>
                        <div class="b-tabs__content">
                            <div id="tabs-31" class="clearfix">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="b-home-detail-option f-home-detail-option">
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.dob"/>:</div>
                                                <div class="b-home-detail-option_item_value f-home-detail-option_item_value f-primary-b">${seeker.dob}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.gender"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.gender}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.marital.status"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.maritalStatus}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="b-home-detail-option f-home-detail-option">

                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.email"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.contactEmail}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.phone"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.contactNo}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.profile.years.experiance"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.yearsExperience}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tabs-32" class="clearfix">
                                <c:forEach var="job" end="4" items="${jobs}">
                                    <div class="b-search-result-box">
                                        <div class="b-search-item b-diagonal-line-bg-light">
                                            <h3 class="f-primary-l  is-global-title f-h4-special"><a href="offer.htm?id=${job.id}" class="f-more">${job.title}</a></h3>
                                            <div class="b-form-row f-h4-special b-form--mini clearfix">
                                                <div class="pull-left">
                                                    <a href="blog_detail_right_slidebar.html" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate b-blog-one-column__info_edit">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                </div>
                                                <div class="b-blog-one-column__info_container">
                                                    <div class="b-blog-one-column__info f-blog-one-column__info">
                                                        <spring:message code="offer.result.category"/> : <span class="f-more">${job.type}
                                                            <span class="b-blog-one-column__info_delimiter"></span>
                                                            <a href="blog_detail_right_slidebar.html#comment" class="f-more f-primary"><i class="fa fa-comment"></i>12 <spring:message code="offer.result.no.application"/></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="b-form-row b-blog-one-column__text"><span class="f-more"><spring:message code="offer.result.description"/></span> ${fn:substring(job.description, 0, 300)}...</div>
                                            <div class="b-form-row">
                                                <a href="offer.htm?id=${job.id}" class="b-btn f-btn b-btn-xs b-btn-default f-primary-b"><spring:message code="offer.result.read.more"/></a>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                            <div id="tabs-33" class="clearfix">
                                <c:forEach var="application" items="${applications}">
                                    <h4 class="f-tabs-vertical__title f-primary-b">${application.job.title}</h4>
                                    <p><ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col"> 
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        ${application.applicationTime}   
                                    </li>
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        ${application.status}   
                                    </li>
                                </ul></p>
                                <a href="offer.htm?id=${application.job.id}" class="f-more f-primary-b"><spring:message code="offer.result.read.more"/>...</a>
                                </c:forEach> 
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                    <div class="row b-col-default-indent">
                        <div class="col-md-12">
                            <div class="b-categories-filter">
                                <h4 class="f-primary-b b-h4-special f-h4-special--gray f-h4-special"><spring:message code="right.side.panel.sector.title"/></h4>
                                <ul>
                                    <c:forEach var="report" items="${reports}">
                                        <li>
                                            <a class="f-categories-filter_name" href="sector-directory.htm?page=1&sector=${report.sector}"><i class="fa fa-plus"></i> ${report.sector}</a>
                                            <span class="b-categories-filter_count f-categories-filter_count">${report.availableJobs}</span>
                                        </li>
                                    </c:forEach>       
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">popular postes</h4>
                            <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                                <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Phasellus id mattis dolorunc et erat hendrerit, tincidunt</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Vel eleifend id ullamcorper eu velit</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                14, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Lorem ipsum dolor sit amet purus vitae magna rhoncus</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b hidden">
                                        <div class="b-blog-short-post__item_img">
                                            <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                        </div>
                                        <div class="b-remaining">
                                            <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                                <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                            </div>
                                            <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                                10, January, 2014
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md12">
                            <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">tags cloud</h4>
                            <div>
                                <c:forEach var="tag" items="${tags}">
                                    <a class="f-tag b-tag" href="#">${tag.description}</a>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>