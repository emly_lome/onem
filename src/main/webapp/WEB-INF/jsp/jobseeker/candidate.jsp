<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="job.seeker.detail.title"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li class="f-secondary-l"><i class="fa fa-angle-right"></i><a href="candidates.htm"><spring:message code="menu.candidates"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="job.seeker.detail.title"/> </span></li>
            </ul>
        </div>
    </div>
</div>
<div class="container ">
    <div class="l-inner-page-container">
        <div class="row">
            <div class="col-md-9">
                <div class="b-detail-home-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="b-form-row f-primary-l f-detail-home-content_title c-secondary f-h4-special b-blog__title clearfix">
                                ${seeker.title} ${seeker.firstName} ${seeker.lastName}
                            </div>
                            <div class="b-form-row f-h4-special clearfix">
                                <div class="b-blog-one-column__info_container b-blog-one-column__info_container--estate">
                                    <span class="b-txt-wrap">
                                        <button class="button-xs b-btn-title-real-estate f-primary-b">${seeker.status}</button>
                                    </span>
                                    <span class="b-txt-wrap"><span class="f-info b-info"><span class="f-primary-b">${seeker.highestQualification}</span> Age : ${age} years</span>
                                        <span class="b-blog-one-column__info_delimiter"></span></span>
                                    <span class="b-txt-wrap"> <spring:message code="job.seeker.registration.form.personel.ref.no"/>: ${seeker.refno}
                                        <span class="b-blog-one-column__info_delimiter"></span></span>
                                    <span class="b-txt-wrap"><i class="b-icon b-icon--garage"></i> ${seeker.address}, ${seeker.town}
                                        <span class="b-blog-one-column__info_delimiter"></span></span>
                                        <c:if test="${!empty cv}">
                                        <span class="b-txt-wrap"> 
                                            <a href="open-cv.htm?id=${seeker.id}&file=${cv.name}" target="blank_"><spring:message code="job.seeker.detail.cv"/></a></span>
                                        </c:if>
                                </div>
                            </div>
                            <p class="f-primary-l">${seeker.description}</p>                            
                        </div>
                    </div>
                </div>
                <div class="b-detail-home-content">
                    <div class="b-tabs f-tabs j-tabs b-tabs-reset b-tabs--secondary f-tabs--secondary">
                        <ul>
                            <li><a href="#tabs-31"><spring:message code="job.seeker.area.addional.details"/></a></li>
                        </ul>
                        <div class="b-tabs__content">
                            <div id="tabs-31" class="clearfix">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="b-home-detail-option f-home-detail-option">
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.dob"/>:</div>
                                                <div class="b-home-detail-option_item_value f-home-detail-option_item_value f-primary-b">${seeker.dob}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.gender"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.gender}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.marital.status"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.maritalStatus}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="b-home-detail-option f-home-detail-option">

                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.email"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.contactEmail}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.personel.phone"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.contactNo}</div>
                                            </div>
                                            <div class="b-home-detail-option_row">
                                                <div class="b-home-detail-option_item_title"><spring:message code="job.seeker.registration.form.profile.years.experiance"/>:</div>
                                                <div class="b-home-detail-option_item_value f-primary-b">${seeker.yearsExperience}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row b-col-default-indent">
                    <div class="col-md-12">
                        <div class="b-categories-filter">
                            <h4 class="f-primary-b b-h4-special f-h4-special--gray f-h4-special"><spring:message code="right.side.panel.sector.title"/></h4>
                            <ul>
                                <c:forEach var="report" items="${reports}">
                                    <li>
                                        <a class="f-categories-filter_name" href="sector-directory.htm?page=1&sector=${report.sector}"><i class="fa fa-plus"></i> ${report.sector}</a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${report.availableJobs}</span>
                                    </li>
                                </c:forEach>       
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">popular postes</h4>
                        <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                            <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Phasellus id mattis dolorunc et erat hendrerit, tincidunt</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Vel eleifend id ullamcorper eu velit</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            14, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Lorem ipsum dolor sit amet purus vitae magna rhoncus</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b hidden">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md12">
                        <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">tags cloud</h4>
                        <div>
                            <c:forEach var="tag" items="${tags}">
                                <a class="f-tag b-tag" href="#">${tag.description}</a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>