<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="job.seeker.registration.title"/></h1>
        </div>
    </div>
</div>
<div class="b-breadcrumbs f-breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
            <li><i class="fa fa-angle-right"></i><span><spring:message code="job.seeker.registration.title"/></span></li>
        </ul>
    </div>
</div>
<div class="l-main-container">
    <section class="b-infoblock">
        <div class="container">
            <div class="row b-col-default-indent">
                <c:if test="${!empty error}">
                    <div class="b-alert-warning f-alert-warning">
                        <div class="b-right">
                            <i class="fa fa-times-circle-o"></i>
                        </div>
                        <div class="b-remaining">
                            <i class="fa fa-exclamation-triangle"></i> ${error}
                        </div>
                    </div>
                </c:if>
                <form:form action="save-job-seeker.htm" class="b-col-default-indent" commandName="form"
                           enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="b-shadow-container b-remaining b-form-style-small">
                            <div class="col-md-12">
                                <p class="f-title-medium c-default f-uppercase f-primary-b b-margin-top-small"><spring:message code="job.seeker.registration.form.title.personal"/></p>
                                <hr class="b-hr-light">
                            </div>

                            <div class="col-md-6">
                                <spring:message code="job.seeker.registration.form.email" var="email"/>
                                <form:input path="email" type="text" class="form-control" placeholder="${email}"/>
                            </div>
                            <div class="col-md-3">
                                <spring:message code="job.seeker.registration.form.password" var="password"/>
                                <form:input path="password" type="password" class="form-control" placeholder="${password}"/>
                            </div>
                            <div class="col-md-3">
                                <input type="password" class="form-control" placeholder="<spring:message code="job.seeker.registration.form.retype.password"/>">
                            </div>
                            <div class="col-md-3 b-form-row b-form-select b-select--alt">
                                <form:select path="title" class="j-select">
                                    <option selected="selected"><spring:message code="job.seeker.registration.form.personel.title.mr"/></option>
                                    <option><spring:message code="job.seeker.registration.form.personel.title.miss"/></option>
                                    <option><spring:message code="job.seeker.registration.form.personel.title.mrs"/></option>
                                </form:select>
                            </div>
                            <div class="col-md-3">
                                <spring:message code="job.seeker.registration.form.personel.first.name" var="firstname"/>
                                <form:input path="firstname" type="text" class="form-control" placeholder="${firstname}"/>
                            </div>
                            <div class="col-md-6">
                                <spring:message code="job.seeker.registration.form.personel.last.name" var="lastname"/>
                                <form:input path="lastname" type="text" class="form-control" placeholder="${lastname}"/>
                            </div>
                            <div class="col-md-4">
                                <div class="b-form-control__icon-wrap">
                                    <spring:message code="job.seeker.registration.form.personel.dob" var="dob"/>
                                    <form:input path="dob" type="text" class="form-control datepicker" placeholder="${dob}"/>
                                    <i class="fa fa-calendar b-form-control__icon f-form-control__icon"></i>
                                </div>
                            </div>
                            <div class="col-md-4 b-form-row b-form-select b-select--alt">
                                <form:select path="gender" class="j-select">
                                    <option selected="selected"><spring:message code="job.seeker.registration.form.personel.gender.male"/></option>
                                    <option><spring:message code="job.seeker.registration.form.personel.gender.female"/></option>
                                </form:select>
                            </div>
                            <div class="col-md-4 b-form-row b-form-select b-select--alt">
                                <form:select path="maritalStatus" class="j-select">
                                    <option selected="selected"><spring:message code="job.seeker.registration.form.personel.marital.status.single"/></option>
                                    <option><spring:message code="job.seeker.registration.form.personel.marital.status.married"/></option>
                                    <option><spring:message code="job.seeker.registration.form.personel.marital.status.divorced"/></option>
                                    <option><spring:message code="job.seeker.registration.form.personel.marital.status.widow"/></option>
                                </form:select>
                            </div>
                            <div class="col-md-8">
                                <spring:message code="job.seeker.registration.form.personel.address" var="address"/>
                                <form:input path="address" type="text" class="form-control" placeholder="${address}"/>
                            </div>
                            <div class="col-md-4">
                                <spring:message code="job.seeker.registration.form.personel.town" var="town"/>
                                <form:input path="town" type="text" class="form-control" placeholder="${town}"/>
                            </div>
                            <div class="col-md-4 b-form-row b-form-select b-select--alt">
                                <form:select path="status" class="j-select">
                                    <option selected="selected"><spring:message code="job.seeker.registration.form.personel.status.available"/></option>
                                    <option><spring:message code="job.seeker.registration.form.personel.status.non.available"/></option>
                                </form:select>
                            </div>
                            <div class="col-md-4">
                                <spring:message code="job.seeker.registration.form.personel.phone" var="phone"/>
                                <form:input path="phone" type="text" class="form-control" placeholder="${phone}"/>
                            </div>
                            <div class="col-md-4">
                                <spring:message code="job.seeker.registration.form.personel.ref.no" var="ref"/>
                                <form:input path="refno" type="text" class="form-control disabled" placeholder="${ref}"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="b-shadow-container b-remaining b-form-style-small">
                            <div class="col-md-12">
                                <p class="f-title-medium c-default f-uppercase f-primary-b b-margin-top-small"><spring:message code="job.seeker.registration.form.title.profile"/></p>
                                <hr class="b-hr-light">
                            </div>

                            <div class="col-md-6">
                                <spring:message code="job.seeker.registration.form.profile.high.qualification" var="qualification"/>
                                <form:input path="qualification" type="text" class="form-control" placeholder="${qualification}"/>
                            </div>
                            <div class="col-md-6">
                                <spring:message code="job.seeker.registration.form.profile.years.experiance" var="experience"/>
                                <form:input path="experience" type="text" class="form-control" placeholder="${experience}"/>
                            </div>
                            <div class="col-md-12">
                                <spring:message code="job.seeker.registration.form.profile.description" var="description"/>
                                <form:textarea path="description" class="form-control" placeholder="${description}" rows="9"/>
                            </div>                            
                            <div class="col-md-7">
                                <div class="b-file-upload file_upload">
                                    <button class="b-file-upload__btn fa fa-link" type="button"></button>
                                    <div class="b-file-upload__text form-control" ></div>
                                    <input name="cv" type="file"/>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <input type="submit" name="<spring:message code="job.seeker.registration.form.action"/>" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </section>
</div>