<div class="l-main-container">
    <div class="b-page-error">
        <div class="container">
            <div class="b-title-error f-title-error">
                <span class="f-primary-eb">404</span>
                <strong class="f-primary-l">Page Not Found !</strong>
            </div>
            <div class="b-error-description f-error-description">
                <strong class="f-primary">Sorry, this page does not exist</strong>
                <span class="f-primary">The link you clicked might be corrupted, or the page may have been removed.</span>
            </div>
            <div class="b-error-search">
                <div class="b-form-row b-input-search">
                    <input class="form-control" type="text" placeholder="Enter your keyword" />
                    <span class="b-btn b-btn-search f-btn-search fa fa-search"></span>
                </div>
            </div>
        </div>
    </div>
</div>