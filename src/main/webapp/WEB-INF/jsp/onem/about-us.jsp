<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.about.us"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span><spring:message code="menu.about.us"/></span></li>
            </ul>
        </div>
    </div>
    <section class="b-diagonal-line-bg-light b-infoblock--small ">
        <div class="container">
            <div class="row b-col-default-indent">
                <div class="col-md-6 col-xs-12">
                    <div class="b-slidercontainer b-small-arr f-small-arr b-shadow-container">
                        <div class="b-slider j-smallslider">
                            <ul>
                                <li data-transition="3dcurtain-vertical" data-slotamount="12">
                                    <img data-retina src="img/slider/about-us-1.jpg">
                                </li>
                                <li data-transition="" data-slotamount="12">
                                    <img data-retina src="img/slider/about-us-2.jpg">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <h3 class="f-primary-b"><spring:message code="about.us.welcome"/></h3>
                    <p class="f-primary-l"><spring:message code="about.us.welcome.description.one"/> </p>
                    <p class="f-primary-l"><spring:message code="about.us.welcome.description.two"/> </p>
                    <p class="f-primary-l"><spring:message code="about.us.welcome.description.three"/> </p>                    
                </div>
            </div>
        </div>
    </section>
    <section class="b-remaining">
        <div class="container b-infoblock--without-border">
            <div class="row b-shortcode-example">
                <div  data-active="4" class="b-tabs-vertical b-tabs-vertical--default f-tabs-vertical j-tabs-vertical b-tabs-reset row">
                    <div class="col-md-3 col-sm-4 b-tabs-vertical__nav">
                        <ul>
                            <li><a class="f-primary-l" href="#tabs-1"><i class="fa fa-suitcase"></i> <spring:message code="sub.menu.about.us.what"/></a></li>
                            <li><a class="f-primary-l" href="#tabs-2"><i class="fa fa-flask"></i> <spring:message code="sub.menu.about.us.care"/></a></li>
                            <li><a class="f-primary-l" href="#tabs-3"><i class="fa fa-flag"></i> <spring:message code="about.us.service.job.seeker"/></a></li>
                            <li><a class="f-primary-l" href="#tabs-4"><i class="fa fa-users"></i> <spring:message code="about.us.service.enterprise"/></a></li>
                            <li><a class="f-primary-l" href="#tabs-5"><i class="fa fa-home"></i> <spring:message code="about.us.management"/></a></li>
                            <li><a class="f-primary-l" href="#tabs-6"><i class="fa fa-comments"></i> <spring:message code="about.us.daily.management"/></a></li>
                        </ul>
                    </div>
                    <div class="col-md-9 col-sm-8 b-tabs-vertical__content">
                        <div id="tabs-1">
                            <div class="b-tabs-vertical__content-text">
                                <h3 class="f-tabs-vertical__title f-primary-b"><spring:message code="sub.menu.about.us.what"/>?</h3>
                                <p><spring:message code="about.us.what.we.do.one"/></p>
                                <p><spring:message code="about.us.what.we.do.two"/></p>
                            </div>
                        </div>
                        <div id="tabs-2">
                            <div class="b-tabs-vertical__content-text">
                                <h3 class="f-tabs-vertical__title f-primary-b"><spring:message code="sub.menu.about.us.care"/>?</h3>
                                <p><spring:message code="about.us.what.we.care.one"/> </p>
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col"> 
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.what.we.care.two"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.what.we.care.three"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.what.we.care.four"/>
                                    </li>
                            </div>
                        </div>
                        <div id="tabs-3">
                            <div class="b-tabs-vertical__content-text">
                                <h3 class="f-tabs-vertical__title f-primary-b"><spring:message code="about.us.service.job.seeker"/></h3>
                                <p><spring:message code="about.us.service.job.seeker.one"/></p>
                                <p><spring:message code="about.us.service.job.seeker.two"/></p>
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col"> 
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.service.job.seeker.three"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>  
                                        <spring:message code="about.us.service.job.seeker.four"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.service.job.seeker.five"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs-4">
                            <div class="b-tabs-vertical__content-text">
                                <h3 class="f-tabs-vertical__title f-primary-b"><spring:message code="about.us.service.enterprise"/></h3>
                                <p><spring:message code="about.us.service.enterprise.one"/></p>
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col"> 
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.service.enterprise.two"/>   
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.service.enterprise.three"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.service.enterprise.four"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.service.enterprise.five"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs-5">
                            <div class="b-tabs-vertical__content-text">
                                <h3 class="f-tabs-vertical__title f-primary-b"><spring:message code="about.us.management"/></h3>
                                <p><spring:message code="about.us.management.description.one"/></p>
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col"> 
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.management.description.two"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.management.description.three"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.management.description.four"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.management.description.five"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs-6">
                            <div class="b-tabs-vertical__content-text">
                                <h3 class="f-tabs-vertical__title f-primary-b"><spring:message code="about.us.daily.management"/></h3>
                                <p><spring:message code="about.us.daily.management.one"/></p>
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col"> 
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.daily.management.two"/>
                                    </li><br />
                                    <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.daily.management.three"/>
                                    </li><br /><li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.daily.management.four"/>
                                    </li><br /><li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                        <spring:message code="about.us.daily.management.five"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="b-google-map map-theme">
        <div class="b-google-map__map-view b-google-map__map-height">
            <!-- Google map load -->
        </div>
        <img data-retina src="img/google-map-marker-default.png" data-label="" class="marker-template hidden" />
        <div class="b-google-map__info-window-template hidden"
             data-selected-marker="0"
             data-width="526">
            <div class="b-google-map__info-window col-xs-12">
                <div class="col-lg-7 col-xs-12 b-google-map__info-window-address">
                    <ul class="list-unstyled">
                        <li class="col-xs-12">
                            <div class="b-google-map__info-window-address-icon f-center pull-left">
                                <i class="fa fa-home"></i>
                            </div>
                            <div>
                                <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                    <spring:message code="onem.sub.title"/>
                                </div>
                                <div class="desc"><spring:message code="onem.address.info.line"/></div>
                            </div>
                        </li>
                        <li class="col-xs-12">
                            <div class="b-google-map__info-window-address-icon f-center pull-left">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div>
                                <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                    <spring:message code="menu.home"/>
                                </div>
                                <div class="desc"><spring:message code="onem.web.info"/></div>
                            </div>
                        </li>
                        <li class="col-xs-12">
                            <div class="b-google-map__info-window-address-icon f-center pull-left">
                                <i class="fa fa-skype"></i>
                            </div>
                            <div>
                                <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                    Skype
                                </div>
                                <div class="desc"><spring:message code="onem.skype.info"/></div>
                            </div>
                        </li>
                        <li class="col-xs-12">
                            <div class="b-google-map__info-window-address-icon f-center pull-left">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div>
                                <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                    email
                                </div>
                                <div class="desc"><spring:message code="onem.email.info"/></div>
                            </div>
                        </li>
                    </ul>

                </div>
                <div class="col-lg-5 b-google-map__info-window-image hidden-xs hidden-sm hidden-md">
                    <img data-retina src="img/google-map-skyscrapper.png" style="width: 218px; height: 243px;" alt=""/>
                </div>
            </div>
        </div>
        <div class="b-contact-form container">
            <div class="b-contact-form__window c-primary">
                <form:form name="message" action="send-message.htm" method="POST" commandName="message">
                    <div class="col-xs-12 b-contact-form__window-title f-contact-form__window-title text-uppercase f-primary-b">
                        <spring:message code="contact.form.title"/>
                        <hr />
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="b-contact-form__window-form-row">
                            <label for="contact_form_name" class="b-contact-form__window-form-row-label"><spring:message code="contact.form.name"/></label>
                            <form:input type="text" path="name" class="form-control" id="contact_form_name" />
                        </div>
                        <div class="b-contact-form__window-form-row">
                            <label for="contact_form_email" class="b-contact-form__window-form-row-label"><spring:message code="contact.form.email"/></label>
                            <form:input type="text" path="email" class="form-control" id="contact_form_email" />
                        </div>
                        <div class="b-contact-form__window-form-row">
                            <label for="contact_form_website" class="b-contact-form__window-form-row-label"><spring:message code="contact.form.website"/></label>
                            <form:input type="text" path="website" class="form-control" id="contact_form_website" />
                        </div>
                        <div class="b-contact-form__window-form-row">
                            <label for="contact_form_title" class="b-contact-form__window-form-row-label"><spring:message code="contact.form.message.title"/></label>
                            <form:input type="text" path="subject" class="form-control" id="contact_form_title" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="b-contact-form__window-form-row">
                            <label for="contact_form_message" class="b-contact-form__window-form-row-label"><spring:message code="contact.form.title"/></label>
                            <form:textarea id="contact_form_message" path="message" rows="7" class="b-contact-form__window-form-textarea form-control"/>
                        </div>
                        <div class="b-contact-form__window-form-row">
                            <label for="contact_form_copy" class="b-contact-form__window-form-row-label">
                                <input type="checkbox" id="contact_form_copy" class="b-form-checkbox" />
                                <span><span class="f-primary"><spring:message code="contact.form.send.me.copy"/></span></span>
                            </label>
                        </div>
                        <div class="b-contact-form__window-form-row">
                            <input type="submit" name="<spring:message code="contact.form.send.message"/>" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-contact-form__window-form-row-button">
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </section>
</div>