<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="job.seeker.detail.title"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li class="f-secondary-l"><i class="fa fa-angle-right"></i><a href="enterprises.htm"><spring:message code="menu.enterprise"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="enterprise.detail.title"/> </span></li>
            </ul>
        </div>
    </div>
</div>
<section class=" b-infoblock">
    <div class="container">
        <div class="f-carousel-secondary b-portfolio__example-box f-some-examples-tertiary b-carousel-reset b-carousel-arr-square b-carousel-arr-square--big f-carousel-arr-square">
            <div class="b-carousel-title f-carousel-title f-carousel-title__color f-secondary-b">
                ${enterprise.name}                   
            </div>
        </div>
    </div>
</section>
<section class="b-education-detail-box b-diagonal-line-bg-light b-infoblock">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="b-tabs f-tabs j-tabs b-tabs-reset">
                    <ul>
                        <li><a href="#tabs-21"><spring:message code="enterprise.detail.description"/></a></li>
                        <li><a href="#tabs-22"><spring:message code="enterprise.detail.offers"/></a></li>
                    </ul>
                    <div class="b-tabs__content">
                        <div id="tabs-21">
                            <h4 class="f-tabs-vertical__title f-primary-b"><spring:message code="enterprise.detail.description.title"/></h4>
                            <p>${enterprise.description}</p>
                        </div>
                        <div id="tabs-22">
                            <c:forEach var="job" end="4" items="${jobs}">
                                <div class="b-search-result-box">
                                    <div class="b-search-item b-diagonal-line-bg-light">
                                        <h3 class="f-primary-l  is-global-title f-h4-special"><a href="offer.htm?id=${job.id}" class="f-more">${job.title}</a></h3>
                                        <div class="b-form-row f-h4-special b-form--mini clearfix">
                                            <div class="pull-left">
                                                <a href="blog_detail_right_slidebar.html" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate b-blog-one-column__info_edit">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </div>
                                            <div class="b-blog-one-column__info_container">
                                                <div class="b-blog-one-column__info f-blog-one-column__info">
                                                    <spring:message code="offer.result.category"/> : <span class="f-more">${job.type}</span>
                                                    <span class="b-blog-one-column__info_delimiter"></span>
                                                    <a href="blog_detail_right_slidebar.html#comment" class="f-more f-primary"><i class="fa fa-comment"></i>12 <spring:message code="offer.result.no.application"/></a>
                                                    <span class="b-blog-one-column__info_delimiter"></span>
                                                    <spring:message code="offer.result.sector"/> : <span class="f-more">${job.sector}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b-form-row b-blog-one-column__text"><span class="f-more"><spring:message code="offer.result.description"/></span> ${fn:substring(job.description, 0, 300)}...</div>
                                        <div class="b-form-row">
                                            <a href="offer.htm?id=${job.id}" class="b-btn f-btn b-btn-xs b-btn-default f-primary-b"><spring:message code="offer.result.read.more"/></a>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="f-secondary-b f-title-b-hr f-h4-special f-title-medium"><spring:message code="enterprise.detail.contact"/></div>
                <div class="b-information-box f-information-box f-primary-b b-information--max-size">
                    <ul>
                        <li>
                            <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="enterprise.detail.contact.name"/></strong>
                            <i class="b-dotted f-dotted">:</i>
                            <span class="f-information_data">${enterprise.contactName}</span>
                        </li>
                        <li>
                            <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="enterprise.detail.contact.postion"/></strong>
                            <i class="b-dotted f-dotted">:</i>
                            <span class="f-information_data">${enterprise.contactPosition}</span>
                        </li>
                        <li>
                            <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="enterprise.detail.contact.email"/></strong>
                            <i class="b-dotted f-dotted">:</i>
                            <span class="f-information_data">${enterprise.contactEmail}</span>
                        </li>
                        <li>
                            <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="enterprise.detail.contact.phone"/></strong>
                            <i class="b-dotted f-dotted">:</i>
                            <span class="f-information_data">${enterprise.contactNo}</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>