<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.candidates"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">

    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span><spring:message code="menu.candidates"/></span></li>
            </ul>
        </div>
    </div>
    <section class="b-infoblock--small">
        <div class="container">
            <div class="row b-col-default-indent">
                <div class="col-md-9">
                    <h3 class="is-global-title">${no} <spring:message code="candidates.subtitle"/></h3>

                    <div class="j-accordion b-accordion f-accordion b-accordion--secondary b-accordion--with-icon-indent f-accordion--secondary">
                        <c:forEach var="candidate" items="${candidates}">
                            <h3 class="b-accordion__title f-accordion__title f-primary-b">
                                <i class="fa fa-users"></i> ${candidate.title} ${candidate.firstName} ${candidate.lastName} - ${candidate.highestQualification}
                            </h3>                        

                            <div class="b-accordion__content">
                                <p>${fn:substring(candidate.description, 0, 300)}...<br/>
                                    <a href="job-seeker.htm?id=${candidate.id}" class="f-more f-primary-b"><spring:message code="offer.result.read.more"/></a></p>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <aside class="col-md-3">
                    <div class="b-categories-filter b-default-top-indent">
                        <h4 class="f-primary-b b-h4-special f-h4-special--gray f-h4-special"><spring:message code="right.side.panel.sector.title"/></h4>
                                <ul>
                                    <c:forEach var="report" items="${reports}">
                                        <li>
                                            <a class="f-categories-filter_name" href="sector-directory.htm?page=1&sector=${report.sector}"><i class="fa fa-plus"></i> ${report.sector}</a>
                                            <span class="b-categories-filter_count f-categories-filter_count">${report.availableJobs}</span>
                                        </li>
                                    </c:forEach>       
                                </ul>
                    </div>
                    <div class="b-default-top-indent">
                        <h4 class="f-primary-b b-h4-special f-h4-special c-primary">submit your questions</h4>

                        <div class="b-form-row">
                            <input type="text" class="form-control" placeholder="Your email"/>
                        </div>
                        <div class="b-form-row">
                            <input type="text" class="form-control" placeholder="Subject"/>
                        </div>
                        <div class="b-form-row">
                            <textarea class="form-control" placeholder="Your questions" rows="5"></textarea>
                        </div>
                        <div class="b-form-row">
                            <a href="#" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">submit
                                questions</a>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>

</div>