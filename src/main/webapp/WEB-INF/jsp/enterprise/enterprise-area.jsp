<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default">${enterprise.name} <spring:message code="enterprise.area.title"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="enterprise.area.title"/> </span></li>
            </ul>
        </div>
    </div>
</div>
<div class="container ">
    <div class="l-inner-page-container">
        <div class="row">
            <div class="col-md-9">
                <div class="b-tabs f-tabs j-tabs b-tabs-reset">
                    <ul>
                        <li><a href="#tabs-21"><spring:message code="enterprise.detail.description"/></a></li>
                        <li><a href="#tabs-22"><spring:message code="enterprise.detail.contact.title"/></a></li>
                        <li><a href="#tabs-23"><spring:message code="enterprise.detail.offers"/></a></li>
                    </ul>
                    <div class="b-tabs__content">
                        <div id="tabs-21">
                            <h4 class="f-tabs-vertical__title f-primary-b"><spring:message code="enterprise.detail.description.title"/></h4>
                            <p>${enterprise.description}</p>
                        </div>
                        <div id="tabs-22">
                            <h4 class="f-tabs-vertical__title f-primary-b"><spring:message code="enterprise.detail.contact"/></h4>
                            <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col">
                                <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                    <spring:message code="enterprise.detail.contact.name"/>: ${enterprise.contactName}
                                </li>
                                <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                    <spring:message code="enterprise.detail.contact.postion"/>: ${enterprise.contactPosition}
                                </li>
                                <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                    <spring:message code="enterprise.detail.contact.email"/>: ${enterprise.contactEmail}
                                </li>
                                <li><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>
                                    <spring:message code="enterprise.detail.contact.phone"/>: ${enterprise.contactNo}
                                </li>
                            </ul>
                        </div>
                        <div id="tabs-23">
                            <c:forEach var="job" end="4" items="${offers}">
                                <div class="b-search-result-box">
                                    <div class="b-search-item b-diagonal-line-bg-light">
                                        <h3 class="f-primary-l  is-global-title f-h4-special"><a href="offer.htm?id=${job.id}" class="f-more">${job.title}</a></h3>
                                        <div class="b-form-row f-h4-special b-form--mini clearfix">
                                            <div class="pull-left">
                                                <a href="blog_detail_right_slidebar.html" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate b-blog-one-column__info_edit">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </div>
                                            <div class="b-blog-one-column__info_container">
                                                <div class="b-blog-one-column__info f-blog-one-column__info">
                                                    <spring:message code="offer.result.category"/> : <span class="f-more">${job.type}</span>
                                                    <span class="b-blog-one-column__info_delimiter"></span>
                                                    <i class="fa fa-comment"></i>${fn:length(jobOfferApplications)} <spring:message code="offer.result.no.application"/>
                                                    <span class="b-blog-one-column__info_delimiter"></span>
                                                    <spring:message code="offer.result.sector"/> : <span class="f-more">${job.sector}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="b-form-row b-blog-one-column__text"><span class="f-more"><spring:message code="offer.result.description"/></span> ${fn:substring(job.description, 0, 300)}...</div>
                                        <div class="b-form-row">
                                            <a href="offer.htm?id=${job.id}" class="b-btn f-btn b-btn-xs b-btn-default f-primary-b"><spring:message code="offer.result.read.more"/></a>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                            <div class="row b-pagination f-pagination">
                                <div class="container">
                                    <ul>
                                        <li><a href="directory.htm?page=1">First</a></li>
                                        <li><a class="prev" href="directory.htm?page=<c:if test="${selected - 1 > 0}">${selected - 1}</c:if>"><i class="fa fa-angle-left"></i></a></li>
                                                <c:forEach var="page" begin="1" end="${pages}">
                                            <li <c:if test="${page == selected}">class="is-active-pagination"</c:if>><a href="directory.htm?page=${page}">${page}</a></li>
                                            </c:forEach>
                                        <li><a class="next" href="directory.htm?page=<c:if test="${selected + 1 == pages}">${selected + 1}</c:if>"><i class="fa fa-angle-right"></i></a></li>
                                        <li><a href="directory.htm?page=${pages}">Last</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="row b-col-default-indent">
                    <div class="col-md-12">
                        <div class="b-categories-filter">
                            <h4 class="f-primary-b b-h4-special f-h4-special--gray f-h4-special"><spring:message code="right.side.panel.sector.title"/></h4>
                            <ul>
                                <c:forEach var="report" items="${reports}">
                                    <li>
                                        <a class="f-categories-filter_name" href="sector-directory.htm?page=1&sector=${report.sector}"><i class="fa fa-plus"></i> ${report.sector}</a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${report.availableJobs}</span>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">popular postes</h4>
                        <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                            <div class="b-blog-short-post b-blog-short-post--img-hover-bordered b-blog-short-post--w-img f-blog-short-post--w-img row">
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Phasellus id mattis dolorunc et erat hendrerit, tincidunt</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Vel eleifend id ullamcorper eu velit</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            14, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Lorem ipsum dolor sit amet purus vitae magna rhoncus</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                                <div class="b-blog-short-post--popular col-md-12  col-xs-12 f-primary-b hidden">
                                    <div class="b-blog-short-post__item_img">
                                        <a href="#"><img data-retina src="img/shop/popular_1.png" alt=""/></a>
                                    </div>
                                    <div class="b-remaining">
                                        <div class="b-blog-short-post__item_text f-blog-short-post__item_text">
                                            <a href="#">Fusce vitae dui sit amet lacus rutrum convallis</a>
                                        </div>
                                        <div class="b-blog-short-post__item_date f-blog-short-post__item_date f-primary-it">
                                            10, January, 2014
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md12">
                        <h4 class="f-primary-b b-h4-special f-h4-special f-h4-special--gray b-h4-special--portfolio">tags cloud</h4>
                        <div>
                            <c:forEach var="tag" items="${tags}">
                                <a class="f-tag b-tag" href="#">${tag.description}</a>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>