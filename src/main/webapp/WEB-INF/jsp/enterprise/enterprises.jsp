<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.enterprise"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span><spring:message code="menu.enterprise"/></span></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="b-education-box b-infoblock">
                <c:forEach var="enterprise" items="${enterprises}">
                    <div class="b-some-examples__item f-some-examples__item">
                        <div class="b-some-examples__item_info">
                            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-secondary-b c-primary"><a href="enterprise.htm?id=${enterprise.id}">${enterprise.name}</a></div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info f-title-extra-small">
                                <div class="b-right"><a href="enterprise.htm?id=${enterprise.id}" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-secondary-b"><spring:message code="offer.result.read.more"/></a></div>
                                <div class="b-remaining">${enterprise.contactName} | ${enterprise.contactPosition} | ${enterprise.contactNo} | ${enterprise.contactEmail}</div>
                            </div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description f-secondary-l ">
                                ${fn:substring(enterprise.description, 0, 200)}...
                            </div>
                        </div>
                    </div>
                </c:forEach>                
            </div>
        </div>
    </div>
</div>