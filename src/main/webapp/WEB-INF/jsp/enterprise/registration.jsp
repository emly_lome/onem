<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="enterprise.registration.title"/></h1>
        </div>
    </div>
</div>
<div class="b-breadcrumbs f-breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
            <li><i class="fa fa-angle-right"></i><span><spring:message code="enterprise.registration.title"/></span></li>
        </ul>
    </div>
</div>

<div class="l-main-container">
    <section class="b-infoblock">
        <div class="container">
            <div class="row b-col-default-indent">
                <div class="col-md-6">
                    <div class="b-slidercontainer b-slider b-slider--thumb b-slider--thumb-visible b-small-arr f-small-arr b-slider--navi-alt b-shadow-container">
                        <div class="j-contentwidthslider" data-height="700">
                            <ul>
                                <li data-transition="slotfade-vertical" data-slotamount="25" >
                                    <img data-retina src="img/slider/section-bg-mobile.jpg">
                                </li>
                                <li data-transition="slotfade-vertical" data-slotamount="17" >
                                    <img data-retina src="img/slider/section-bg-education.jpg">
                                </li>
                                <li data-transition="slotfade-vertical" data-slotamount="7" >
                                    <img data-retina src="img/slider/section-bg-meadow.jpg">
                                </li>
                                <li data-transition="slotfade-vertical" data-slotamount="7" >
                                    <img data-retina src="img/slider/section-bg-street.jpg">
                                </li>
                                <li data-transition="slotfade-vertical" data-slotamount="7" >
                                    <img data-retina src="img/slider/slider-bg-video.jpg">
                                </li>
                                <li data-transition="slotfade-vertical" data-slotamount="7" >
                                    <img data-retina src="img/slider/services-work.jpg">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <c:if test="${!empty errors}">
                        <div class="b-alert-warning f-alert-warning">
                            <div class="b-right">
                                <i class="fa fa-times-circle-o"></i>
                            </div>
                            <div class="b-remaining">
                               
                               <c:forEach var="error" items="${errors}">
                                    <i class="fa fa-exclamation-triangle"></i> ${error}<br />
                                </c:forEach>                          
                            </div>
                        </div>
                    </c:if>
                    <div class="b-shadow-container b-remaining b-form-style-small">
                        <div class="col-md-12">
                            <p class="f-title-medium c-default f-uppercase f-primary-b b-margin-top-small">
                                <spring:message code="enterprise.registration.form.title"/></p>
                            <hr class="b-hr-light">
                        </div>                        
                        <form:form action="save-enterprise.htm" class="b-col-default-indent" commandName="enterprise">
                            <div class="col-md-4">
                                <form:errors path="username" cssClass="b-alert-warning f-alert-warning"/>
                                <spring:message code="enterprise.registration.form.username" var="username"/>
                                <form:input placeholder="${username}" type="text" path="username" class="form-control"/>
                            </div>
                            <div class="col-md-4">
                                <form:errors path="password"/>
                                <spring:message code="enterprise.registration.form.password" var="password"/>
                                <form:input path="password" type="password" class="form-control" placeholder="${password}"/>
                            </div>
                            <div class="col-md-4">
                                <input type="password" class="form-control" placeholder="<spring:message code="enterprise.registration.form.retype.password"/>">
                            </div>
                            <div class="col-md-12">
                                <form:errors path="name"/>
                                <spring:message code="enterprise.registration.form.name" var="name"/>
                                <form:input path="name" type="text" class="form-control" placeholder="${name}"/>
                            </div>
                            <div class="col-md-12">
                                <spring:message code="enterprise.registration.form.description" var="description"/>
                                <form:textarea path="description" class="form-control" placeholder="${description}" rows="8"/>
                            </div>
                            <div class="col-md-8">
                                <form:errors path="address"/>
                                <spring:message code="enterprise.registration.form.address" var="address"/>
                                <form:input path="address" type="text" class="form-control" placeholder="${address}"/>
                            </div>
                            <div class="col-md-4">
                                <form:errors path="town"/>
                                <spring:message code="enterprise.registration.form.town" var="town"/>
                                <form:input path="town" type="text" class="form-control" placeholder="${town}"/>
                            </div>
                            <div class="col-md-6">
                                <form:errors path="contact"/>
                                <spring:message code="enterprise.registration.form.contact.name" var="contact"/>
                                <form:input path="contact" type="text" class="form-control" placeholder="${contact}"/>
                            </div>
                            <div class="col-md-6">
                                <form:errors path="position"/>
                                <spring:message code="enterprise.registration.form.contact.position" var="position"/>
                                <form:input path="position" type="text" class="form-control" placeholder="${position}"/>
                            </div>
                            <div class="col-md-5">
                                <form:errors path="email"/>
                                <spring:message code="enterprise.registration.form.contact.email" var="email"/>
                                <form:input path="email" type="text" class="form-control" placeholder="${email}"/>
                            </div>
                            <div class="col-md-4">
                                <form:errors path="phone"/>
                                <spring:message code="enterprise.registration.form.contact.phone" var="phone"/>
                                <form:input path="phone" type="text" class="form-control" placeholder="${phone}"/>
                            </div>                           

                            <div class="col-md-3">
                                <input type="submit"  title="Test" name="<spring:message code="enterprise.registration.form.contact.save"/>" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100"/>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="b-infoblock b-diagonal-line-bg-light">
        <div class="b-carousel-reset b-carousel-arr-out b-carousel-small-arr f-carousel-small-arr b-remaining">
            <div class="f-center b-logo-group j-logo-slider">
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-1.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-1h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-2.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-2h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-3.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-3h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-4.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-4h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-5.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-5h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-3.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-3h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-4.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-4h.png" alt=""/>
                    </a></div>
                <div class="b-logo-item"><a href="#">
                        <img class="is-normal" src="img/slider/partner/client-logo-5.png" alt=""/>
                        <img class="is-hover" src="img/slider/partner/client-logo-5h.png" alt=""/>
                    </a></div>

            </div>
        </div>
    </section>
</div>