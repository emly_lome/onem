<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="job.apply.title"/></h1>
        </div>
    </div>
</div>
<div class="b-breadcrumbs f-breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
            <li class="f-secondary-l"><i class="fa fa-angle-right"></i><a href="directory.htm?page=${page}"><spring:message code="menu.rom"/></a></li>
            <li class="f-secondary-l"><i class="fa fa-angle-right"></i><a href="offer.htm?id=${job.id}"><spring:message code="offer.detail.title"/></a></li>
            <li class="f-secondary-l c-primary"><i class="fa fa-angle-right"></i><span><spring:message code="job.apply.title"/></span></li>
        </ul>
    </div>
</div>
<div class="l-main-container">
    <section class="b-infoblock">
        <div class="container">
            <div class="row b-col-default-indent">
                <c:if test="${!empty error}">
                    <div class="b-alert-warning f-alert-warning">
                        <div class="b-right">
                            <i class="fa fa-times-circle-o"></i>
                        </div>
                        <div class="b-remaining">
                            <i class="fa fa-exclamation-triangle"></i> ${error}
                        </div>
                    </div>
                </c:if>
                <form:form action="submit-application.htm?id=${job.id}" class="b-col-default-indent" commandName="form"
                           enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="b-shadow-container b-remaining b-form-style-small">
                            <div class="col-md-12">
                                <p class="f-title-medium c-default f-uppercase f-primary-b b-margin-top-small"><spring:message code="job.apply.message.title"/></p>
                                <hr class="b-hr-light">
                            </div>
                            <div class="col-md-12">
                                <p>${job.description}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="b-shadow-container b-remaining b-form-style-small">
                            <div class="col-md-12">
                                <p class="f-title-medium c-default f-uppercase f-primary-b b-margin-top-small"><spring:message code="job.apply.message.title"/></p>
                                <hr class="b-hr-light">
                            </div>
                            <div class="col-md-6">
                                <spring:message code="job.apply.to" var="to"/>
                                <form:input path="to" type="text" class="form-control" placeholder="${to}"/>
                            </div>
                            <div class="col-md-6">
                                <spring:message code="job.apply.cc" var="cc"/>
                                <form:input path="cc" type="text" class="form-control" placeholder="${cc}"/>
                            </div>
                            <div class="col-md-12">
                                <spring:message code="job.apply.subject" var="subject"/>
                                <form:input path="subject" type="text" class="form-control" placeholder="${subject}"/>
                            </div>
                            <div class="col-md-12">
                                <spring:message code="job.apply.message" var="message"/>
                                <form:textarea path="text" class="form-control" placeholder="${message}" rows="9"/>
                            </div>                            
                            <div class="col-md-7">
                                <div class="b-file-upload file_upload">
                                    <button class="b-file-upload__btn fa fa-link" type="button"></button>
                                    <div class="b-file-upload__text form-control" ></div>
                                    <input name="cv" type="file"/>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <input type="submit" name="<spring:message code="job.seeker.registration.form.action"/>" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100">
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </section>
</div>