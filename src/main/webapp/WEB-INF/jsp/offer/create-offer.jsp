
<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.create.job"/></h1>
        </div>
    </div>
</div>
<div class="b-breadcrumbs f-breadcrumbs">
    <div class="container">
        <ul>
            <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
            <li class="f-secondary-l"><i class="fa fa-angle-right"></i><a href="login.htm?home=enterprise"><spring:message code="menu.private.area"/></a></li>
            <li><i class="fa fa-angle-right"></i><span><spring:message code="menu.create.job"/></span></li>
        </ul>
    </div>
</div>

<div class="l-main-container">

    <section class="b-infoblock">
        <div class="container">
            <div class="row b-col-default-indent">
                <form:form action="save-job.htm" class="b-col-default-indent" commandName="jobForm">
                    <div class="col-md-6">
                        <div class="b-shadow-container b-remaining b-form-style-small">
                            <div class="col-md-12">
                                <p class="f-title-medium c-default f-uppercase f-primary-b b-margin-top-small"><spring:message code="create.job.form.title"/></p>
                                <hr class="b-hr-light">
                            </div>
                            <div class="col-md-6">
                                <spring:message code="create.job.form.postion.title" var="pos"/>
                                <form:input path="title" type="text" class="form-control" placeholder="${pos}"/>
                            </div>
                            <spring:message code="create.job.form.industry.sector" var="sector"/>

                            <div class="col-md-6">
                                <div class="b-form-row b-form-select b-select--alt">
                                    <form:select path="sector" class="j-select">
                                        <option selected="selected">--- ${sector} ---</option>
                                        <c:forEach var="sector" items="${sectors}">
                                            <option>${sector}</option>
                                        </c:forEach>                                        
                                    </form:select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <spring:message code="create.job.form.type" var="type"/>
                                <form:input path="type" type="text" class="form-control" placeholder="${type}"/>
                            </div>
                            <div class="col-md-4">
                                <spring:message code="create.job.form.location" var="location"/>
                                <form:input path="location" type="text" class="form-control" placeholder="${location}"/>
                            </div>
                            <div class="col-md-4">
                                <div class="b-form-control__icon-wrap">
                                    <spring:message code="create.job.form.closing.date" var="closing"/>
                                    <form:input path="closing" type="text" class="form-control datepicker" placeholder="${closing}"/>
                                    <i class="fa fa-calendar b-form-control__icon f-form-control__icon"></i>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <spring:message code="create.job.form.description" var="description"/>
                                <form:textarea path="description" class="form-control" placeholder="${description}" rows="11"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="b-shadow-container b-remaining b-form-style-small">
                            <div class="col-md-12">
                                <spring:message code="create.job.form.qualification" var="qualification"/>
                                <form:textarea path="qualification" class="form-control" placeholder="${qualification}" rows="8"/>
                            </div>
                            <div class="col-md-12">
                                <spring:message code="create.job.form.experience" var="experience"/>
                                <form:textarea path="experience" class="form-control" placeholder="${experience}" rows="9"/>
                            </div>
                            <div class="col-md-3">
                                <spring:message code="create.job.form.salary" var="salary"/>
                                <form:input path="salary" type="text" class="form-control" placeholder="${salary}"/>
                            </div>
                            <div class="col-md-3">
                                <spring:message code="create.job.form.contact.type" var="contact"/>
                                <form:input path="contact" type="text" class="form-control" placeholder="${contact}"/>
                            </div>
                            <div class="col-md-6">
                                <input type="submit" name="<spring:message code="create.job.form.action"/>" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100"/>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </section>
</div>

