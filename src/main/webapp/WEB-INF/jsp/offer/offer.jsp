
<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page_2">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="offer.detail.title"/></h1>      
        </div>
    </div>
</div>

<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs b-bg-breadcrumbs">
        <div class="container">
            <ul>
                <li class="f-secondary-l"><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li class="f-secondary-l"><i class="fa fa-angle-right"></i><a href="directory.htm?page=${page}"><spring:message code="menu.rom"/></a></li>
                <li class="f-secondary-l c-primary"><i class="fa fa-angle-right"></i><span><spring:message code="offer.detail.title"/></span></li>
            </ul>
        </div>
    </div>
    <section class="b-education-detail-box b-diagonal-line-bg-light b-infoblock">
        <div class="container">
            <div class="f-carousel-secondary b-portfolio__example-box f-some-examples-tertiary b-carousel-reset b-carousel-arr-square b-carousel-arr-square--big f-carousel-arr-square">
                <div class="b-carousel-title f-carousel-title f-carousel-title__color f-secondary-b">
                    ${offer.title}                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="b-tabs f-tabs j-tabs b-tabs-reset">
                        <ul>
                            <li><a href="#tabs-21"><spring:message code="offer.detail.description"/></a></li>
                            <li><a href="#tabs-22"><spring:message code="offer.detail.qualification"/></a></li>
                            <li><a href="#tabs-23"><spring:message code="offer.detail.experience"/></a></li>
                        </ul>
                        <div class="b-tabs__content">
                            <div id="tabs-21">
                                <h4 class="f-tabs-vertical__title f-primary-b"><spring:message code="offer.detail.job.description"/></h4>
                                <p>${offer.description}  </p>
                            </div>
                            <div id="tabs-22">
                                <h4 class="f-tabs-vertical__title f-primary-b"><spring:message code="offer.detail.expected.qualification"/></h4>
                                <p>${offer.qualification} </p>
                            </div>
                            <div id="tabs-23">
                                <h4 class="f-tabs-vertical__title f-primary-b"><spring:message code="offer.detail.experience.required"/></h4>
                                <p>${offer.experience} </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="f-secondary-b f-title-b-hr f-h4-special f-title-medium"><a href="apply.htm?id=${offer.id}" class="f-more"><spring:message code="offer.detail.to.apply"/></a></div>
                    <div class="b-information-box f-information-box f-primary-b b-information--max-size">
                        <ul>
                            <li>
                                <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="offer.detail.closing.date"/></strong>
                                <i class="b-dotted f-dotted">:</i>
                                <span class="f-information_data">${offer.closingDate}</span>
                            </li>
                            <li>
                                <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="offer.detail.type"/></strong>
                                <i class="b-dotted f-dotted">:</i>
                                <span class="f-information_data">${offer.type}</span>
                            </li>
                            <li>
                                <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="Offer.detail.location"/></strong>
                                <i class="b-dotted f-dotted">:</i>
                                <span class="f-information_data">${offer.location}</span>
                            </li>
                            <li>
                                <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="offer.detail.contact"/></strong>
                                <i class="b-dotted f-dotted">:</i>
                                <span class="f-information_data">${offer.enterprise.contactName}, ${offer.enterprise.contactPosition}</span>
                            </li>
                            <li>
                                <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="offer.detail.phone"/></strong>
                                <i class="b-dotted f-dotted">:</i>
                                <span class="f-information_data">${offer.enterprise.contactNo}</span>
                            </li>
                            <li>
                                <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="offer.detail.email"/></strong>
                                <i class="b-dotted f-dotted">:</i>
                                <span class="f-information_data">${offer.enterprise.contactEmail}</span>
                            </li>
                            <li>
                                <strong class="f-information-box__name b-information-box__name f-secondary-b"><spring:message code="offer.detail.client"/></strong>
                                <i class="b-dotted f-dotted">:</i>
                                <span class="f-information_data"><a href="enterprise.htm?id=${offer.enterprise.id}">${offer.enterprise.name}</a></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--    <section class="container b-infoblock--without-border">
        <div class="f-secondary-b f-title-b-hr">related courses</div>
        <div class="row b-shortcode-example">
            <div class="col-sm-6 col-md-4">
                <div class="b-some-examples__item f-some-examples__item">
                    <div class="b-some-examples__item_img view view-sixth">
                        <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/education/education_1.jpg" alt=""/></a>
                        <div class="b-item-hover-action f-center mask">
                            <div class="b-item-hover-action__inner">
                                <div class="b-item-hover-action__inner-btn_group">
                                    <a href="education_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-some-examples__item_info">
                        <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-secondary-b c-primary"><a href="education_detail.html">MA Advertising and Design</a></div>
                        <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info f-uppercase f-title-extra-small">
                            <div class="b-right">Duration: 3 Years</div>
                            <div class="b-remaining">Starts: January 31, 2014</div>
                        </div>
                        <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description f-secondary-l ">
                            Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis vestibulum quis quam vel accumsan  dertook grunted less jeez hound.
                        </div>
                    </div>
                    <div class="b-some-examples__item_action">
                        <div class="b-right">
                            <a href="education_detail.html" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-secondary-b">Apply now</a>
                        </div>
                        <div class="b-remaining b-some-examples__item_total f-some-examples__item_total f-secondary-b">$90/ month</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="b-some-examples__item f-some-examples__item">
                    <div class="b-some-examples__item_img view view-sixth">
                        <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/education/education_2.jpg" alt=""/></a>
                        <div class="b-item-hover-action f-center mask">
                            <div class="b-item-hover-action__inner">
                                <div class="b-item-hover-action__inner-btn_group">
                                    <a href="education_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-some-examples__item_info">
                        <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-secondary-b c-primary"><a href="education_detail.html">BSc Neuroscience.</a></div>
                        <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info f-uppercase f-title-extra-small">
                            <div class="b-right">Duration: 3 Years</div>
                            <div class="b-remaining">Starts: January 31, 2014</div>
                        </div>
                        <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description f-secondary-l ">
                            Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis vestibulum quis quam vel accumsan  dertook grunted less jeez hound.
                        </div>
                    </div>
                    <div class="b-some-examples__item_action">
                        <div class="b-right">
                            <a href="education_detail.html" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-secondary-b">Apply now</a>
                        </div>
                        <div class="b-remaining b-some-examples__item_total f-some-examples__item_total f-secondary-b">$90/ month</div>
                    </div>
                </div>
            </div>
            <div class="clearfix visible-sm-block"></div>
            <div class="col-sm-6 col-md-4">
                <div class="b-some-examples__item f-some-examples__item">
                    <div class="b-some-examples__item_img view view-sixth">
                        <a href="#"><img class="j-data-element" data-animate="fadeInDown" data-retina src="img/education/education_3.jpg" alt=""/></a>
                        <div class="b-item-hover-action f-center mask">
                            <div class="b-item-hover-action__inner">
                                <div class="b-item-hover-action__inner-btn_group">
                                    <a href="education_detail.html" class="b-btn f-btn b-btn-light f-btn-light info"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-some-examples__item_info">
                        <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-secondary-b c-primary"><a href="education_detail.html">Graduate Diploma in Human ...</a></div>
                        <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info f-uppercase f-title-extra-small">
                            <div class="b-right">Duration: 3 Years</div>
                            <div class="b-remaining">Starts: January 31, 2014</div>
                        </div>
                        <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description f-secondary-l ">
                            Suspendisse blandit ligula turpis, ac convallis risus fermentum non. Duis vestibulum quis quam vel accumsan  dertook grunted less jeez hound.
                        </div>
                    </div>
                    <div class="b-some-examples__item_action">
                        <div class="b-right">
                            <a href="education_detail.html" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-secondary-b">Apply now</a>
                        </div>
                        <div class="b-remaining b-some-examples__item_total f-some-examples__item_total f-secondary-b">$90/ month</div>
                    </div>
                </div>
            </div>
            <div class="clearfix hidden-sm hidden-xs"></div>
        </div>
    </section>-->

</div>