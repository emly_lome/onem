<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.home"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="menu.contact"/> </span></li>
            </ul>
        </div>
    </div>
    <section class="b-google-map map-theme b-bord-box" data-location-set="contact-us">
        <div class="b-google-map__map-view b-google-map__map-height">
            <!-- Google map load -->
        </div>
        <img data-retina src="img/google-map-marker-default.png" data-label="" class="marker-template hidden" />
        <div class="b-google-map__info-window-template hidden"
             data-selected-marker="0"
             data-width="250">
            <div class="b-google-map__info-window f-center b-google-map__info-office f-google-map__info-office">
                <h4 class="f-primary-b"><spring:message code="onem.sub.title"/></h4>
                <small><spring:message code="onem.office"/></small>
            </div>
        </div>
    </section>
    <div class="b-desc-section-container">
        <section class="container b-welcome-box">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <h1 class="is-global-title f-center"><spring:message code="contact.welcome"/></h1>
                    <p class="f-center"><spring:message code="contact.welcome.description"/> </p>
                </div>
            </div>
        </section>
        <section class="container">
            <div class="row">
                <div class="col-sm-6 b-contact-form-box">
                    <h3 class="f-primary-b b-title-description f-title-description">
                        <spring:message code="contact.form.title"/>
                        <div class="b-title-description__comment f-title-description__comment f-primary-l"><spring:message code="contact.welcome.description"/></div>
                        <c:if test="${!empty msg}">
                            <div class="b-alert-success f-alert-success">
                                <div class="b-right">
                                  <i class="fa fa-times-circle-o"></i>
                                </div>
                                <div class="b-remaining">
                                  <i class="fa fa-check-circle-o"></i> ${msg}
                                </div>
                             </div>
                        </c:if>                        
                    </h3>
                    <div class="row">
                        <form:form name="message" action="send-message.htm" method="POST" commandName="message">
                            <div class="col-md-6">
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="name"><spring:message code="contact.form.name"/></label>
                                    <div class="b-form-vertical__input">
                                        <form:input type="text" id="name" path="name" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="email"><spring:message code="contact.form.email"/></label>
                                    <div class="b-form-vertical__input">
                                        <form:input type="text" id="email" path="email" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="website"><spring:message code="contact.form.website"/></label>
                                    <div class="b-form-vertical__input">
                                        <form:input type="text" path="website" id="website" class="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-vertical__label" for="title"><spring:message code="contact.form.message.title"/></label>
                                    <div class="b-form-vertical__input">
                                        <form:input type="text" path="subject" id="title" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="b-form-row b-form--contact-size">
                                    <label class="b-form-vertical__label"><spring:message code="contact.form.message"/></label>
                                    <form:textarea class="form-control" path="message" rows="5"/>                                  
                                </div>
                                <div class="b-form-row">
                                    <input name="<spring:message code="contact.form.send.message"/>" type="submit" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100"/>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
                <div class="col-sm-6 b-contact-form-box">
                    <h3 class="f-primary-b b-title-description f-title-description">
                        <spring:message code="contact.info"/>
                        <div class="b-title-description__comment f-title-description__comment f-primary-l"><spring:message code="contact.info.description"/> </div>
                    </h3>
                    <div class="row b-google-map__info-window-address">
                        <ul class="list-unstyled">
                            <li class="col-xs-12">
                                <div class="b-google-map__info-window-address-icon f-center pull-left">
                                    <i class="fa fa-home"></i>
                                </div>
                                <div>
                                    <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                        <spring:message code="onem.sub.title"/>
                                    </div>
                                        <div class="desc"><spring:message code="onem.address.info.line"/></div>
                                </div>
                            </li>
                            <li class="col-xs-12">
                                <div class="b-google-map__info-window-address-icon f-center pull-left">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div>
                                    <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                        <spring:message code="menu.home"/>
                                    </div>
                                        <div class="desc"><spring:message code="onem.web.info"/></div>
                                </div>
                            </li>
                            <li class="col-xs-12">
                                <div class="b-google-map__info-window-address-icon f-center pull-left">
                                    <i class="fa fa-skype"></i>
                                </div>
                                <div>
                                    <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                        Skype
                                    </div>
                                    <div class="desc"><spring:message code="onem.skype.info"/></div>
                                </div>
                            </li>
                            <li class="col-xs-12">
                                <div class="b-google-map__info-window-address-icon f-center pull-left">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div>
                                    <div class="b-google-map__info-window-address-title f-google-map__info-window-address-title">
                                        email
                                    </div>
                                    <div class="desc"><spring:message code="onem.email.info"/></div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="b-diagonal-line-bg-light b-bord-box">
        <section class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="f-primary-b b-title-description f-title-description">like us on facebook</h3>
                    <div class="b-wiget-fb">
                        <div class="b-wiget-fb-content" id="fb-root">
                            <div class="fb-like-box" data-width="285" data-href="https://www.facebook.com/FacebookDevelopers"
                                 data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false"
                                 data-show-border="false"></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h3 class="f-primary-b b-title-description f-title-description">twitter feeds</h3>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="b-twitter-feeds__item">
                                <div class="b-twitter-feeds__item_name f-twitter-feeds__item_name f-primary-b"><i
                                        class="fa fa-twitter"></i> frexy studio
                                </div>
                                <div class="b-twitter-feeds__item_twit f-twitter-feeds__item_twit">
                                    Lorem ipsum dolor sit amet, consectetur adip iscing elit. Curabitur ut blandit sapien. <a
                                        href="http://t.co/hd3sk" target="_blank">http://t.co/hd3sk</a>
                                </div>
                                <div class="b-twitter-feeds__item_date f-twitter-feeds__item_date f-primary-it c-senary">2 days
                                    ago
                                </div>
                            </div>
                            <div class="b-twitter-feeds__item b-default-top-indent">
                                <div class="b-twitter-feeds__item_name f-twitter-feeds__item_name f-primary-b"><i
                                        class="fa fa-twitter"></i> John Doe
                                </div>
                                <div class="b-twitter-feeds__item_twit f-twitter-feeds__item_twit">
                                    Lorem ipsum dolor sit amet, consectetur adip iscing elit. Curabitur ut blandit sapien. <a
                                        href="http://t.co/hd3sk" target="_blank">http://t.co/hd3sk</a>
                                </div>
                                <div class="b-twitter-feeds__item_date f-twitter-feeds__item_date f-primary-it c-senary">2 days
                                    ago
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>