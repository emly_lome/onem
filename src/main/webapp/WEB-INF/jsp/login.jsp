<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="login.title"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li><i class="fa fa-angle-right"></i><span><spring:message code="login.title"/></span></li>
            </ul>
        </div>
    </div>
    <div class="container b-container-login-page">
        <div class="row">
            <div class="col-md-6">
                <div class="b-log-in-form">
                    <div class="f-primary-l f-title-big c-secondary"><spring:message code="login.welcome"/></div>
                    <p><spring:message code="login.welcome.description"/></p>
                    <hr class="b-hr" /> 
                    <c:if test="${!empty error}">
                        <div class="b-alert-warning f-alert-warning">
                            <div class="b-right">
                                <i class="fa fa-times-circle-o"></i>
                            </div>
                            <div class="b-remaining">
                                <i class="fa fa-exclamation-triangle"></i> ${error}<br />                        
                            </div>
                        </div>
                        <br />
                    </c:if>
                    <div class="b-form-row b-form-inline b-form-horizontal">
                        <div class="col-xs-12">
                            <form:form name="login" action="private-area.htm?home=${home}" method="POST" commandName="loginForm">
                                <div class="b-form-row">
                                    <label class="b-form-horizontal__label" for="create_account_email"><spring:message code="login.form.username"/></label>
                                    <div class="b-form-horizontal__input">
                                        <form:input type="text" id="create_account_email" path="username" cssClass="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <label class="b-form-horizontal__label" for="create_account_password"><spring:message code="login.form.password"/></label>
                                    <div class="b-form-horizontal__input">
                                        <form:input type="password" id="create_account_password" path="password" cssClass="form-control" />
                                    </div>
                                </div>
                                <div class="b-form-row">
                                    <div class="b-form-horizontal__label"></div>
                                    <label for="contact_form_copy" class="b-contact-form__window-form-row-label">
                                        <input type="checkbox" id="contact_form_copy" class="b-form-checkbox" />
                                        <span><spring:message code="login.remember.me"/></span>
                                    </label>
                                </div>
                                <div class="b-form-row">
                                    <div class="b-form-horizontal__label"></div>
                                    <div class="b-form-horizontal__input">
                                        <input name="<spring:message code="login.form.action"/>" type="submit" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100"/>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="f-primary-l f-title-big c-secondary"><spring:message code="login.create.account"/></div>
                <p><spring:message code="login.create.account.description"/> </p>
                <div class="b-shortcode-example">
                    <ul class="b-list-markers f-list-markers">
                        <li><a href="registration.htm?home=${home}"><i class="fa fa-check-circle b-list-markers__ico f-list-markers__ico"></i> <spring:message code="login.create.account"/></a></li>
<!--                        <li><a href="forgot-password.htm?type=username"><i class="fa fa-check-circle b-list-markers__ico f-list-markers__ico"></i> <spring:message code="login.forgot.username"/></a></li>-->
                        <li><a href="forgot-password.htm?type=password"><i class="fa fa-check-circle b-list-markers__ico f-list-markers__ico"></i> <spring:message code="login.forgot.password"/></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>