<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.enterprise"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li class="f-secondary-l"><a href="administrator.htm"><i class="fa fa-angle-right"></i><spring:message code="onem.header.administrator"/></a></li>
                <li></a><i class="fa fa-angle-right"></i><span><spring:message code="menu.manage.enterprise"/></span></li>
            </ul>
        </div>
    </div>
    <div class="l-inner-page-container">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="b-education-box b-infoblock">
                        <c:forEach var="enterprise" items="${enterprises}">
                    <div class="b-some-examples__item f-some-examples__item">
                        <div class="b-some-examples__item_info">
                            <div class="b-some-examples__item_info_level b-some-examples__item_name f-some-examples__item_name f-secondary-b c-primary"><a href="enterprise.htm?id=${enterprise.id}">${enterprise.name}</a></div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_double_info f-some-examples__item_double_info f-title-extra-small">
                                <c:if test="${enterprise.status eq 'activated'}">
                                    <div class="b-right"> <a href="manage-enterprises.htm?page=1&action=status&option=1&status=deactivated&id=${enterprise.id}" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-secondary-b"><spring:message code="administrator.enterprises.deactivate"/></a></div>
                                </c:if>
                                <c:if test="${enterprise.status eq 'deactivated'}">
                                    <div class="b-right"><a href="manage-enterprises.htm?page=1&action=status&option=0&status=activated&id=${enterprise.id}" class="b-btn f-btn b-btn-sm f-btn-sm b-btn-default f-secondary-b"><spring:message code="administrator.enterprises.activate"/></a></div>
                                </c:if>
                                <div class="b-remaining">${enterprise.contactName} | ${enterprise.contactPosition} | ${enterprise.contactNo} | ${enterprise.contactEmail}</div>
                            </div>
                            <div class="b-some-examples__item_info_level b-some-examples__item_description f-some-examples__item_description f-secondary-l ">
                                ${fn:substring(enterprise.description, 0, 200)}...
                            </div>
                        </div>
                    </div>
                </c:forEach>                
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row b-col-default-indent">
                        <div class="col-md-12">
                            <div class="b-categories-filter">
                                <h4 class="f-primary-b b-h4-special f-h4-special--gray f-h4-special"><spring:message code="administrator.enterprises.filter"/></h4>
                                <ul>
                                    <li>
                                        <a class="f-categories-filter_name" href="#"><i class="fa fa-plus"></i> <spring:message code="administrator.enterprises.all"/></a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${noOfEnterprise}</span>
                                    </li>
                                    <li>
                                        <a class="f-categories-filter_name" href="#"><i class="fa fa-plus"></i> <spring:message code="administrator.enterprises.activated"/></a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${noOfActivateEnterprise}</span>
                                    </li>
                                    <li>
                                        <a class="f-categories-filter_name" href="#"><i class="fa fa-plus"></i> <spring:message code="administrator.enterprises.non.activated"/></a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${noOfNonActivateEnterprise}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>