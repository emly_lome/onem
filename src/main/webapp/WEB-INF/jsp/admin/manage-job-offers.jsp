<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="menu.enterprise"/></h1>
        </div>
    </div>
</div>
<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li class="f-secondary-l"><a href="administrator.htm"><i class="fa fa-angle-right"></i><spring:message code="onem.header.administrator"/></a></li>
                <li></a><i class="fa fa-angle-right"></i><span><spring:message code="menu.manage.job.offer"/></span></li>
            </ul>
        </div>
    </div>
    <div class="l-inner-page-container">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <c:forEach var="job" items="${jobs}">
                        <div class="b-search-result-box">
                            <div class="b-search-item b-diagonal-line-bg-light">
                                <h3 class="f-primary-l  is-global-title f-h4-special"><a href="manage-offer.htm?id=${job.id}" class="f-more">${job.title}</a></h3>
                                <div class="b-form-row f-h4-special b-form--mini clearfix">
                                    <div class="pull-left">
                                        <a href="apply.htm?id=${job.id}" class="b-infoblock-with-icon__icon f-infoblock-with-icon__icon fade-in-animate b-blog-one-column__info_edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </div>
                                    <div class="b-blog-one-column__info_container">
                                        <div class="b-blog-one-column__info f-blog-one-column__info">
                                            <spring:message code="offer.result.category"/> : <span class="f-more">${job.type}</span>
                                            <span class="b-blog-one-column__info_delimiter"></span>
                                            <i class="fa fa-comment"></i><span class="f-more">${fn:length(jobOfferApplications)} <spring:message code="offer.result.no.application"/></span>
                                            <span class="b-blog-one-column__info_delimiter"></span>
                                            <spring:message code="offer.result.sector"/> : <span class="f-more">${job.sector}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-form-row b-blog-one-column__text"><span class="f-more"><spring:message code="offer.result.description"/></span> ${fn:substring(job.description, 0, 300)}...</div>
                                <div class="b-form-row">
                                    <a href="manage-offer.htm?id=${job.id}" class="b-btn f-btn b-btn-xs b-btn-default f-primary-b"><spring:message code="offer.result.read.more"/></a>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                    <div class="row b-pagination f-pagination">
                        <div class="container">
                            <ul>
                                <li><a href="directory.htm?page=1">First</a></li>
                                <li><a class="prev" href="directory.htm?page=<c:if test="${selected - 1 > 0}">${selected - 1}</c:if>"><i class="fa fa-angle-left"></i></a></li>
                                        <c:forEach var="page" begin="1" end="${pages}">
                                    <li <c:if test="${page == selected}">class="is-active-pagination"</c:if>><a href="directory.htm?page=${page}">${page}</a></li>
                                    </c:forEach>
                                <li><a class="next" href="directory.htm?page=<c:if test="${selected + 1 == pages}">${selected + 1}</c:if>"><i class="fa fa-angle-right"></i></a></li>
                                <li><a href="directory.htm?page=${pages}">Last</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="row b-col-default-indent">
                        <div class="col-md-12">
                            <div class="b-categories-filter">
                                <h4 class="f-primary-b b-h4-special f-h4-special--gray f-h4-special"><spring:message code="administrator.enterprises.filter"/></h4>
                                <ul>
                                    <li>
                                        <a class="f-categories-filter_name" href="#"><i class="fa fa-plus"></i> <spring:message code="administrator.enterprises.all"/></a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${noOfEnterprise}</span>
                                    </li>
                                    <li>
                                        <a class="f-categories-filter_name" href="#"><i class="fa fa-plus"></i> <spring:message code="administrator.enterprises.activated"/></a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${noOfActivateEnterprise}</span>
                                    </li>
                                    <li>
                                        <a class="f-categories-filter_name" href="#"><i class="fa fa-plus"></i> <spring:message code="administrator.enterprises.non.activated"/></a>
                                        <span class="b-categories-filter_count f-categories-filter_count">${noOfNonActivateEnterprise}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>