<div class="b-inner-page-header f-inner-page-header b-bg-header-inner-page">
    <div class="b-inner-page-header__content">
        <div class="container">
            <h1 class="f-primary-l c-default"><spring:message code="administrator.manage.documentation"/></h1>
        </div>
    </div>
</div>

<div class="l-main-container">
    <div class="b-breadcrumbs f-breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="index.htm"><i class="fa fa-home"></i><spring:message code="menu.home"/></a></li>
                <li class="f-secondary-l"><i class="fa fa-angle-right"></i><a href="administrator.htm"><spring:message code="onem.header.administrator"/></a></li>
                <li><i class="fa fa-angle-right"></i><span> <spring:message code="administrator.manage.documentation"/> </span></li>
            </ul>
        </div>
    </div>
    <section class="b-infoblock">
        <div class="container">
            <div class="row b-col-default-indent">
                <div class="col-md-4">
                    <div class="b-shadow-container b-remaining b-form-style-small">
                        <div class="col-md-12">
                            <p class="f-title-medium c-default f-uppercase f-primary-b b-margin-top-small"><spring:message code="administrator.add.documentation"/></p>
                            <hr class="b-hr-light">
                        </div>
                        <form:form action="upload-documentation.htm" class="b-col-default-indent" commandName="document"
                                enctype="multipart/form-data">
                            <div class="col-md-6">
                                <spring:message code="administrator.entry.name" var="name"/>
                                <form:input path="name" type="text" class="form-control" placeholder="${name}"/>
                            </div>
                            <div class="col-md-6 b-form-row b-form-select b-select--alt">
                                <form:select path="type" class="j-select">
                                    <option selected="selected"><spring:message code="administrator.entry.type.brochures"/></option>
                                    <option><spring:message code="administrator.entry.type.regulation"/></option>
                                    <option><spring:message code="administrator.entry.type.newsletter"/></option>
                                    <option><spring:message code="administrator.entry.type.speeches"/></option>
                                    <option><spring:message code="administrator.entry.type.decrets"/></option>
                                </form:select>
                            </div>                            
                            <div class="col-md-12">
                                <spring:message code="administrator.entry.description" var="description"/>
                                <form:textarea path="description" class="form-control" placeholder="${description}" rows="5"/>
                            </div>
                            <div class="col-md-12">
                                <div class="b-file-upload file_upload">
                                    <button class="b-file-upload__btn fa fa-link" type="button"></button>
                                    <div class="b-file-upload__text form-control" ></div>
                                    <input name="documentFile" type="file">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input name="Upload" type="submit" class="b-btn f-btn b-btn-md b-btn-default f-primary-b b-btn__w100"/>
                            </div>
                        </form:form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="b-tabs f-tabs j-tabs b-tabs-reset b-tabs--secondary f-tabs--secondary">
                    <ul>
                        <li><a href="#tabs-31"><spring:message code="documentation.brochures"/></a></li>
                        <li><a href="#tabs-32"><spring:message code="documentation.regulation"/></a></li>
                        <li><a href="#tabs-33"><spring:message code="documentation.newsletter"/></a></li>
                        <li><a href="#tabs-34"><spring:message code="documentation.speeches"/></a></li>
                        <li><a href="#tabs-35"><spring:message code="documentation.decrets"/></a></li>
                    </ul>
                    <div class="b-tabs__content">
                        <div id="tabs-31" class="clearfix">
                            <div class="row">
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col">
                                    <c:forEach var="release" items="${releases}">
                                        <c:if test="${release.type eq 'Brochures'}">
                                            <li><a href="press-release.htm?target=${release.fileName}&type=${release.type}" 
                                                   target="blank_"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>${release.name}</a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs-32" class="clearfix">
                            <div class="row">
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col">
                                    <c:forEach var="release" items="${releases}">
                                        <c:if test="${release.type eq 'Regulations'}">
                                            <li><a href="press-release.htm?target=${release.fileName}&type=${release.type}" 
                                                   target="blank_"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>${release.name}</a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs-33" class="clearfix">
                            <div class="row">
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col">
                                    <c:forEach var="release" items="${releases}">
                                        <c:if test="${release.type eq 'Newsletters'}">
                                            <li><a href="press-release.htm?target=${release.fileName}&type=${release.type}" 
                                                   target="blank_"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>${release.name}</a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs-34" class="clearfix">
                            <div class="row">
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col">
                                    <c:forEach var="release" items="${releases}">
                                        <c:if test="${release.type eq 'Speech'}">
                                            <li><a href="press-release.htm?target=${release.fileName}&type=${release.type}" 
                                                   target="blank_"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>${release.name}</a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                        <div id="tabs-35" class="clearfix">
                            <div class="row">
                                <ul class="c-primary c--inherit b-list-markers f-list-markers b-list-markers--without-leftindent f-list-markers--medium c-primary--all f-color-primary b-list-markers-2col f-list-markers-2col">
                                    <c:forEach var="release" items="${releases}">
                                        <c:if test="${release.type eq 'Decret'}">
                                            <li><a href="press-release.htm?target=${release.fileName}&type=${release.type}" 
                                                   target="blank_"><i class="fa fa-check-circle-o b-list-markers__ico f-list-markers__ico"></i>${release.name}</a>
                                            </li>
                                        </c:if>
                                    </c:forEach>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </section>
</div>