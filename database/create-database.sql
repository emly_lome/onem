-- -----------------------------------------------------
-- Database onem
-- -----------------------------------------------------
CREATE DATABASE onem DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE USER 'onem'@'localhost' IDENTIFIED BY PASSWORD '*183089C046CDADC70745306EC6F82431906691BD';

GRANT ALL PRIVILEGES ON onem.* TO 'onem'@'localhost' IDENTIFIED BY 'onem' WITH GRANT OPTION;

GRANT ALL PRIVILEGES ON onem.* TO 'onem'@'%' IDENTIFIED BY 'onem' WITH GRANT OPTION;

USE onem;