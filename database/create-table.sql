SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema onem
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `onem` ;
CREATE SCHEMA IF NOT EXISTS `onem` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `onem` ;

-- -----------------------------------------------------
-- Table `onem`.`enterprise`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`enterprise` ;

CREATE TABLE IF NOT EXISTS `onem`.`enterprise` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(2000) NOT NULL,
  `address` VARCHAR(150) NOT NULL,
  `town` VARCHAR(45) NOT NULL,
  `contact_name` VARCHAR(100) NOT NULL,
  `contact_position` VARCHAR(45) NOT NULL,
  `contact_no` VARCHAR(45) NULL,
  `contact_email` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`job`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`job` ;

CREATE TABLE IF NOT EXISTS `onem`.`job` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `sector` VARCHAR(100) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  `closing_date` DATE NOT NULL,
  `description` VARCHAR(5000) NOT NULL,
  `qualification` VARCHAR(2000) NULL,
  `experience` VARCHAR(2000) NULL,
  `salary` DOUBLE NOT NULL DEFAULT 0.0,
  `entreprise_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_job_entreprise_idx` (`entreprise_id` ASC),
  CONSTRAINT `fk_job_entreprise`
    FOREIGN KEY (`entreprise_id`)
    REFERENCES `onem`.`enterprise` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`job_seeker`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`job_seeker` ;

CREATE TABLE IF NOT EXISTS `onem`.`job_seeker` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(20) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` VARCHAR(20) NOT NULL,
  `marital_status` VARCHAR(45) NULL,
  `address` VARCHAR(200) NOT NULL,
  `town` VARCHAR(45) NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `contact_no` VARCHAR(45) NULL,
  `contact_email` VARCHAR(100) NOT NULL,
  `highest_qualification` VARCHAR(100) NOT NULL,
  `years_experience` VARCHAR(100) NULL,
  `description` VARCHAR(1000) NULL,
  `ref_no` VARCHAR(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`job_seeker_file`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`job_seeker_file` ;

CREATE TABLE IF NOT EXISTS `onem`.`job_seeker_file` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `type` VARCHAR(100) NOT NULL,
  `description` VARCHAR(100) NOT NULL,
  `job_seeker_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_job_seeker_file_job_seeker1_idx` (`job_seeker_id` ASC),
  CONSTRAINT `fk_job_seeker_file_job_seeker1`
    FOREIGN KEY (`job_seeker_id`)
    REFERENCES `onem`.`job_seeker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`job_preference`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`job_preference` ;

CREATE TABLE IF NOT EXISTS `onem`.`job_preference` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(500) NOT NULL,
  `job_seeker_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_job_preference_job_seeker1_idx` (`job_seeker_id` ASC),
  CONSTRAINT `fk_job_preference_job_seeker1`
    FOREIGN KEY (`job_seeker_id`)
    REFERENCES `onem`.`job_seeker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`job_alert`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`job_alert` ;

CREATE TABLE IF NOT EXISTS `onem`.`job_alert` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `decription` VARCHAR(45) NULL,
  `job_seeker_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_job_alert_job_seeker1_idx` (`job_seeker_id` ASC),
  CONSTRAINT `fk_job_alert_job_seeker1`
    FOREIGN KEY (`job_seeker_id`)
    REFERENCES `onem`.`job_seeker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`configuration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`configuration` ;

CREATE TABLE IF NOT EXISTS `onem`.`configuration` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `value` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`enterprise_login`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`enterprise_login` ;

CREATE TABLE IF NOT EXISTS `onem`.`enterprise_login` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `entreprise_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_enterprise_login_entreprise1_idx` (`entreprise_id` ASC),
  CONSTRAINT `fk_enterprise_login_entreprise1`
    FOREIGN KEY (`entreprise_id`)
    REFERENCES `onem`.`enterprise` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`job_seeker_login`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`job_seeker_login` ;

CREATE TABLE IF NOT EXISTS `onem`.`job_seeker_login` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `job_seeker_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  INDEX `fk_job_seeker_login_job_seeker1_idx` (`job_seeker_id` ASC),
  CONSTRAINT `fk_job_seeker_login_job_seeker1`
    FOREIGN KEY (`job_seeker_id`)
    REFERENCES `onem`.`job_seeker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`documentation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`documentation` ;

CREATE TABLE IF NOT EXISTS `onem`.`documentation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(200) NOT NULL,
  `description` VARCHAR(500) NULL,
  `type` VARCHAR(45) NOT NULL,
  `file_name` VARCHAR(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `file_name_UNIQUE` (`file_name` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`Job_offer_application`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`Job_offer_application` ;

CREATE TABLE IF NOT EXISTS `onem`.`Job_offer_application` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `application_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `job_offer` INT NOT NULL,
  `job_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Job_offer_application_job1_idx` (`job_id` ASC),
  CONSTRAINT `fk_Job_offer_application_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `onem`.`job` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`news`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`article` ;

CREATE TABLE IF NOT EXISTS `onem`.`article` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `bref_description` VARCHAR(200) NOT NULL,
  `full_description` VARCHAR(5000) NOT NULL,
  `created_date` DATETIME NOT NULL,
  `enterprise_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_news_enterprise1_idx` (`enterprise_id` ASC),
  CONSTRAINT `fk_news_enterprise1`
    FOREIGN KEY (`enterprise_id`)
    REFERENCES `onem`.`enterprise` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`news_image`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`news_image` ;

CREATE TABLE IF NOT EXISTS `onem`.`news_image` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `filename` VARCHAR(45) NULL,
  `artical_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_news_image_news1_idx` (`artical_id` ASC),
  CONSTRAINT `fk_news_image_news1`
    FOREIGN KEY (`artical_id`)
    REFERENCES `onem`.`article` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `onem`.`sector`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`sector` ;

CREATE TABLE IF NOT EXISTS `onem`.`sector` (
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `onem`.`job_offer_application`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `onem`.`job_offer_application`;

CREATE TABLE IF NOT EXISTS `onem`.`job_offer_application` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `application_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `status` VARCHAR(45) NOT NULL,
  `job_id` INT NOT NULL,
  `job_seeker_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Job_offer_application_job1_idx` (`job_id` ASC),
  INDEX `fk_Job_offer_application_job_seeker1_idx` (`job_seeker_id` ASC),
  CONSTRAINT `fk_Job_offer_application_job1`
    FOREIGN KEY (`job_id`)
    REFERENCES `onem`.`job` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Job_offer_application_job_seeker1`
    FOREIGN KEY (`job_seeker_id`)
    REFERENCES `onem`.`job_seeker` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
