ALTER TABLE enterprise ADD status VARCHAR(20) NOT NULL;
update enterprise set status='activated';

ALTER TABLE job_seeker DROP ref_no;
ALTER TABLE job_seeker ADD ref_no VARCHAR(20) NOT NULL;

ALTER TABLE sector MODIFY `name` VARCHAR(100) NOT NULL, DROP PRIMARY KEY;
ALTER TABLE sector ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;
